<?php 
require '../filestobeincluded/db_config.php';
$selected_leads = $_POST['data_id'];

$selected_leads = str_replace("%5B", "", $selected_leads);
$selected_leads = str_replace("%5D", "", $selected_leads);
$selected_leads = str_replace("id=", "", $selected_leads);
$selected_leads = str_replace("&", ",", $selected_leads);

if(session_status() === PHP_SESSION_NONE) session_start();

$type = $conn->query("SELECT User_Type FROM users WHERE ID='".$_SESSION['useremployeeid']."' ");
$type = mysqli_fetch_row($type)[0];


?>
<form method="POST" id="refer_new_counsellor_form">
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Stage<span style="color: #F64744; font-size: 12px">*</span></label>
        <div class="col-lg-10">
            <select data-plugin="customselect" class="form-control" id="follow_stage_select" name="follow_stage_select">
                <option disabled selected>Select Stage</option>
                    <?php
                    if($type != 'b2b'){
                        $result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type is NULL AND Status = 'Y'");
                    }else{
                        $result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type = 'b2b' AND Status = 'Y'");
                    }
                    while($stages = $result_stage->fetch_assoc()) {
                ?>
                    <option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
                <?php } ?>
            </select>
        </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Reason<span style="color: #F64744; font-size: 12px">*</span></label>
    <div class="col-lg-10">
        <select data-plugin="customselect" class="form-control" id="follow_reason" name="follow_reason">
            <option disabled selected>Select Reason</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-lg-2 col-form-label">Select Counsellor</label>
    <div class="col-lg-10">
        <select class="form-control" multiple data-plugin="customselect" id="refer_lead_owner" name="refer_lead_owner">
            <?php
                $result_refer_lead = $conn->query("SELECT * FROM users WHERE Status='Y' ");
                while($refer_leads = $result_refer_lead->fetch_assoc()) {
            ?>
                <option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
            <?php } ?>
        </select>
        <div class="custom-control custom-checkbox" style="margin-top: 10px;">
            <input class="custom-control-input checkbox-function" type="checkbox" id="all" value="not-selected">
            <label class="custom-control-label" for="all">Select All</label>
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Add Comment</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" id="referal_comment" placeholder="">
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" id="refer_btn" class="btn btn-primary" onclick="referSelectedLeads()" >&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
</div>
</form>

<script>
    $('#all').click(function() {
        if($('#all').val() == "not-selected") {
            $('#refer_lead_owner option').attr('selected', true).parent().trigger('change');
            $('#all').val("selected");
        }
        else {
            $('#refer_lead_owner option').attr('selected', false).parent().trigger('change');
            $('#all').val("not-selected");
        }
    });
</script>

<script>
    $(document).ready(function() { 
        $("#refer_lead_owner").select2({
            placeholder: "Choose"
        });
    });
</script>

<script>
    function enableReferButton() {
        $("#refer_btn").prop("disabled", false);
        $('#refer_btn').css('cursor', 'pointer');
        $('#refer_btn').text('Refer');
    }

    function disableReferButton() {
        $("#refer_btn").prop("disabled", true);
        $('#refer_btn').css('cursor', 'no-drop');
        $('#refer_btn').text('Referring...');
    }
</script>

<script>
    function referSelectedLeads() {
        if($('#refer_new_counsellor_form').valid()) {
            var new_counsellor = $('#refer_lead_owner').val();
			var new_stage = $('#follow_stage_select').val();
			var new_reason = $('#follow_reason').val();
            var all_selected_leads = '<?php echo $selected_leads ?>';
            var comments = $('#referal_comment').val();

            $.ajax
                ({
                  type: "POST",
                  url: "ajax_leads/ajax_refer_selected.php",
                  beforeSend: disableReferButton,
                  complete: enableReferButton,
                  data: { "new_counsellor": new_counsellor, "all_selected_leads": all_selected_leads, "comments": comments, "new_stage":new_stage, "new_reason":new_reason },
                  success: function (data) {
                      console.log(data);
                    if(data.match("true")) {
                        $('.modal').modal('hide');
                        toastr.success('Selected leads referred successfully');
                        // location.reload();
                    }
                    else {
                        toastr.error('Unable to refer leads');
                    }
                  }
                });
            }
        }
</script>



<script>
    $(document).ready(function() {
        $('#refer_new_counsellor_form').validate({
            rules: {
                follow_stage_select: {
                    required: true
                },
                follow_reason: {
                    required: true
                },
                refer_lead_owner: {
                    required: true
                }
            },
            messages: {
                follow_stage_select: {
                    required: 'Please select a stage'
                },
                follow_reason: {
                    required: 'Please select a reason'
                },
                refer_lead_owner: {
                    required: 'Please select a counsellor'
                }
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
            }
        });
    })    
</script>

<script>
	$(document).ready(function() {
		$('#follow_stage_select').change(function() {

			$('#follow_reason').html("<option disabled selected>Select Reason</option>");

			var new_selected_stage = $('#follow_stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "../onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": new_selected_stage },
				success: function(data) {
					if(data != "") {
						$('#follow_reason').html(data);
					}
					else {
						$('#follow_reason').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});
</script>

<?php
exit;
?>
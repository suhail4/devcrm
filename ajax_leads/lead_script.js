


		function followupmodal(id) {
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"userid": userid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}




		function responsesmodal(id) {
			
			var responseid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"responseid": responseid},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}




		function re_enquiredmodal(id) {
			
			var renquiredid = id;
			var tabValue = $('.user_tabs.active').val();

			// AJAX request
			$.ajax({
				url: 'ajax_leads/right_modal.php',
				type: 'post',
				data: {"renquiredid": renquiredid, "tabValue":tabValue},
				success: function(response){ 
					// Add response in Modal body
					$('#right-modal-body').html(response); 
					$('.modal-backdrop').remove();

					// Display Modal
					$('#right_modal').modal('show'); 
				}
			});
		}




							

    $(".checkbox-selectall").click(function () {
		$(".checkbox-function").prop('checked', $(this).prop('checked'));
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide1").css({display: "block"});
			$("#divShowHide2").css({display: "block"});
			$("#divShowHide3").css({display: "block"});
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide1").css({display: "none"});
			$("#divShowHide2").css({display: "none"});
			$("#divShowHide3").css({display: "none"});
			$("#divShowHide4").css({display: "none"});
		}
    });
    
    $(".checkbox-function").change(function(){
        if (!$(this).prop("checked")){
            $(".checkbox-selectall").prop("checked",false);
        }
	});
	
	function checkbox() {
		var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
		console.log(data_count);
		if(data_count>0){
			$("#divShowHide1").css({display: "block"});
			$("#divShowHide2").css({display: "block"});
			$("#divShowHide3").css({display: "block"});
			$("#divShowHide4").css({display: "block"});
		}else{
			$("#divShowHide1").css({display: "none"});
			$("#divShowHide2").css({display: "none"});
			$("#divShowHide3").css({display: "none"});
			$("#divShowHide4").css({display: "none"});
		}
    }

           	







		function whatsApp(id) {
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/whatsapp.php',
				type: 'post',
				data: {"userid": userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-whatsapp').html(response); 

					// Display Modal
					$('#whatsappmessage').modal('show'); 
				}
			});
		}





		function addFollowUp_ajax(id) {
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/add_followup.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-addfollowup').html(response); 

					// Display Modal
					$('#addfollowup').modal('show'); 
				}
			});
		}
												





		function editLead(id){
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/edit_lead_form.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-edit').html(response); 

					// Display Modal
					$('#editlead').modal('show'); 
				}
			});
		}





		function viewLeadHistory(id){
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/lead_history.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-history').html(response); 

					// Display Modal
					$('#viewhistory').modal('show'); 
				}
			});
		}





		function referLead(id){
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/refer_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-refer').html(response); 

					// Display Modal
					$('#referlead').modal('show'); 
				}
			});
		}
								







		function deleteLead(id){
			
			var userid = id;

			// AJAX request
			$.ajax({
				url: 'ajax_leads/del_lead.php',
				type: 'post',
				data: {userid: userid},
				success: function(response){ 
					// Add response in Modal body
					$('#modal-body-delete').html(response); 

					// Display Modal
					$('#deletelead').modal('show'); 
				}
			});
		}





	function setEmailStatus() {
		if($('#swemail').val() == 'on') {
			$('#swemail').val('off');
		}
		else {
			$('#swemail').val('on');
		}
	}



	function setSMSStatus() {
		if($('#swsms').val() == 'on') {
			$('#swsms').val('off');
		}
		else {
			$('#swsms').val('on');
		}
	}



	function addLead() {
		if($('#step_1_form').valid() && $('#step_2_form').valid() && $('#step_3_form').valid() && $('#step_4_form').valid() && $('#step_5_form').valid()) {
			var stageID = $('#stage_select').val();
			var reasonID = $('#reason_select').val();
			var fullName = $('#full_name').val();
			var emailID = $('#email_id').val();
			var mobileNumber = $('#mobile_number').val();
			var remarks = $('#remarks').val();

			var address = $('#address').val();
			var stateID = $('#state_select').val();
			var cityID = $('#city_select').val();
			var pincode = $('#pin_code').val();

			var sourceID = $('#source_select').val();
			var subsourceID = $('#subsource_select').val();
			var campaignName = $('#campaign_name').val();
			var leadOwner = $('#lead_owner').val();

			var swEmail = $('#swemail').val();
			var swSMS = $('#swsms').val();

			var schoolName = $('#school_name').val();
			var percentage = $('#percentage').val();
			var qualification = $('#qualification').val();
			var refer = $('#refer').val();

			var instituteID = $('#select_institute').val();
			var courseID = $('#select_course').val();
			var specializationID = $('#select_specialization').val();

			if(swEmail == 'on') {
				$.ajax
		        ({
		          type: "POST",
		          url: "/Mailer/send_mail.php",
		          data: { "leadName": fullName, "emailID": emailID },
		          success: function (data) {
		          	console.log(data);
		            if(data.match("true")) {
		                toastr.success('Welcome Email sent!');
		            }
		            else {
		                toastr.error('Unable to send welcome Email');
		            }
		          }
		        });
			}

			if(swSMS == 'on') {
				$.ajax
		        ({
		          type: "POST",
		          url: "/Twilio/index.php",
		          data: { "leadName": fullName, "phoneNumber": mobileNumber },
		          success: function (data) {
		          	console.log(data);
		            if(data.match("true")) {
		                toastr.success('Welcome SMS sent!');
		            }
		            else {
		                toastr.error('Unable to send welcome SMS');
		            }
		          }
		        });
			}

			$.ajax
	        ({
	          type: "POST",
	          url: "/ajax_leads/add_lead.php",
	          data: { "stageID": stageID, "reasonID":  reasonID, "fullName": fullName, "emailID": emailID, "mobileNumber": mobileNumber, "remarks": remarks, "address": address, "stateID": stateID, "cityID": cityID, "pincode": pincode, "sourceID": sourceID, "subsourceID": subsourceID, "campaignName": campaignName, "leadOwner": leadOwner, "schoolName": schoolName, "percentage": percentage, "qualification": qualification, "refer": refer, "instituteID": instituteID, "courseID": courseID, "specializationID": specializationID },
	          success: function (data) {
	          	console.log(data);
	            $('#leadadd').modal('hide');
	            if(data.match("true")) {
	                toastr.success('Lead added successfully');
	                window.location.reload();
	            }
	            else {
	                toastr.error('Unable to add lead');
	            }
	          }
	        });
	        return false;
		}
		else {
			if('step_1_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 0);
			}
			if('step_2_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 1);
			}
			if('step_3_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 2);
			}
			if('step_4_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 3);
			}
			if('step_5_form'.valid() == false) {
				$('#smartwizard-arrows').smartWizard("goToStep", 4);
			}
		}
	}




	function mywizard(){
		$('#smartwizard-arrows').smartWizard({
			theme: 'arrows'
		});

		$('#step_1_form').validate({
			rules: {
				stage_select: {
					required: true
				},
				full_name: {
					required: true
				},
				email_id: {
					email: "Please enter a valid email address."
				},
				mobile_number: {
					required: true,
					minlength: 10,
					maxlength: 10
				},
				remarks: {
					required: true
				}
			},
			messages: {
				stage_select: {
					required: 'Please select a stage'
				},
				full_name: {
					required: 'Name is required'
				},
				mobile_number: {
					required: 'Mobile number is required',
					minlength: 'Invalid mobile number',
					maxlength: 'Invalid mobile number'
				},
				remarks: {
					required: 'Remarks are required'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$('#step_2_form').validate({
			rules: {
				state_select: {
					required: true
				},
				pin_code: {
					minlength: 6,
					maxlength: 6
				}
			},
			messages: {
				state_select: {
					required: 'Please select a state'
				},
				pin_code: {
					minlength: 'Invalid Pincode',
					minlength: 'Invalid Pincode'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$('#step_3_form').validate({
			rules: {
				source_select: {
					required: true
				}
			},
			messages: {
				source_select: {
					required: 'Please select a source'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$('#step_5_form').validate({
			rules: {
				select_institute: {
					required: true
				}
			},
			messages: {
				select_institute: {
					required: 'Please select an university'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});

		$("#smartwizard-arrows").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
			if(currentStepIndex == 0) {
				if($('#step_1_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 1) {
				if($('#step_2_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 2) {
				if($('#step_3_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 3) {
				if($('#step_4_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
			if(currentStepIndex == 4) {
				if($('#step_5_form').valid()) {
					return true;
				}
				else {
					return false;
				}
			}
		});
};



	$(document).ready(function() {
		$('#stage_select').change(function() {

			$('#reason_select').html("<option disabled selected>Select Reason</option>");

			var selected_stage = $('#stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": selected_stage },
				success: function(data) {
					if(data != "") {
						$('#reason_select').html(data);
					}
					else {
						$('#reason_select').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});



	$(document).ready(function() {
		$('#state_select').change(function() {

			$('#city_select').html("<option disabled selected>Select City</option>");

			var selected_state = $('#state_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "state_select": "", "selected_state": selected_state },
				success: function(data) {
					if(data != "") {
						$('#city_select').html(data);
					}
					else {
						$('#city_select').html("<option disabled selected>Select City</option>");
					}
				}
			});
		});
	});



	$(document).ready(function() {
		$('#source_select').change(function() {

			$('#subsource_select').html("<option disabled selected>Select Sub-Source</option>");

			var selected_source = $('#source_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "source_select": "", "selected_source": selected_source },
				success: function(data) {
					if(data != "") {
						$('#subsource_select').html(data);
					}
					else {
						$('#subsource_select').html("<option disabled selected>Select Sub-Source</option>");
					}
				}
			});
		});
	});



	$(document).ready(function() {
		$('#select_institute').change(function() {

			$('#select_course').html("<option disabled selected>Select Course</option>");

			var selected_institute = $('#select_institute').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "select_institute": "", "selected_institute": selected_institute },
				success: function(data) {
					if(data != "") {
						$('#select_course').html(data);
						jQuery('#select_course').trigger('change');
					}
					else {
						$('#select_course').html("<option disabled selected>Select Course</option>");
					}
				}
			});
		});
	});



	$(document).ready(function() {
		$('#select_course').change(function() {
			console.log("yes");

			$('#select_specialization').html("<option disabled selected>Select Specialization</option>");

			var selected_course = $('#select_course').val();
			var selected_institute = $('#select_institute').val();

			console.log(selected_institute);
			
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "select_course": "", "selected_course": selected_course, "selected_institute": selected_institute },
				success: function(data) {
					console.log(data);
					if(data != "") {
						$('#select_specialization').html(data);
					}
					else {
						$('#select_specialization').html("<option disabled selected>Select Specialization</option>");
					}
				}
			});
		});
	});





	function updateLead(id) {
			var new_leadID = $('#new_leadID').val();
			var new_stageID = $('#new_stage_select').val();
			var new_reasonID = $('#new_reason_select').val();
			var new_fullName = $('#new_full_name').val();
			var new_emailID = $('#new_email_id').val();
			var new_mobileNumber = $('#new_mobile_number').val();
			var new_remarks = $('#new_remarks').val();

			var new_address = $('#new_address').val();
			var new_stateID = $('#new_state_select').val();
			var new_cityID = $('#new_city_select').val();
			var new_pincode = $('#new_pin_code').val();

			var new_sourceID = $('#new_source_select').val();
			var new_subsourceID = $('#new_subsource_select').val();
			var new_campaignName = $('#new_campaign_name').val();
			var new_leadOwner = $('#new_select_counsellor').val();

			var new_schoolName = $('#new_school_name').val();
			var new_percentage = $('#new_percentage').val();
			var new_qualification = $('#new_qualification').val();
			var new_refer = $('#new_refer').val();

			var new_instituteID = $('#new_select_institute').val();
			var new_courseID = $('#new_select_course').val();
			var new_specializationID = $('#new_select_specialization').val();


			$.ajax
	        ({
	          type: "POST",
	          url: "/ajax_leads/edit_lead.php",
	          data: {"new_leadID": new_leadID, "new_stageID": new_stageID, "new_reasonID":  new_reasonID, "new_fullName": new_fullName, "new_emailID": new_emailID, "new_mobileNumber": new_mobileNumber, "new_remarks": new_remarks, "new_address": new_address, "new_stateID": new_stateID, "new_cityID": new_cityID, "new_pincode": new_pincode, "new_sourceID": new_sourceID, "new_subsourceID": new_subsourceID, "new_campaignName": new_campaignName, "new_leadOwner": new_leadOwner, "new_schoolName": new_schoolName, "new_percentage": new_percentage, "new_qualification": new_qualification, "new_refer": new_refer, "new_instituteID": new_instituteID, "new_courseID": new_courseID, "new_specializationID": new_specializationID },
	          success: function (data) {
	          	console.log(data);
				  $('.modal').modal('hide');
	            if(data.match("true")) {
	                toastr.success('Lead updated successfully');
	                window.location.reload();
	            }
	            else {
	                toastr.error('Unable to update lead');
	            }
	          }
	        });
	        return false;
		
	}



	$(document).ready(function() {
		$('#new_stage_select').change(function() {

			$('#new_reason_select').html("<option disabled selected>Select Reason</option>");

			var new_selected_stage = $('#new_stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": new_selected_stage },
				success: function(data) {
					if(data != "") {
						$('#new_reason_select').html(data);
					}
					else {
						$('#new_reason_select').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});



	$(document).ready(function() {
		$('#new_select_institute').change(function() {

			$('#new_select_course').html("<option disabled selected>Select Course</option>");

			var new_selected_institute = $('#new_select_institute').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onSelect.php",
				data: { "select_institute": "", "selected_institute": new_selected_institute },
				success: function(data) {
					if(data != "") {
						$('#new_select_course').html(data);
						getSpecializations();
					}
					else {
						$('#new_select_course').html("<option disabled selected>Select Course</option>");
					}
				}
			});
		});
	});


$(document).ready(function(){
  $('[data-toggle="popover"]').popover();   
});


	function getSpecializations() {

		$('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");

		var new_selected_course = $('#new_select_course').val();
		
		$.ajax
		({
			type: "POST",
			url: "onselect/onSelect.php",
			data: { "select_course": "", "selected_course": new_selected_course },
			success: function(data) {
				if(data != "") {
					$('#new_select_specialization').html(data);
				}
				else {
					$('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");
				}
			}
		});
	}



    function deleteLeads(id) {
        var delete_lead_id = $('#delete_lead_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/delete_lead.php",
          data: { "delete_lead_id": delete_lead_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead deleted successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to delete lead');
            }
          }
        });
        return false;
    }


  
      $(document).ready(function(){  
           $('#upload_csv').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "ajax_leads/upload_lead.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
                     	console.log(data);
						$('.modal').modal('hide');
						if(data.match("true")) {
							toastr.success('Lead uploaded successfully');
							window.location.reload();
						}
						else {
							toastr.error('Unable to upload lead');
						}  
                     }  
                })  
           });  
      });  
 


	function updateOwner(){
            var lead_id = $('#lead_id').val();
            var lead_owner_id = $('#lead_owner').val();
            var refer_lead_owner_id = $('#refer_lead_owner').val();
            var refer_comment = $('#referal_comment').val();
        $.ajax
        ({
          type: "POST",
          url: "ajax_leads/refer_lead_sql.php",
          data: { "lead_owner_id": lead_owner_id, "refer_lead_owner_id": refer_lead_owner_id, "refer_comment": refer_comment, "lead_id": lead_id },
          success: function (data) {
          	console.log(data);
            $('.modal').modal('hide');
            if(data.match("true")) {
				toastr.success('Lead refered successfully');
				window.location.reload();
            }
            else {
                toastr.error('Unable to refer lead');
            }
          }
        });
        return false;
    }





	function add_active(val){
    //alert(val);
    $('.user_tabs').removeClass('active');
    if(val ==0){
      $('#all_lead').addClass('active');
      
    }
    else if(val ==1){
      $('#new_lead').addClass('active');
      
    }
    else if(val ==2){
      $('#follow_up').addClass('active');
     
    }else if(val ==3){
      $('#not_interested').addClass('active');
      
    }else if(val ==4){
      $('#reg_done').addClass('active');
      
    }else if(val ==5){
      $('#admi_done').addClass('active');
      
    }else if(val ==6){
      $('#intr_b2b').addClass('active');
      
    }else if(val ==7){
      $('#not_connect').addClass('active');
      
    }else if(val ==8){
      $('#re_enquiry').addClass('active');
      
    }

  }

		function getList(vars="") {
			var searchstring = $('#searchbox').val();
			var tabValue = $('.user_tabs.active').val();
			//alert(tabValue);

			var itemPerPage = 10;
      var page =1;
      var totalPages = 0;
      if($("#pagination_info").attr("data-totalpages") != undefined ){
          totalPages = $("#pagination_info").attr("data-totalpages");
      }

      if($("#pagination_info").attr("data-currentpage") != undefined){
          page = $("#pagination_info").attr("data-currentpage");
      }

      if(vars == 'next' && page > 0 && totalPages != page ){
         page = parseInt(page)+1;
        // $('#next').attr('data-page',page);
         $("#pagination_info").attr("data-currentPage",page)
      }else{
        //$('#next').attr('data-page',1);
        $('#next').attr('disabled','disabled');
      }

      if(vars == 'pre' && page > 1 ){
         page = parseInt(page)-1 ;
         $('#pre').attr('data-page',page);
        // value='';
      }else{
        $('#pre').attr('data-page',1);
        $('#pre').attr('disabled','disabled');
      }

			$.ajax({
				url:"fetch_leads.php",
				type: "GET",
				global: true, 
				data: {"tabValue":tabValue,"searchstring":searchstring,"page":page,"itemPerPage":itemPerPage},
				success: function(data) {
					//console.log(data);
					$("#leads_tbody").html(data);
					$('html, body').animate({ scrollTop: 0 }, 'slow');
					
				}
			});
		}
		getList();
	

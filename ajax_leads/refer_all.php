<?php 
require '../filestobeincluded/db_config.php';
$all_leads=array();
$leads_query_res = $conn->query("SELECT * FROM Leads ORDER BY TimeStamp DESC");
while($row = $leads_query_res->fetch_assoc()) {
    $all_leads[] = $row;
}
?>

<?php
    foreach($all_leads as $r_leads){
        $refer_lead_id = $r_leads['ID'].',';
    }
?>

<form method="POST" id="refer_new_counsellor_form">
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Stage<span style="color: #F64744; font-size: 12px">*</span></label>
        <div class="col-lg-10">
            <select data-plugin="customselect" class="form-control" id="follow_stage_select" name="follow_stage_select">
                <option disabled selected>Select Stage</option>
                    <?php
                    $result_stage = $conn->query("SELECT * FROM Stages WHERE Status = 'Y'");
                    while($stages = $result_stage->fetch_assoc()) {
                ?>
                    <option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
                <?php } ?>
            </select>
        </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Reason<span style="color: #F64744; font-size: 12px">*</span></label>
    <div class="col-lg-10">
        <select data-plugin="customselect" class="form-control" id="follow_reason" name="follow_reason">
            <option disabled selected>Select Reason</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-lg-2 col-form-label">Select Counsellor</label>
    <div class="col-lg-10">
        <select data-plugin="customselect" class="form-control" id="refer_lead_owner" name="refer_lead_owner">
            <option disabled selected>Choose</option>
            <?php
                $result_refer_lead = $conn->query("SELECT * FROM users");
                while($refer_leads = $result_refer_lead->fetch_assoc()) {
            ?>
                <option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label">Add Comment</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" id="referal_comment" placeholder="">
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="referAllLeads()" >&nbsp;&nbsp;Refer&nbsp;&nbsp;</button>
</div>
</form>

<script>
    function referAllLeads() {
        if($('#refer_new_counsellor_form').valid()) {
            var new_counsellor = $('#refer_lead_owner').val();
            var all_selected_leads = '<?php foreach($all_leads as $r_leads){ echo $r_leads['ID'].',';} ?>';
            var comments = $('#referal_comment').val();

            $.ajax
                ({
                  type: "POST",
                  url: "ajax_leads/ajax_refer_all.php",
                  data: { "new_counsellor": new_counsellor, "all_selected_leads": all_selected_leads, "comments": comments },
                  success: function (data) {
                    console.log(data);
                    if(data.match("true")) {
                        toastr.success('All leads referred successfully');
                        location.reload();
                    }
                    else {
                        toastr.error('Unable to all refer leads');
                    }
                  }
                });
            }
        }
</script>

<script>
	$(document).ready(function() {
		$('#follow_stage_select').change(function() {

			$('#follow_reason').html("<option disabled selected>Select Reason</option>");

			var new_selected_stage = $('#follow_stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "../onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": new_selected_stage },
				success: function(data) {
					if(data != "") {
						$('#follow_reason').html(data);
					}
					else {
						$('#follow_reason').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});
</script>

<script>
    $(document).ready(function() {
        $('#refer_new_counsellor_form').validate({
            rules: {
                refer_lead_owner: {
                    required: true
                }
            },
            messages: {
                refer_lead_owner: {
                    required: 'Please select a counsellor'
                }
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
            }
        });
    })    
</script>
<?php
exit;
?>
<?php
//include database configuration file
ini_set('max_input_time','600');
if(session_status() === PHP_SESSION_NONE) session_start();
include '../filestobeincluded/db_config.php';

$stage = $_POST['stage_id'];
if($stage == '0' || $stage == ''){
    $query = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp, Follow_Ups.Follow_Up_Status as F_Status,Leads.Timestamp FROM Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Follow_Ups.ID IN (SELECT MAX(ID) FROM Follow_Ups GROUP BY Lead_ID) AND Followup_Timestamp < Follow_Ups.Last_Updated_Timestamp GROUP BY Lead_ID ORDER BY ID DESC");
}else if($stage == '1'){
    $query = $conn->query("SELECT Leads.ID as ID,Leads.Name As Name,Follow_Ups.ID As follow_id,Leads.Email As Email,Leads.Stage_ID As Stage_ID,Leads.Reason_ID As Reason_ID,Leads.Mobile As Mobile,Leads.Alt_Mobile As Alt_Mobile,Leads.Remarks As Remarks,Leads.Address As Address,Leads.City_ID As City_ID,Leads.Pincode As Pincode,Leads.Source_ID As Source_ID,Leads.Subsource_ID As Subsource_ID,Leads.CampaignName As CampaignName,Leads.Previous_Owner_ID As Previous_Owner_ID, Leads.School As School,Leads.Grade As Grade,Leads.Qualification As Qualification,Leads.Refer As Refer,Leads.Institute_ID As Institute_ID,Leads.Course_ID As Course_ID,Leads.Specialization_ID As Specialization_ID,Leads.Counsellor_ID As Counsellor_ID,Follow_Ups.Counsellor_ID As couns,Follow_Ups.Remark As rem,Follow_Ups.Followup_Timestamp As Followup_Timestamp,Leads.Timestamp FROM (SELECT *, max(Followup_Timestamp) FROM Follow_Ups GROUP BY Followup_Timestamp, Lead_ID ORDER BY Followup_Timestamp DESC) AS Follow_Ups LEFT JOIN Leads ON Follow_Ups.Lead_ID = Leads.ID LEFT JOIN users ON Leads.Counsellor_ID=users.ID LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID WHERE Follow_Ups.ID IN (SELECT MAX(ID) FROM Follow_Ups GROUP BY Lead_ID) AND  Follow_Ups.Followup_Timestamp > NOW() GROUP BY Lead_ID DESC");
}



if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "Followup_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Name',	'Email', 'Mobile', 'Alt Mobile', 'Stage', 'Reason', 'Remarks', 'University', 'Course', 'Specialization', 'State', 'City', 'Source', 'Subsource', 'Counsellor','Manager', 'Updated Date', 'Creation Date', 'Follow Up Remark');

    fputcsv($f, $fields, $delimiter);
    $stage_arr = array();
		$stage_query = $conn->query("SELECT ID,Name FROM Stages");
		while ($row = mysqli_fetch_assoc($stage_query)) {
			$stage_arr[$row['ID']] = $row['Name'];
		}

		$reason_arr = array();
		$reason_query = $conn->query("SELECT ID,Name FROM Reasons");
		while ($row = mysqli_fetch_assoc($reason_query)) {
			$reason_arr[$row['ID']] = $row['Name'];
		}

		$institute_arr = array();
		$institute_query = $conn->query("SELECT ID,Name FROM Institutes");
		while ($row = mysqli_fetch_assoc($institute_query)) {
			$institute_arr[$row['ID']] = $row['Name'];
		}

		$course_arr = array();
		$course_query = $conn->query("SELECT ID,Name FROM Courses");
		while ($row = mysqli_fetch_assoc($course_query)) {
			$course_arr[$row['ID']] = $row['Name'];
		}

		$specialization_arr = array();
		$specialization_query = $conn->query("SELECT ID,Name FROM Specializations");
		while ($row = mysqli_fetch_assoc($specialization_query)) {
			$specialization_arr[$row['ID']] = $row['Name'];
		}

		$state_arr = array();
		$state_query = $conn->query("SELECT ID,Name FROM States");
		while ($row = mysqli_fetch_assoc($state_query)) {
			$state_arr[$row['ID']] = $row['Name'];
		}

		$source_arr = array();
		$source_query = $conn->query("SELECT ID,Name FROM Sources");
		while ($row = mysqli_fetch_assoc($source_query)) {
			$source_arr[$row['ID']] = $row['Name'];
		}

		$subsource_arr = array();
		$subsource_query = $conn->query("SELECT ID,Name FROM Sub_Sources");
		while ($row = mysqli_fetch_assoc($subsource_query)) {
			$subsource_arr[$row['ID']] = $row['Name'];
		}
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        
        $stage_Id = isset($stage_arr[$row['Stage_ID']]) ? $stage_arr[$row['Stage_ID']] : " ";

        
        $reason_Id = isset($reason_arr[$row['Reason_ID']]) ? $reason_arr[$row['Reason_ID']] : " ";

        $institute_Id = isset($institute_arr[$row['Institute_ID']]) ? $institute_arr[$row['Institute_ID']] : " ";

        $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$row['Course_ID']."' AND Institute_ID = '".$row['Institute_ID']."'");
        if($course_query->num_rows > 0) {
            $course_fet = mysqli_fetch_assoc($course_query);
            $course_Id = $course_fet['Name'];
        }else{
            $course_Id = ' ';
        }
        

        $specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '".$row['Specialization_ID']."' AND Institute_ID = '".$row['Institute_ID']."' AND Course_ID = '".$row['Course_ID']."'");
        if($specialization_query->num_rows > 0) {
            $specialization_fet = mysqli_fetch_assoc($specialization_query);
            $specialization_Id = $specialization_fet['Name'];
        }else{
            $specialization_Id = ' ';
        }
        
        $state_Id = isset($state_arr[$row['State_ID']]) ? $state_arr[$row['State_ID']] : " ";

        $city_query = $conn->query("SELECT * FROM Cities WHERE ID = '".$row['City_ID']."'");
        if($city_query->num_rows > 0) {
            $city_fet = mysqli_fetch_assoc($city_query);
            $city_Id = $city_fet['Name'];
        }else{
            $city_Id = ' ';
        }
        
        $source_Id = isset($source_arr[$row['Source_ID']]) ? $source_arr[$row['Source_ID']] : " ";

        
        $subsource_Id = isset($subsource_arr[$row['Subsource_ID']]) ? $subsource_arr[$row['Subsource_ID']] : " ";

        $employee_Id = $row['Counsellor_ID'];
        $manager = $conn->query("SELECT Reporting_To_User_ID from users WHERE ID ='".$row['Counsellor_ID']."'");
        $manager_name = mysqli_fetch_row($manager);
        $manager_name = $conn->query("SELECT Name from users WHERE ID ='".$manager_name[0]."'");
        $manager_name = mysqli_fetch_row($manager_name);


        
        $created_date = $row['Created_at'];
        $updated_date = $row['TimeStamp'];

        $followup_sql  = $conn->query("SELECT * FROM Follow_Ups WHERE Lead_ID='".$row['ID']."' ORDER BY ID DESC LIMIT 1");
        if($followup_sql->num_rows > 0)
        {
            $followup_get = mysqli_fetch_assoc($followup_sql);
            $follow_up_remark = $followup_get['Remark'];
        }
        else {
            $follow_up_remark = '';
        }

        $lineData = array($row['Name'], $row['Email'], $row['Mobile'], $row['Alt_Mobile'], $stage_Id, $reason_Id, $row['Remarks'], $institute_Id, $course_Id, $specialization_Id, $state_Id, $city_Id, $source_Id, $subsource_Id, $employee_Id,$manager_name[0], $updated_date, $created_date, $follow_up_remark);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

<?php
//include database configuration file
if(session_status() === PHP_SESSION_NONE) session_start();
include '../filestobeincluded/db_config.php';


    $query = $conn->query("SELECT * FROM Reasons ORDER BY ID ASC");




if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "Reasons_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Reason', 'Stage');

    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $reason_query = $conn->query("SELECT * FROM Stages WHERE ID='".$row['Stage_ID']."'");
        $get_reason = mysqli_fetch_assoc($reason_query);
        
      
        $lineData = array($row['Name'], $get_reason['Name']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>
<?php 

if(session_status() === PHP_SESSION_NONE) session_start();
date_default_timezone_set('Asia/Kolkata');
require '../filestobeincluded/db_config.php';

$follow_up_id = $_POST['userid'];
$get_followup_details = $conn->query("SELECT * FROM Follow_Ups WHERE ID = $follow_up_id");
$detail = mysqli_fetch_assoc($get_followup_details);
$userid = $detail['Lead_ID'];
 
$sql = "select Name from Leads where ID=".$userid;
$result = $conn->query($sql);
$row = mysqli_fetch_assoc($result);
?>
<div class="row">
    <div class="col-12">
        <form method="POST" id="followup_form">
        	<input type="hidden" id="employee_id" value="<?php echo $_SESSION['useremployeeid'] ?>">
        	<input type="hidden" id="the_lead_id" value="<?php echo $userid ?>">
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Lead Name</label>
                <div class="col-lg-10">
                    <input type="text" id="lead_fullname" name="lead_fullname" class="form-control" value="<?php echo $row['Name']; ?>" disabled>
                </div>
            </div>
            <div class="form-group row">
            	<label class="col-lg-2 col-form-label">Stage<span style="color: #F64744; font-size: 12px">*</span></label>
		        	<div class="col-lg-10">
				        <select data-plugin="customselect" class="form-control" id="follow_stage_select" name="follow_stage_select">
				        	<option disabled selected>Select Stage</option>
				        		<?php
				                $result_stage = $conn->query("SELECT * FROM Stages WHERE Status = 'Y'");
				                while($stages = $result_stage->fetch_assoc()) {
				            ?>
				                <option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
				            <?php } ?>
				        </select>
		        	</div>
            </div>
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Reason<span style="color: #F64744; font-size: 12px">*</span></label>
                <div class="col-lg-10">
	                <select data-plugin="customselect" class="form-control" id="follow_reason" name="follow_reason">
	                    <option disabled selected>Select Reason</option>
	                </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-12 col-form-label">Add/Update Followup Type</label>
                <div class="col-lg-12">
                    <select data-plugin="customselect" class="form-control" id="followup_type" name="followup_type">
                        <option value="Followup Call" selected>Followup Call</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Select Date</label>
                <div class="col-lg-10">
                    <input type="date" id="basic_datepicker" name="basic_datepicker" class="form-control" value="<?php echo date("Y-m-d", strtotime(' +1 day')) ?>">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Select Time</label>
                <div class="col-lg-10">
                    <input type="Time" id="basic_timepicker" name="basic_timepicker" onchange="onTimeChange()" class="form-control" value="<?php echo date("G:i") ?>">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Remark</label>
                <div class="col-lg-10">
                    <input type="text" id="followup_remark" class="form-control" placeholder="Remark">
                </div>
            </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="addFollowUp();">Add</button>
</div>
</form>

<script>
	document.getElementById('follow_stage_select').addEventListener('change', function() {
    if (this.value == 3) {
		document.getElementById('basic_timepicker').disabled = true;
		document.getElementById('basic_datepicker').disabled = true;
		document.getElementById('followup_type').disabled = true;
		$('#followup_type').html("<option selected value='0'>None</option>");
    } else {
        document.getElementById('basic_timepicker').disabled = false;
		document.getElementById('basic_datepicker').disabled = false;
		document.getElementById('followup_type').disabled = false;
    }
});
</script>

<script>
	function addFollowUp() {
		if($('#followup_form').valid()) {
			var follow_up_id = '<?php echo $follow_up_id; ?>';
			var counsellor_id = $('#employee_id').val();
			var lead_id = $('#the_lead_id').val();
			var followup_type = $('#followup_type').val();
			var new_stage = $('#follow_stage_select').val();
			var new_reason = $('#follow_reason').val();
			var follow_date = $('#basic_datepicker').val();
			var follow_time = $('#basic_timepicker').val();
			var follow_date = $('#basic_datepicker').val();
			var follow_time = $('#basic_timepicker').val();
			var followup_remark = $('#followup_remark').val();

			$.ajax
	        ({
	          type: "POST",
	          url: "ajax_leads/next_ajax_followup.php",
	          data: {"follow_up_id": follow_up_id, "counsellor_id": counsellor_id, "lead_id": lead_id, "followup_type": followup_type, "new_stage":  new_stage, "new_reason": new_reason, "follow_date": follow_date, "follow_time": follow_time, "followup_remark": followup_remark },
	          success: function (data) {
				$('.modal').modal('hide');
	            if(data.match("true")) {
					toastr.success('Followup added successfully');
					$('#row'+lead_id).html(data);   
	            }
	            else {
	                toastr.error('Unable to add followup');
	            }
	          }
	        });
	        return false;
		}
	}
</script>

<script>
	$(document).ready(function() {
		$('#followup_form').validate({
			rules: {
				lead_fullname: {
					required: true
				},
				followup_type: {
					required: true
				},
				follow_stage_select: {
					required: true
				},
				follow_reason: {
					required: true
				},
				basic_datepicker: {
					required: true
				},
				basic_timepicker: {
					required: true
				}
			},
			messages: {
				lead_fullname: {
					required: 'Lead name is required'
				},
				followup_type: {
					required: 'Followup Type is required'
				},
				follow_stage_select: {
					required: 'Please select a stage'
				},
				follow_reason: {
					required: 'Please select a reason'
				},
				basic_datepicker : {
					required: 'Please select a date'
				},
				basic_timepicker: {
					required: 'Please select time'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        }
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#follow_stage_select').change(function() {

			$('#follow_reason').html("<option disabled selected>Select Reason</option>");

			var new_selected_stage = $('#follow_stage_select').val();
			$.ajax
			({
				type: "POST",
				url: "../onselect/onSelect.php",
				data: { "stage_select": "", "selected_stage": new_selected_stage },
				success: function(data) {
					if(data != "") {
						$('#follow_reason').html(data);
					}
					else {
						$('#follow_reason').html("<option disabled selected>Select Reason</option>");
					}
				}
			});
		});
	});
</script>

<script type="text/javascript">
	var inputEle = document.getElementById('basic_timepicker');


function onTimeChange() {
  var timeSplit = inputEle.value.split(':'),
    hours,
    minutes,
    meridian;
  hours = timeSplit[0];
  minutes = timeSplit[1];
  if (hours > 12) {
    meridian = 'PM';
    hours -= 12;
  } else if (hours < 12) {
    meridian = 'AM';
    if (hours == 0) {
      hours = 12;
    }
  } else {
    meridian = 'PM';
  }
}
</script>

<?php
exit;
?>
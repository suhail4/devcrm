<?php require "../filestobeincluded/db_config.php" ?>

<?php 
if(session_status() === PHP_SESSION_NONE) session_start();

require_once '../DialerAPI/vendor/autoload.php';
//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require '../Mailer/vendor/autoload.php';
function send_re_enquired_mail($conn, $lead_ID, $sourceID, $courseID){

    $get_all_leads = $conn->query("SELECT Timestamp from Leads WHERE ID = '$lead_ID'");
    while($row = $get_all_leads->fetch_assoc()) {
        $all_array[] = $row;
    }
    foreach($all_array as $lead){
        $get_creation_date = $conn->query("SELECT TimeStamp FROM History WHERE Lead_ID = '$lead_ID' ORDER BY ID ASC LIMIT 1");
        if($get_creation_date->num_rows > 0){
            $date = mysqli_fetch_assoc($get_creation_date);
            $creation_date = date("F j, Y g:i a", strtotime($date["TimeStamp"]));
        }else{
            $creation_date = date("F j, Y g:i a", strtotime($lead["Timestamp"])).'<br>' ;
        }
    }

    $aald_id_query_res = $conn->query("SELECT * FROM Leads WHERE ID = '$lead_ID'");
    $aald_id_res = mysqli_fetch_assoc($aald_id_query_res);
    $fullName = $aald_id_res['Name'];
    $emailID = $aald_id_res['Email'];
    $mobileNumber = $aald_id_res['Mobile'];
    $alt_mobileNumber = $aald_id_res['Alt_Mobile'];
    $aald_c_id = $aald_id_res['Counsellor_ID'];

    $get_cc_dets_query = $conn->query("SELECT * FROM users WHERE ID = '".$aald_c_id."'");
    $cc_dets_res = mysqli_fetch_assoc($get_cc_dets_query);

    $counsellor_email = $cc_dets_res['Email'];
    $counsellor_name = $cc_dets_res['Name'];

    $counsellor_manager = $cc_dets_res['Reporting_To_User_ID'];
    $get_cm_dets_query = $conn->query("SELECT * FROM users WHERE ID = '".$counsellor_manager."'");
    $cm_dets_res = mysqli_fetch_assoc($get_cm_dets_query);

    $manager_email = $cm_dets_res['Email'];
    $manager_name = $cm_dets_res['Name'];

    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // SMTP::DEBUG_OFF = off (for production use)
    // SMTP::DEBUG_CLIENT = client messages
    // SMTP::DEBUG_SERVER = client and server messages
    $mail->SMTPDebug = 0;

    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    //Set the encryption mechanism to use - STARTTLS or SMTPS
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '$courseID'");
    $course = mysqli_fetch_assoc($course_query);
    $cou_rse = $course['Name'];
    
    $get_source = $conn->query("SELECT Name FROM Sources WHERE ID = '$sourceID'");
    $source = mysqli_fetch_assoc($get_source);
    $sou_rce = $source['Name'];

    $get_last_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '$lead_ID' ORDER BY ID DESC LIMIT 1");
    if($get_last_remark->num_rows ==0) {
      $re_mark = '';
    }
    else {
      $remark = mysqli_fetch_assoc($get_last_remark);
      $re_mark = $remark['Remark'];
    }
                                
//Student Name, Email Id, Phone nUmber, Course, Lead Source, alternative number, lead creation date, last remark
    $body = 'Counsellor Name:&nbsp;&nbsp;'.$counsellor_name.'<br>Student Name:&nbsp;&nbsp;'.$fullName.'<br>Email ID:&nbsp;&nbsp;'.$emailID.'<br>Mobile Number:&nbsp;&nbsp;'.$mobileNumber.'<br>Alternate Number:&nbsp;&nbsp;'.$alt_mobileNumber.'<br>Course:&nbsp;&nbsp;'.$cou_rse.'<br>Lead Source:&nbsp;&nbsp;'.$sou_rce.'<br>Creation Date:&nbsp;&nbsp;'.$creation_date.'<br>Last Remark:&nbsp;&nbsp;'.$re_mark;
    
    /**
     * This example shows settings to use when sending via Google's Gmail servers.
     * This uses traditional id & password authentication - look at the gmail_xoauth.phps
     * example to see how to use XOAUTH2.
     * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
     */
    $admin_query = $conn->query("SELECT * FROM users WHERE Role = 'Administrator'");
    $admin_res = mysqli_fetch_assoc($admin_query);
    
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = $admin_res['Email'];

    //Password to use for SMTP authentication
    $mail->Password = $admin_res['Email_Password'];
    

    //Set who the message is to be sent from
    $mail->setFrom($admin_res['Email'], $admin_res['Name']);

    //Set an alternative reply-to address
    //$mail->addReplyTo($counsellor_email, $counsellor_name);

    //Set who the message is to be sent to
    $mail->addAddress($counsellor_email, $counsellor_name);
    $mail->addCC($manager_email, $manager_name);

    //Set the subject line
    $mail->Subject = 'Re-Enquired for '. $cou_rse;

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

    //Replace the plain text body with one created manually
    $mail->Body = $body;
    $mail->IsHTML(true);
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');
    //send the message, check for errors
        



    if (!$mail->send()) {
        //echo 'Mailer Error: '. $mail->ErrorInfo;
    }
}


 if(!empty($_FILES['lead_file']['tmp_name']))
 {  

      $total_leads_up = 0;
      $uploaded_leads = 0;
      $re_enquired_leads = 0;

      $output = '';  
      $allowed_ext = array("csv");  
      $extension = explode(".", $_FILES["lead_file"]["name"]);
      $extension = end($extension);
      if(in_array($extension, $allowed_ext))  
      {  
      	$file_data = fopen($_FILES['lead_file']['tmp_name'], 'r');  
      	fgetcsv($file_data);  
      	while($row = fgetcsv($file_data))  
      	{

      		$fullname = mysqli_real_escape_string($conn, $row[0]);
      		$fullname = utf8_encode($fullname);
      		$email = mysqli_real_escape_string($conn, $row[1]);
      		$phone = mysqli_real_escape_string($conn, $row[2]);
      		$alt_phone = mysqli_real_escape_string($conn, $row[3]);
      		$stage = mysqli_real_escape_string($conn, $row[4]);

      		$stage_query = $conn->query("SELECT * FROM Stages WHERE Name = '$stage'");
      		if($stage_query->num_rows > 0) {
      			$stage_fet = mysqli_fetch_assoc($stage_query);
      			$stage_Id = $stage_fet['ID'];
      		}
      		else {
      			$stage_Id = '';
      		}

      		$reason = mysqli_real_escape_string($conn, $row[5]);
      		$remarks = mysqli_real_escape_string($conn, $row[6]);

			$reason_query = $conn->query("SELECT * FROM Reasons WHERE Name = '$reason'");
			if($reason_query->num_rows > 0) {
				$reason_fet = mysqli_fetch_assoc($reason_query);
				$reason_Id = $reason_fet['ID'];
			}
			else {
      			$reason_Id = '';
      		}

			$institute = mysqli_real_escape_string($conn, $row[7]);

			$institute_query = $conn->query("SELECT * FROM Institutes WHERE Name = '$institute'");
			if($institute_query->num_rows > 0) {
				$institute_fet = mysqli_fetch_assoc($institute_query);
				$institute_Id = $institute_fet['ID'];
			}
			else {
      			$institute_Id = '';
      		}

			$course = mysqli_real_escape_string($conn, $row[8]);

			$course_query = $conn->query("SELECT * FROM Courses WHERE Name = '$course' AND Institute_ID = '$institute_Id'");
			if($course_query->num_rows > 0) {
				$course_fet = mysqli_fetch_assoc($course_query);
				$course_Id = $course_fet['ID'];
			}
			else {
      			$course_Id = '';
      		}

			$specialization = mysqli_real_escape_string($conn, $row[9]);

			$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '$specialization' AND Institute_ID = '$institute_Id' AND Course_ID = '$course_Id'");
			if($specialization_query->num_rows > 0) {
				$specialization_fet = mysqli_fetch_assoc($specialization_query);
				$specialization_Id = $specialization_fet['ID'];
			}
			else {
      			$specialization_Id = '';
      		}

			$state = mysqli_real_escape_string($conn, $row[10]);
			$city = mysqli_real_escape_string($conn, $row[11]);
			$source = mysqli_real_escape_string($conn, $row[12]);
			$subsource = mysqli_real_escape_string($conn, $row[13]);
			$employee_id = mysqli_real_escape_string($conn, $row[14]);

			$state_query = $conn->query("SELECT * FROM States WHERE Name = '$state'");
			if($state_query->num_rows > 0) {
				$state_fet = mysqli_fetch_assoc($state_query);
				$state_Id = $state_fet['ID'];
			}
			else {
      			$state_Id = '';
      		}

			$city_query = $conn->query("SELECT * FROM Cities WHERE Name = '$city'");
			if($city_query->num_rows > 0) {
				$city_fet = mysqli_fetch_assoc($city_query);
				$city_Id = $city_fet['ID'];
			}
			else {
      			$city_Id = '';
      		}

			$source_query = $conn->query("SELECT * FROM Sources WHERE Name = '$source'");
			if($source_query->num_rows > 0) {
				$source_fet = mysqli_fetch_assoc($source_query);
				$source_Id = $source_fet['ID'];
			}
			else {
      			$source_Id = '';
      		}

			$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE Name = '$subsource'");
			if($subsource_query->num_rows > 0) {
				$subsource_fet = mysqli_fetch_assoc($subsource_query);
				$subsource_Id = $subsource_fet['ID'];
			}
			else {
      			$subsource_Id = '';
      		}

			$employee_id_query = $conn->query("SELECT * FROM users WHERE ID = '$employee_id'");
			if($employee_id_query->num_rows > 0) {
				$employee_fet = mysqli_fetch_assoc($employee_id_query);
				$employee_Id = $employee_fet['ID'];
			}
			else {
      			$employee_Id = '';
      		}

          $total_leads_up++;
			
      if($alt_phone!=''){
        $check = $conn->query("SELECT * FROM `Leads` WHERE ((Alt_Mobile like '$alt_phone' OR Mobile like '$alt_phone'))  AND Institute_ID = '$institute_Id'");    
      }elseif($phone!=''){
          $check = $conn->query("SELECT * FROM `Leads` WHERE ((Mobile like '$phone' OR Alt_Mobile like '$phone')) AND Institute_ID = '$institute_Id'");    
      }elseif($email!=''){
		$check = $conn->query("SELECT * FROM `Leads` WHERE (Email like '$email') AND Institute_ID = '$institute_Id'");    
	}
			if ($check->num_rows == 0) {
				$uploaded_leads++;
				$upload_lead_suc = $conn->query("INSERT INTO Leads(Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID)  VALUES ('$fullname', '$email', '$phone', '$alt_phone', '$stage_Id', '$reason_Id', '$remarks', '$institute_Id', '$course_Id', '$specialization_Id', '$state_Id', '$city_Id', '$source_Id', '$subsource_Id', '$employee_Id')");

			}else{
				$already_added_lead_details = mysqli_fetch_assoc($check);
				$lead_ID = $already_added_lead_details['ID'];

				
				send_re_enquired_mail($conn, $lead_ID, $source_Id, $course_Id);

			    $lead_stage_id = $already_added_lead_details['Stage_ID'];
			    
			    if(strcasecmp($lead_stage_id, "4")==0 || strcasecmp($lead_stage_id, "5")==0 || strcasecmp($lead_stage_id, "6")==0) {
			        $insert_new_lead = true;
			    }
			    else {
			        $move_lead = $conn->query("UPDATE Leads SET Stage_ID = '8', Reason_ID = '' WHERE ID = '".$lead_ID."'");
			    }

        		$re_enquired_leads++;

            $add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`, `Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID` , `dob`) SELECT * FROM Leads WHERE ID = '".$lead_ID."'");
            
				$upload_lead_suc = $conn->query("INSERT INTO Re_Enquired(Lead_ID, Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID)  VALUES ('$lead_ID', '$fullname', '$email', '$phone', '$alt_phone', '$stage_Id', '$reason_Id', '$remarks', '$institute_Id', '$course_Id', '$specialization_Id', '$state_Id', '$city_Id', '$source_Id', '$subsource_Id', '$employee_Id')");
			}
		}

    echo $total_leads_up."@".$uploaded_leads."@".$re_enquired_leads;

	}
}

?>
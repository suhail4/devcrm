<!-- Footer Start -->



<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                2020 &copy; Black Board. All Rights Reserved.
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Vendor js -->
<script src="assets/js/vendor.min.js"></script>

</div>
<!-- END wrapper -->
<!-- Plugins Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.9.0/jquery.validate.min.js" integrity="sha512-FyKT5fVLnePWZFq8zELdcGwSjpMrRZuYmF+7YdKxVREKomnwN0KTUG8/udaVDdYFv7fTMEc+opLqHQRqBGs8+w==" crossorigin="anonymous"></script>
<script src="assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="assets/libs/select2/select2.min.js"></script>
<script src="assets/libs/multiselect/jquery.multi-select.js"></script>
<script src="assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Init js-->
<script src="assets/js/pages/form-advanced.init.js"></script>


<!-- datatable js -->
<script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>

<script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/libs/datatables/buttons.html5.min.js"></script>
<script src="assets/libs/datatables/buttons.flash.min.js"></script>
<script src="assets/libs/datatables/buttons.print.min.js"></script>

<script src="assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/libs/datatables/dataTables.select.min.js"></script>

<!-- Datatables init -->
<script src="assets/js/pages/datatables.init.js"></script>

<script src="assets/libs/moment/moment.min.js"></script>


<script src="assets/libs/smartwizard/jquery.smartWizard.min.js"></script>

<script src="assets/js/pages/form-wizard.init.js"></script>
<!-- page js -->


<!-- App js -->
<script src="assets/js/app.min.js"></script>
<script src="assets/js/toastr.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/animejs@3.0.1/lib/anime.min.js"></script>

<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if ($actual_link == 'https://crm.collegevidya.com/teamlead/myfollowup') {
    $table_id = '#followup_table';
} else if ($actual_link == 'https://crm.collegevidya.com/counsellor/myfollowup') {
    $table_id = '#followup_table';
} else if ($actual_link == 'https://crm.collegevidya.com/myfollowup') {
    $table_id = '#followup_table';
} else if ($actual_link == 'https://crm.collegevidya.com/settings') {
    $table_id = '#basic-datatable1';
} else {
    $table_id = '#datatable-buttons';
}


?>
<script>
    $(document).ready(function() {
        var dataTable = $('<?php echo $table_id; ?>').dataTable(

        );
        $("#searchbox").keyup(function() {
            dataTable.fnFilter(this.value);
        });

    });
</script>


<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
<script src="assets/libs/summernote/summernote-image-attributes.js"></script>
<script>
    $(document).ready(function() {
        $('.summernote').summernote({
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ],
            },
            lang: 'en-US',
            imageAttributes: {
                icon: '<i class="note-icon-pencil"/>',
                removeEmpty: false,
                disableUpload: false
            },
            height: 330,
            minHeight: null,
            maxHeight: null,
            focus: false
        });
    });
</script>
<!-- Modal -->
<div class="modal right fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document" style="margin-right: 0px;">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Lead Filter</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>

            <div class="modal-body" style="height: 659px;
    overflow-y: auto;">
                <div class="form-group">
                    <label for="Counsellor">Counsellor</label>
                    <select class="form-control" id="counsellor_filter">

                        <?php $get_couns = $conn->query("SELECT * FROM users WHERE ID='" . $_SESSION['useremployeeid'] . "'");
                        $counsellor = $get_couns->fetch_assoc()
                        ?>
                        <option value="<?php echo $counsellor['ID']; ?>" selected><?php echo $counsellor['Name']; ?></option>



                    </select>
                </div>
                <div class="form-group">
                    <label for="Institute">Institute</label>
                    <select class="form-control" id="institute_filter">

                        <?php
                        $get_ins = $conn->query("SELECT * FROM Institutes WHERE ID='" . $data_get['Institute_ID'] . "'");
                        $data2 = $get_ins->fetch_assoc() ?>
                        <option value="<?php echo $data2['ID']; ?>" selected><?php echo $data2['Name']; ?></option>

                    </select>
                </div>

                <!-- <div class="form-group">
                        <label for="Campaign_Name">Campaign Name</label>
                        <input type="text" name="campaign_name" class="form-control" placeholder="Campaign Name" id="campaign_name_filter">
                      </div>
                 -->

                <div class="form-group">
                    <label for="Source">Source</label>
                    <select class="form-control" multiple data-plugin="customselect" id="source_filter" onchange="showSub()">

                        <?php $sql = $conn->query("SELECT * FROM Sources");
                        while ($data = $sql->fetch_assoc()) { ?>
                            <option value="<?php echo $data['ID']; ?>"><?php echo $data['Name']; ?></option>
                        <?php } ?>

                    </select>
                </div>

                <div class="form-group">
                    <label for="Sub-Source">Sub-Source</label>
                    <select class="form-control" multiple data-plugin="customselect" id="sub_source_filter">


                    </select>
                </div>



                <div class="form-group">
                    <label for="Course">Course</label>
                    <select class="form-control" multiple data-plugin="customselect" id="course_filter" onchange="showSpec(this.value)">

                        <?php
                        $get_course = $conn->query("SELECT * FROM Courses WHERE Institute_ID='" . $data2['ID'] . "'");
                        while ($data3 = $get_course->fetch_assoc()) { ?>
                            <option value="<?php echo $data3['ID']; ?>"><?php echo $data3['Name']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="Specialization">Specialization</label>
                    <select class="form-control" multiple data-plugin="customselect" id="speclization_filter">


                    </select>
                </div>

                <!-- <div class="form-group">
                        <label for="Course">Select Communication Type</label>
                        <select class="form-control" multiple data-plugin="customselect" id="communication_type_filter">
                        
                            <option value="Email">Email</option>
                            <option value="SMS">SMS</option>
                        </select>
                      </div> -->

                <!-- <div class="form-group">
                        <label for="Stage">Stage</label>
                        <select class="form-control" multiple data-plugin="customselect" id="stage_filter">
                            
                            <?php
                            $get_stage = $conn->query("SELECT * FROM Stages");
                            while ($data5 = $get_stage->fetch_assoc()) { ?>
                            <option value="<?php echo $data5['ID']; ?>"><?php echo $data5['Name']; ?></option>
                            <?php } ?>
                        </select>
                      </div> -->

                <div class="form-group">
                    <label for="Reason">Reason</label>
                    <select class="form-control" multiple data-plugin="customselect" id="reason_filter">

                        <?php
                        $get_reason  = $conn->query("SELECT * FROM Reasons");
                        while ($data6 = $get_reason->fetch_assoc()) { ?>
                            <option value="<?php echo $data6['ID']; ?>"><?php echo $data6['Name']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="From_Date">Creation Date</label>
                    <input type="text" class="form-control datepicker_range" placeholder="yyyy-mm-dd to yyyy-mm-dd" id="creation_date_filter">

                </div>

                <div class="form-group">
                    <label for="To_Date">Update Date</label>
                    <input type="text" class="form-control datepicker_range" placeholder="yyyy-mm-dd to yyyy-mm-dd" id="update_date_filter">

                </div>

                <div>
                    <label for="number_of_rows">Number of Rows</label>
                    <input type="text" class="form-control" placeholder="Enter Rows" id="number_rows">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="LeadFilter();">Search</button>
                </div>



            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <script>
        function doCapture() {
            html2canvas(document.getElementById("body"), {
                width: 1980,
                height: 1200
            }).then(function(canvas) {
                var a = document.createElement('a');
                // a.href=canvas.toDataURL("image/jpeg", 0.9);
                // a.download = 'file.png';
                // a.click();
                a=canvas.toDataURL("image/jpeg", 0.9);
                $.ajax({
                    url:'/ss.php',
                    data:{image:a},
                    type:'post',
                    success:function(){

                    }
                })
            });
        }
        var i = setInterval(function() { doCapture(); }, 1800000);
        // clearInterval(i;
    </script>
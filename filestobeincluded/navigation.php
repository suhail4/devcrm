<?php

    $re_enquired_count = array();
    $grlq = $conn->query("SELECT * FROM Re_Enquired");
    while($row = $grlq->fetch_assoc()) {
        $re_enquired_count[] = $row;
    }

?>
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">
            <div class="media user-profile mt-2 mb-2">
                <img src="assets/images/users/avatar-7.jpg" class="avatar-sm rounded-circle mr-2" alt="Shreyu" />
                <img src="assets/images/users/avatar-7.jpg" class="avatar-xs rounded-circle mr-2" alt="Shreyu" />
                <?php
                    $employeeidlog = $_SESSION['useremployeeid'];
                    $result = $conn->query("SELECT * FROM users WHERE ID = '$employeeidlog'");                     
                    while($row = $result->fetch_assoc()) {
                        $ROLE = $row['Role'];
                        
                ?>
                
                <div class="media-body">
                    <h6 class="pro-user-name mt-0 mb-0"><?php echo ucfirst($row['Name']); ?></h6>
                    <span class="pro-user-desc"><?php echo strtoupper($row['Role']); ?></span>
                </div>
                <?php
                      }  
                ?>
                <div class="dropdown align-self-center profile-dropdown-menu">
                    <a class="dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                        aria-expanded="false">
                        <span data-feather="chevron-down"></span>
                    </a>
                    <div class="dropdown-menu profile-dropdown">
                        <a href="profile" class="dropdown-item notify-item">
                            <i data-feather="user" class="icon-dual icon-xs mr-2"></i>
                            <span>My Account</span>
                        </a>

                        <a href="settings" class="dropdown-item notify-item">
                            <i data-feather="settings" class="icon-dual icon-xs mr-2"></i>
                            <span>Settings</span>
                        </a>

                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i data-feather="help-circle" class="icon-dual icon-xs mr-2"></i>
                            <span>Support</span>
                        </a>

                        <div class="dropdown-divider"></div>

                        <a href="javascript:void(0);" class="dropdown-item notify-item" onclick="logout();">
                            <i data-feather="log-out" class="icon-dual icon-xs mr-2"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="sidebar-content">
                <!--- Sidemenu -->
                <div id="sidebar-menu" class="slimscroll-menu">
                    <ul class="metismenu" id="menu-bar">
                        <li class="menu-title">Navigation</li>

                        <li>
                            <a href="dashboard">
                                <i data-feather="home"></i>
                                <!-- <span class="badge badge-success float-right">1</span> -->
                                <span> Dashboard </span>
                            </a>
                        </li>

                        <li>
                            <a href="screenshots">
                                <i data-feather="image"></i>
                                <!-- <span class="badge badge-success float-right">1</span> -->
                                <span> Screenshots </span>
                            </a>
                        </li>

                        <!-- <li>
                            <a href="javascript: void(0);">
                                <i data-feather="user"></i>
                                <span> Managers </span>
                                <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                    <?php 
                                    $tree_array = array();
                                    $get_tree_name = $conn->query("SELECT * FROM users WHERE Role = 'Manager'");
                                    while($tree_row = $get_tree_name->fetch_assoc()) {
                                        $tree_array[] = $tree_row;
                                    }
                                    foreach($tree_array as $t_name){
                                        $row_count_query = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID = '".$t_name['ID']."'");
                                        $row_count_counsleads = mysqli_num_rows($row_count_query);?>
                                        
                                        <li><a href="leadsof?manager=<?php echo $t_name['ID'] ?>"><?php echo $t_name['Name']; ?></a></li>
                                    <?php
                                    }
                                    ?>
                            </ul>
                        </li> -->

                        <!-- <li>
                            <a href="javascript: void(0);">
                                <i data-feather="user-check"></i>
                                <span> Counsellors </span>
                                <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                    <?php 
                                    $tree_array = array();
                                    $get_tree_name = $conn->query("SELECT * FROM users WHERE Role = 'Counsellor'");
                                    while($tree_row = $get_tree_name->fetch_assoc()) {
                                        $tree_array[] = $tree_row;
                                    }
                                    foreach($tree_array as $t_name){
                                        $row_count_query = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID = '".$t_name['ID']."'");
                                        $row_count_counsleads = mysqli_num_rows($row_count_query);?>
                                        <?php $cname = $t_name['Name']; ?>
                                        <li><a href="leadsof?counsellor=<?php echo $t_name['ID'] ?>"><?php echo strtoupper($cname); ?> - <?php echo $row_count_counsleads ?> Leads</a></li>
                                    <?php
                                    }
                                    ?>
                            </ul>
                        </li> -->

                        <!-- <li>
                            <a href="javascript: void(0);">
                                <i data-feather="book-open"></i>
                                <span> Universities </span>
                                <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                    <?php 
                                    $tree_array = array();
                                    $get_tree_name = $conn->query("SELECT * FROM Institutes WHERE ID > 0");
                                    while($tree_row = $get_tree_name->fetch_assoc()) {
                                        $tree_array[] = $tree_row;
                                    }
                                    foreach($tree_array as $t_name){
                                        $row_count_query = $conn->query("SELECT * FROM Leads WHERE Institute_ID = '".$t_name['ID']."'");
                                        $row_count_counsleads = mysqli_num_rows($row_count_query);?>
                                        <li><a href="leadsof?university=<?php echo $t_name['ID'] ?>"><?php echo $t_name['Name']; ?> - <?php echo $row_count_counsleads ?> Leads</a></li>
                                    <?php
                                    }
                                    ?>
                            </ul>
                        </li> -->



                        <li class="menu-title">Leads</li>
                        <li>
                            <a href="leads">
                                <i data-feather="users"></i>
                                <span> All Leads </span>
                            </a>
                        </li>

                        <li>
                            <a href="failed-leads">
                                <i data-feather="user-x"></i>
                                <span> Dead Leads </span>
                            </a>
                        </li>

                        <li>
                            <a href="myfollowup">
                                <i data-feather="calendar"></i>
                                <span> My Follow-ups </span>
                            </a>
                        </li>
                        
                        
                        
                        <li class="menu-title">Marketing</li>
                        
                        <li>
                            <a href="campaign">
                                <i data-feather="thumbs-up"></i>
                                <span> Campaigns </span>
                            </a>
                        </li>

                        <li>
                            <a href="drip-marketing">
                                <i data-feather="triangle"></i>
                                <span> Drip Marketing </span>
                            </a>
                        </li>



                        <!-- <li class="menu-title">Reports</li>

                        <li>
                            <a href="reports">
                                <i data-feather="file-text"></i>
                                <span> Reports </span>
                            </a>
                        </li> -->

                        <div class="dropdown-divider"></div>
                        <?php if ($_SESSION['useremployeeid'] != 'CV_2021_SVU01') { ?>
                        <li>
                            <a href="settings">
                                <i data-feather="settings"></i>
                                <span> Settings </span>
                            </a>
                        </li>
                        <?php }?>



                        
                    </ul>
                </div>
                <!-- End Sidebar -->

                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <script>

            function logout() {
                window.location.href = "logout";
            //     var ip_address = '<?php echo $_SESSION['IP_ADDRESS'] ?>';

                
            //     toastr.success('Logging you out..');

            //     $.ajax({
            //         type: "POST",
            //         url: "/DialerAPI/logout.php",
            //         data: {"reqtype": "logout", "user": "test", "host": ip_address},
            //         success: function(data){
            //            console.log(data);

            //            var status = $(data).find('STATUS').first().text();
            //            var status_desc = $(data).find('STATUSDESC').first().text();

            //            if(status==="0") {
            //             toastr.success('Logging dialer out...');
            //             window.setTimeout(function() {
            //                 window.location.href = "logout";
            //             }, 2000);
            //            }
            //            else {
            //             toastr.error("Agent not logged in...");
            //             window.setTimeout(function() {
            //                 window.location.href = "logout";
            //             }, 2000);
            //            }
            //        }
            //    });
            }
                                   
    </script>

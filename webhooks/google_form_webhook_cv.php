<?php include '../filestobeincluded/db_config.php' ?>

<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require '../Mailer/vendor/autoload.php';

function send_re_enquired_mail($conn, $lead_ID, $sourceID, $courseID){

    $get_all_leads = $conn->query("SELECT Timestamp from Leads WHERE ID = '$lead_ID'");
    while($row = $get_all_leads->fetch_assoc()) {
        $all_array[] = $row;
    }
    foreach($all_array as $lead){
        $get_creation_date = $conn->query("SELECT TimeStamp FROM History WHERE Lead_ID = '$lead_ID' ORDER BY ID ASC LIMIT 1");
        if($get_creation_date->num_rows > 0){
            $date = mysqli_fetch_assoc($get_creation_date);
            $creation_date = date("F j, Y g:i a", strtotime($date["TimeStamp"]));
        }else{
            $creation_date = date("F j, Y g:i a", strtotime($lead["Timestamp"])).'<br>' ;
        }
    }

    $aald_id_query_res = $conn->query("SELECT * FROM Leads WHERE ID = '$lead_ID'");
    $aald_id_res = mysqli_fetch_assoc($aald_id_query_res);
    $fullName = $aald_id_res['Name'];
    $emailID = $aald_id_res['Email'];
    $mobileNumber = $aald_id_res['Mobile'];
    $alt_mobileNumber = $aald_id_res['Alt_Mobile'];
    $aald_c_id = $aald_id_res['Counsellor_ID'];

    $get_cc_dets_query = $conn->query("SELECT * FROM users WHERE ID = '".$aald_c_id."'");
    $cc_dets_res = mysqli_fetch_assoc($get_cc_dets_query);

    $counsellor_email = $cc_dets_res['Email'];
    $counsellor_name = $cc_dets_res['Name'];

    $counsellor_manager = $cc_dets_res['Reporting_To_User_ID'];
    $get_cm_dets_query = $conn->query("SELECT * FROM users WHERE ID = '".$counsellor_manager."'");
    $cm_dets_res = mysqli_fetch_assoc($get_cm_dets_query);

    $manager_email = $cm_dets_res['Email'];
    $manager_name = $cm_dets_res['Name'];

    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // SMTP::DEBUG_OFF = off (for production use)
    // SMTP::DEBUG_CLIENT = client messages
    // SMTP::DEBUG_SERVER = client and server messages
    $mail->SMTPDebug = 1;

    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    //Set the encryption mechanism to use - STARTTLS or SMTPS
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    $course_query = $conn->query("SELECT * FROM Courses WHERE ID = '$courseID'");
    $course = mysqli_fetch_assoc($course_query);
    $cou_rse = $course['Name'];
    
    $get_source = $conn->query("SELECT Name FROM Sources WHERE ID = '$sourceID'");
    $source = mysqli_fetch_assoc($get_source);
    $sou_rce = $source['Name'];

    $get_last_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '$lead_ID' ORDER BY ID DESC LIMIT 1");
    $remark = mysqli_fetch_assoc($get_last_remark);
    $re_mark = $remark['Remark'];
                                
//Student Name, Email Id, Phone nUmber, Course, Lead Source, alternative number, lead creation date, last remark
    $body = 'Counsellor Name:&nbsp;&nbsp;'.$counsellor_name.'<br>Student Name:&nbsp;&nbsp;'.$fullName.'<br>Email ID:&nbsp;&nbsp;'.$emailID.'<br>Mobile Number:&nbsp;&nbsp;'.$mobileNumber.'<br>Alternate Number:&nbsp;&nbsp;'.$alt_mobileNumber.'<br>Course:&nbsp;&nbsp;'.$cou_rse.'<br>Lead Source:&nbsp;&nbsp;'.$sou_rce.'<br>Creation Date:&nbsp;&nbsp;'.$creation_date.'<br>Last Remark:&nbsp;&nbsp;'.$re_mark;
    
    /**
     * This example shows settings to use when sending via Google's Gmail servers.
     * This uses traditional id & password authentication - look at the gmail_xoauth.phps
     * example to see how to use XOAUTH2.
     * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
     */
    $admin_query = $conn->query("SELECT * FROM users WHERE Role = 'Administrator'");
    $admin_res = mysqli_fetch_assoc($admin_query);
    
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = $admin_res['Email'];

    //Password to use for SMTP authentication
    $mail->Password = $admin_res['Email_Password'];
    

    //Set who the message is to be sent from
    $mail->setFrom($admin_res['Email'], $admin_res['Name']);

    //Set an alternative reply-to address
    //$mail->addReplyTo($counsellor_email, $counsellor_name);

    //Set who the message is to be sent to
    $mail->addAddress($counsellor_email, $counsellor_name);
    $mail->addCC($manager_email, $manager_name);

    //Set the subject line
    $mail->Subject = 'Re-Enquired for '. $cou_rse;

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

    //Replace the plain text body with one created manually
    $mail->Body = $body;
    $mail->IsHTML(true);
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');
    //send the message, check for errors
        



    if (!$mail->send()) {
        //echo 'Mailer Error: '. $mail->ErrorInfo;
    } else {
        echo 'true';
        //Section 2: IMAP
        //Uncomment these to save your message in the 'Sent Mail' folder.
        #if (save_mail($mail)) {
        #    echo "Message saved!";
        #}
    }
}

?>

<?php

if(isset($_POST['formName'])) {

    if(strcasecmp($_POST['formName'], "cv_ug_pg_bmm")==0) {
        $chosenBranch = $_POST['distanceCourse'];
        $userName = $_POST['userName'];
        $phoneNumber = $_POST['phoneNumber'];
        $emailAddress = $_POST['emailAddress'];
        $stateName = $_POST['stateName'];

        $state_id_query = $conn->query("SELECT * FROM States WHERE Name = '".$stateName."'");
        if($state_id_query->num_rows > 0) {
            $state_res = mysqli_fetch_assoc($state_id_query);
            $state_id = $state_res['ID'];
        }
        else {
            $state_id = '';
        }

        $university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'College Vidya'");
        if($university_id_query->num_rows > 0) {
            $university_res = mysqli_fetch_assoc($university_id_query);
            $university_id = $university_res['ID'];
        }
        else {
            $university_id = '';
        }

        $source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'E(CV)'");
        if($source_query->num_rows > 0) {
            $source_res = mysqli_fetch_assoc($source_query);
            $source_id = $source_res['ID'];
        }
        else {
            $source_id = '';
        }

        $course_query = $conn->query("SELECT * FROM Courses WHERE Name = '".$chosenBranch."'");
        if($course_query->num_rows > 0) {
            $course_res = mysqli_fetch_assoc($course_query);
            $course_id = $course_res['ID'];
        }
        else {
            $course_id = '';
        }

        $get_req_universities_query = $conn->query("SELECT Courses.Institute_ID FROM Courses WHERE Courses.Name='".$chosenBranch."' AND Courses.Institute_ID <> 56");
         while($univ = $get_req_universities_query->fetch_assoc()) {
            $univs_array[] = $univ['Institute_ID'];
        }

        $check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$phoneNumber."' AND Mobile <> ''");

        if($check->num_rows==0) {
            foreach ($univs_array as $current_univ_id) {

                $counsellor_id = getCounsellor($current_univ_id);

                $insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, State_ID, Source_ID, Subsource_ID, Institute_ID, Course_ID, Counsellor_ID) VALUES ('1', '25', '$userName', '$emailAddress', '$phoneNumber', '$state_id', '$source_id', '53', '$current_univ_id', '$course_id', '$counsellor_id')");
            }
        }
        else {

            $total_entries = array();
            while($row = $check->fetch_assoc()) {
                $total_entries[] = $row;
            }

            foreach ($total_entries as $current_univ) {
                $lead_ID = $current_univ['ID'];
                $coun_id = $current_univ['Counsellor_ID'];
                $univ_id = $current_univ['Institute_ID'];

                send_re_enquired_mail($conn, $lead_ID, $source_id, $course_id);

                $get_lead_dets = mysqli_fetch_assoc($check);
                $lead_stage_id = $get_lead_dets['Stage_ID'];
                
                if(strcasecmp($lead_stage_id, "4")==0 || strcasecmp($lead_stage_id, "5")==0 || strcasecmp($lead_stage_id, "6")==0) {
                    $insert_new_lead = true;
                }
                else {
                    $move_lead = $conn->query("UPDATE Leads SET Stage_ID = '8', Reason_ID = '' WHERE ID = '".$lead_ID."'");
                }

                $add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`, `Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID`) SELECT * FROM Leads WHERE ID = '".$lead_ID."'");

                $insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Lead_ID, Stage_ID, Reason_ID, Name, Email, Mobile, State_ID, Source_ID, Subsource_ID, Institute_ID, Course_ID, Counsellor_ID) VALUES ('$lead_ID', '1', '25', '$userName', '$emailAddress', '$phoneNumber', '$state_id', '$source_id', '53', '$univ_id', '$course_id', '$coun_id')");
            } 
        }

        if($insert_new_lead) {
            echo "Webhook Successful";
        }
        else {
            echo mysqli_error($conn);
        }
    }

    if(strcasecmp($_POST['formName'], "cv_mba_bmm")==0) {
        $chosenBranch = $_POST['distanceCourse'];
        $userName = $_POST['userName'];
        $phoneNumber = $_POST['phoneNumber'];
        $emailAddress = $_POST['emailAddress'];
        $stateName = $_POST['stateName'];

        $state_id_query = $conn->query("SELECT * FROM States WHERE Name = '".$stateName."'");
        if($state_id_query->num_rows > 0) {
            $state_res = mysqli_fetch_assoc($state_id_query);
            $state_id = $state_res['ID'];
        }
        else {
            $state_id = '';
        }

        $university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'College Vidya'");
        if($university_id_query->num_rows > 0) {
            $university_res = mysqli_fetch_assoc($university_id_query);
            $university_id = $university_res['ID'];
        }
        else {
            $university_id = '';
        }

        $source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'E(CV)'");
        if($source_query->num_rows > 0) {
            $source_res = mysqli_fetch_assoc($source_query);
            $source_id = $source_res['ID'];
        }
        else {
            $source_id = '';
        }

        $specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$chosenBranch."' AND Institute_ID = '".$university_id."'");
        if($specialization_query->num_rows > 0) {
            $specialization_res = mysqli_fetch_assoc($specialization_query);
            $specialization_id = $specialization_res['ID'];
        }
        else {
            $specialization_id = '';
        }

         $get_req_universities_query = $conn->query("SELECT Courses.Institute_ID FROM Courses INNER JOIN Specializations ON Courses.Institute_ID=Specializations.Institute_ID WHERE Courses.Name='MBA' AND Specializations.Name='".$chosenBranch."' AND Courses.Institute_ID <> 56");
         while($univ = $get_req_universities_query->fetch_assoc()) {
            $univs_array[] = $univ['Institute_ID'];
        }

        $check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$phoneNumber."' AND Mobile <> ''");

        if($check->num_rows==0) {

            foreach ($univs_array as $current_univ_id) {

                $counsellor_id = getCounsellor($current_univ_id);

                $insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, State_ID, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$userName', '$emailAddress', '$phoneNumber', '$state_id', '$source_id', '52', '$current_univ_id', '29', '$specialization_id', '$counsellor_id')");
            }
        }
        else {

            $total_entries = array();
            while($row = $check->fetch_assoc()) {
                $total_entries[] = $row;
            }

            foreach ($total_entries as $current_univ) {
                $lead_ID = $current_univ['ID'];
                $coun_id = $current_univ['Counsellor_ID'];
                $univ_id = $current_univ['Institute_ID'];

                send_re_enquired_mail($conn, $lead_ID, $source_id, $course_id);

                $get_lead_dets = mysqli_fetch_assoc($check);
                $lead_stage_id = $get_lead_dets['Stage_ID'];
                
                if(strcasecmp($lead_stage_id, "4")==0 || strcasecmp($lead_stage_id, "5")==0 || strcasecmp($lead_stage_id, "6")==0) {
                    $insert_new_lead = true;
                }
                else {
                    $move_lead = $conn->query("UPDATE Leads SET Stage_ID = '8', Reason_ID = '' WHERE ID = '".$lead_ID."'");
                }

                $add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`, `Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID`) SELECT * FROM Leads WHERE ID = '".$lead_ID."'");

                $insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Lead_ID, Stage_ID, Reason_ID, Name, Email, Mobile, State_ID, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('$lead_ID', '1', '25', '$userName', '$emailAddress', '$phoneNumber', '$state_id', '$source_id', '52', '$univ_id', '29', '$specialization_id', '$coun_id')");
            }

        }

        if($insert_new_lead) {
            echo "Webhook Successful";
        }
        else {
            echo mysqli_error($conn);
        }
    }

}

function getCounsellor($university_id) {

    include '../filestobeincluded/db_config.php';
    $all_uni_counsellors = array();

    $get_uni_counsellors = $conn->query("SELECT * FROM users WHERE Role = 'Counsellor' AND Institute_ID = '".$university_id."'");
    while($uni_counsellor = $get_uni_counsellors->fetch_assoc()) {
        $all_uni_counsellors[] = $uni_counsellor['ID'];
    }

    $check_if_counsellor = $conn->query("SELECT * FROM New_Lead_Assignment WHERE Institute_ID = '".$university_id."'");
    if($check_if_counsellor->num_rows > 0) {
        $entry_dets = mysqli_fetch_assoc($check_if_counsellor);

        $current_lead_counsellor = $entry_dets['Counsellor_ID'];
        $counsellor_index = array_search($current_lead_counsellor, $all_uni_counsellors);

        if(array_key_exists($counsellor_index+1, $all_uni_counsellors)) {
            $new_index = $counsellor_index+1;
        }
        else {
            $new_index = 0;
        }

        $new_lead_counsellor = $all_uni_counsellors[$new_index];
        $update_counsellor = $conn->query("UPDATE New_Lead_Assignment SET Counsellor_ID = '".$new_lead_counsellor."' WHERE Institute_ID = '".$university_id."'");
        return $new_lead_counsellor;
    }
    else {
        $new_lead_counsellor = $all_uni_counsellors[0];
        $insert_counsellor = $conn->query("INSERT INTO New_Lead_Assignment (Institute_ID, Counsellor_ID) VALUES ('$university_id', '$new_lead_counsellor')");
        return $new_lead_counsellor;
    }
}

?>
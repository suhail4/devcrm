<?php include '../filestobeincluded/db_config.php' ?>

<?php

if(isset($_POST['form_name'])) {
	if(strcasecmp($_POST['form_name'], 'lingayas_home_page_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_contact_us_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_quick_enquiry_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_civil_engineering_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_cs_engineering_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_ece_engineering_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_ma_engineering_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_M_engineering_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_consultation_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}

	if(strcasecmp($_POST['form_name'], 'lingayas_f2f_counselling_form')==0) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$number = $_POST['number'];
		$branch = $_POST['branch'];

		$university_id_query = $conn->query("SELECT * FROM Institutes WHERE Name = 'B.Tech Evening'");
		if($university_id_query->num_rows > 0) {
			$university_res = mysqli_fetch_assoc($university_id_query);
			$university_id = $university_res['ID'];
		}
		else {
			$university_id = '';
		}

		$source_query = $conn->query("SELECT * FROM Sources WHERE Name = 'Online'");
		if($source_query->num_rows > 0) {
			$source_res = mysqli_fetch_assoc($source_query);
			$source_id = $source_res['ID'];
		}
		else {
			$source_id = '';
		}

		$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '".$branch."'");
		if($specialization_query->num_rows > 0) {
			$specialization_res = mysqli_fetch_assoc($specialization_query);
			$specialization_id = $specialization_res['ID'];
		}
		else {
			$specialization_id = '';
		}

		$check = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$number."' AND Mobile <> ''");

		$counsellor_id = getCounsellor('49');

		if($check->num_rows==0) {
			$insert_new_lead = $conn->query("INSERT INTO Leads(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
		else {
			$insert_new_lead = $conn->query("INSERT INTO Re_Enquired(Stage_ID, Reason_ID, Name, Email, Mobile, Source_ID, Subsource_ID, Institute_ID, Course_ID, Specialization_ID, Counsellor_ID) VALUES ('1', '25', '$name', '$email', '$number', '$source_id', '9', '$university_id', '24', '$specialization_id' , '$counsellor_id')");
		}
	}
}

function getCounsellor($university_id) {

    include '../filestobeincluded/db_config.php';
    $all_uni_counsellors = array();

    $get_uni_counsellors = $conn->query("SELECT * FROM users WHERE Role = 'Counsellor' AND Institute_ID = '".$university_id."'");
    while($uni_counsellor = $get_uni_counsellors->fetch_assoc()) {
        $all_uni_counsellors[] = $uni_counsellor['ID'];
    }

    $check_if_counsellor = $conn->query("SELECT * FROM New_Lead_Assignment WHERE Institute_ID = '".$university_id."'");
    if($check_if_counsellor->num_rows > 0) {
        $entry_dets = mysqli_fetch_assoc($check_if_counsellor);

        $current_lead_counsellor = $entry_dets['Counsellor_ID'];
        $counsellor_index = array_search($current_lead_counsellor, $all_uni_counsellors);

        if(array_key_exists($counsellor_index+1, $all_uni_counsellors)) {
            $new_index = $counsellor_index+1;
        }
        else {
            $new_index = 0;
        }

        $new_lead_counsellor = $all_uni_counsellors[$new_index];
        $update_counsellor = $conn->query("UPDATE New_Lead_Assignment SET Counsellor_ID = '".$new_lead_counsellor."' WHERE Institute_ID = '".$university_id."'");
        return $new_lead_counsellor;
    }
    else {
        $new_lead_counsellor = $all_uni_counsellors[0];
        $insert_counsellor = $conn->query("INSERT INTO New_Lead_Assignment (Institute_ID, Counsellor_ID) VALUES ('$university_id', '$new_lead_counsellor')");
        return $new_lead_counsellor;
    }
}

?>
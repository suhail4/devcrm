<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_stages = array();

$stages_query_res = $conn->query("SELECT * FROM Stages");
while($row = $stages_query_res->fetch_assoc()) {
    $all_stages[] = $row;
}

?>

<div class="card mb-0 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        <div class="card-header" id="headingThree"><h5 class="m-0 font-size-16">Stages <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
        <div class="card-body text-muted">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addstagesmodal"> <i class="uil uil-plus-circle"></i> Add Stages</button>
            <!----Add Sub-Source Modal-------->
            <div class="modal fade" id="addstagesmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Stage</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">Stage</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="stage_name" placeholder="Stage">
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveStage()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_stages" class="table-responsive">
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Stages</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_stages as $stage) {
                            $counter++;
                            if(strcasecmp($stage['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $stage['Name']; ?></td>
                                <td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editstagemodal<?php echo $counter; ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editstagemodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Stage</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="stage_id<?php echo $counter; ?>" value="<?php echo($stage['ID']); ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">Stage</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_stage_name<?php echo $counter; ?>" placeholder="Stage Name" value="<?php echo $stage['Name']; ?>">
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateStage(<?php echo $counter; ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletestagemodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletestagemodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_stage_id<?php echo $counter; ?>" value="<?php echo($stage['ID']); ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteStage(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	function saveStage() {
		var stage_name = $('#stage_name').val();
      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_stage/add_stage.php",
          data: { "stage_name": stage_name },
          success: function (data) {
            $('#addstagesmodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Stage added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingThree").click();
                });
            }
            else {
                toastr.error('Unable to add stage');
            }
          }
        });
        return false;
	}
</script>

<script>
    function updateStage(id) {
        var stage_id = $('#stage_id'.concat(id)).val();
        var new_stage_name = $('#new_stage_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_stage/update_stage.php",
          data: { "stage_id": stage_id, "new_stage_name": new_stage_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Stage updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingThree").click();
                });
            }
            else {
                toastr.error('Unable to update stage');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteStage(id) {
        var stage_id = $('#delete_stage_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_stage/delete_stage.php",
          data: { "stage_id": stage_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Stage deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingThree").click();
                });
            }
            else {
                toastr.error('Unable to delete stage');
            }
          }
        });
        return false;
    }
</script>
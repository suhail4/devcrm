<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_assignment_rules = array();

$assignment_query_res = $conn->query("SELECT * FROM Assignment_Rules");
while($row = $assignment_query_res->fetch_assoc()) {
    $all_assignment_rules[] = $row;
}

$all_institutes = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> '0'");
while($row = $institutes_query_res->fetch_assoc()) {
	$all_institutes[] = $row;
}

?>

<button class="btn btn-primary float-right" data-toggle="modal" data-target="#addassignmentmodal"> Add Rule</button>
    <!----Add Rule Modal-------->
    <div class="modal fade" id="addassignmentmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myCenterModalLabel">Add Assignment Rule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding: 20px;">
                    <div class="row col-lg-12"><h4>When Criteria is</h4></div>
                    <form method="POST" id="assignmentForm">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="example-number">Institute<span style="color: #F64744; font-size: 12px">*</span></label>
                            <div class="col-lg-10">
                                <select id="assignment_institute_select" name="assignment_institute_select" class="form-control wide" data-plugin="customselect" required>
                                	<option disabled selected>Select Institute</option>
                                    <?
                                    foreach ($all_institutes as $institute) {
                                    	?>
                                    	<option value="<?php echo($institute['ID']); ?>"><?php echo $institute['Name'];  ?></option>
                                    	<?
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 15px;"><div class="col-lg-2"></div><div class="col-lg-10"><h4 style="color: #285fdb;"><b>AND</b></h4></div></div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="example-number">Course<span style="color: #F64744; font-size: 12px">*</span></label>
                            <div class="col-lg-10">
                                <select id="assignment_course_select" name="assignment_course_select" class="form-control wide" data-plugin="customselect" required>
                                    <option disabled selected>Select Course</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 15px;"><div class="col-lg-2"></div><div class="col-lg-10"><h4><b>Then counsellor is...</b></h4></div></div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="example-number">Counsellor<span style="color: #F64744; font-size: 12px">*</span></label>
                            <div class="col-lg-10">
                                <select id="assignment_counsellor_select" name="assignment_counsellor_select" class="form-control wide" data-plugin="customselect" multiple required>
                                    <option disabled>Select Counsellor</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary float-right" type="button" onclick="addAssignmentRule();">Save</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div style="padding-top: 30px;">
    <table id="all_assignment_rules" class="table table-hover">
    <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
        <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Institute</th>
                <th>Course</th>
                <th>Counsellors</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php

            $counter = 0;
            foreach ($all_assignment_rules as $assignment_rule) {
                $counter++;

                $get_institute_dets = $conn->query("SELECT * FROM Institutes WHERE ID = '".$assignment_rule['Institute_ID']."'");
                $institute_dets = mysqli_fetch_assoc($get_institute_dets);

                $get_course_dets = $conn->query("SELECT * FROM Courses WHERE ID = '".$assignment_rule['Course_ID']."'");
                $course_dets = mysqli_fetch_assoc($get_course_dets);

                $counsellors_involved = $assignment_rule['Counsellor_IDs'];
                $all_counsellors = explode(",", $counsellors_involved);

                $c_names = "";

                foreach ($all_counsellors as $counsellor_id) {
                    $get_c_name = $conn->query("SELECT * FROM users WHERE ID = '".$counsellor_id."'");
                    $c_name = mysqli_fetch_assoc($get_c_name);

                    $c_names = $c_names.$c_name['Name'].", ";
                }
                ?>
                <tr>
                <td><?php echo $counter; ?></td>
                <td><?php echo explode(" ", $assignment_rule['Timestamp'])[0]; ?></td>
                <td><?php echo $institute_dets['Name']; ?></td>
                <td><?php echo $course_dets['Name']; ?></td>
                <td><?php echo substr($c_names, 0, strlen($c_names)-2); ?></td>
                <td>
                <i class="fa fa-edit" data-toggle="modal" data-target="#editusermodal" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deleteusermodal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>         
                </td>
            </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    </div>

<script>
    $(document).ready(function () {
        $('#assignmentForm').validate({
            rules: {
                assignment_institute_select: {
                    required: true
                },
                assignment_course_select: {
                    required: true
                },
                assignment_counsellor_select: {
                    required: true
                }
            },
            messages: {
                assignment_institute_select: {
                    required: 'Institute is mandatory'
                },
                assignment_course_select: {
                    required: 'Course is mandatory'
                },
                assignment_counsellor_select: {
                    required: 'Atleast one counsellor is mandatory'
                }
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
            }
        });
    });
</script>

<script>
    function addAssignmentRule() {
        if($('#assignmentForm').valid()) {
            var institute_id = $('#assignment_institute_select').val();
            var course_id = $('#assignment_course_select').val();
            var counsellor_ids = $('#assignment_counsellor_select').val().toString();

            $.ajax
            ({
              type: "POST",
              url: "settings_pages/ajax_assignment_rule/add_assignment_rule.php",
              data: { "institute_id": institute_id, "course_id":  course_id, "counsellor_ids": counsellor_ids },
              success: function (data) {
                $('#addassignmentmodal').modal('hide');
                if(data.match("true")) {
                    toastr.success('Assignment rule added successfully');
                    $("#all_assignment_rules").load(location.href + " #all_assignment_rules");
                }
                else {
                    toastr.error('Unable to add assignment rule');
                }
              }
            });
            return false;
        }
    }
</script>

<script>
	$(document).ready(function() {
		$('#assignment_institute_select').change(function() {

			$('#assignment_course_select').html("<option disabled selected>Select Course</option>");
			$('#assignment_counsellor_select').html("<option disabled>Select Counsellor</option>");

			var selected_institute = $('#assignment_institute_select').val();
			$.ajax
			({
				type: "POST",
				url: "onselect/onAssignmentSelect.php",
				data: { "select_institute": "", "selected_institute": selected_institute },
				success: function(data) {
					if(data != "") {
						$('#assignment_course_select').html(data.split("@")[0]);
						$('#assignment_counsellor_select').html(data.split("@")[1]);
					}
					else {
						$('#assignment_course_select').html("<option disabled selected>Select Course</option>");
						$('#assignment_counsellor_select').html("<option disabled>Select Counsellor</option>");
					}
				}
			});
		});
	});
</script>

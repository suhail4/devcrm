<?php include 'filestobeincluded/header-top.php' ?>
<script src="https://kit.fontawesome.com/3212b33ef4.js" crossorigin="anonymous"></script>
<?php include 'filestobeincluded/header-bottom.php' ?>
<!-- Pre-loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="circle1"></div>
			<div class="circle2"></div>
			<div class="circle3"></div>
		</div>
	</div>
</div>
<!-- End Preloader-->
<?php include 'filestobeincluded/navigation.php'; ?>

<?php print_r($_SESSION); ?>
<?php

date_default_timezone_set('Asia/Kolkata');
$all_leads = array();
$all_users = array();
$get_manger = $conn->query("SELECT * FROM users WHERE ID = '" . $_SESSION['useremployeeid'] . "'");
$data_get = mysqli_fetch_assoc($get_manger);

if (!isset($_GET['leads'])) {
	$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
	while ($row = $leads_query_res->fetch_assoc()) {
		$all_leads[] = $row;
	}
	$row_count_leads = mysqli_num_rows($leads_query_res);
}

if (isset($_GET['leads'])) {
	$lead_query = $_GET['leads'];
	if ($lead_query == 'all') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == '1') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == '2') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == '3') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == '4') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == '5') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == '6') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == '7') {
		$leads_query_res = $conn->query("SELECT * FROM Leads WHERE Stage_ID = $lead_query AND Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	} else if ($lead_query == 're_enquired') {
		$leads_query_res = $conn->query("SELECT * FROM Re_Enquired WHERE Counsellor_ID in ($tree_ids) ORDER BY TimeStamp DESC");
		while ($row = $leads_query_res->fetch_assoc()) {
			$all_leads[] = $row;
		}
		$row_count_leads = mysqli_num_rows($leads_query_res);
	}
}

?>


<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
	<div class="content">

		<!-- Start Content-->
		<div class="container-fluid">
			<div class="row page-title">
				<div class="col-md-12">
					<nav aria-label="breadcrumb" class="float-right mt-1 navbuttons">
						<ol class="breadcrumb">
							<button class="btn btn-primary" data-toggle="modal" data-target="#addcampaign"><span data-toggle="tooltip" data-placement="top" title="Add Campaign"><i data-feather="thumbs-up" class="icon-sm"></i></span></button>&nbsp;&nbsp;
							<!-- campaign modal content -->
							<div id="addcampaign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addcampaignLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="addcampaignLabel">Add Campaign</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<h6>Do you want to add marketing campaign?</h6>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addcampaignconfirm" data-dismiss="modal">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
							<!----campaign modal end----------->
							<!-- confirm campaign modal content -->
							<div id="addcampaignconfirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addcampaignconfirmLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="addcampaignconfirmLabel">Add Campaign to <?php echo $row_count_leads ?> Leads</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form method="POST">
												<div class="form-group row">
													<label class="col-lg-2 col-form-label">Campaign Name</label>
													<div class="col-lg-10">
														<input type="text" class="form-control" id="marketing_campaign_name" placeholder="New Campaign">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-lg-2 col-form-label">Select Rule</label>
													<div class="col-lg-10">
														<select data-plugin="customselect" class="form-control" id="campaign_rule">
															<option disabled selected>Choose</option>

														</select>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-lg-2 col-form-label">No. of Times of Communication</label>
													<div class="col-lg-10">
														<input type="text" pattern="\d*" maxlength="1" class="form-control" id="campaign_comm_times" placeholder="1">
													</div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
							<!----confirm campaign modal end----------->

							<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Send SMS to all leads"><span data-toggle="modal" data-target="#sendsmstoall"><i data-feather="message-square" class="icon-sm"></i></span></button>&nbsp;&nbsp;
							<!-- sendsmstoall modal content -->
							<div id="sendsmstoall" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sendsmstoallLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="sendsmstoallLabel">Send SMS</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<h6>Do you want to send SMS to <?php echo $row_count_leads; ?> leads?</h6>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary send-sms-all" data-dismiss="modal">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
							<script type='text/javascript'>
								$(document).ready(function() {
									$('.send-sms-all').click(function() {
										// AJAX request
										$.ajax({
											url: 'ajax_leads/send_sms_all.php',
											success: function(response) {
												// Add response in Modal body
												$('#modal-body-sms-all').html(response);

												// Display Modal
												$('#sendsmstoallconfirm').modal('show');
											}
										});
									});
								});
							</script>
							<!----sendsmstoall modal end----------->
							<!-- confirm sendsmstoall modal content -->
							<div id="sendsmstoallconfirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sendsmstoallLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="sendsmstoallLabel">Send SMS to <?php echo $row_count_leads ?> Leads</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body" id="modal-body-sms-all">
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
							<!----confirm sendsmstoall modal end----------->


							<button class="btn btn-primary" data-toggle="modal" data-target="#sendemailtoall"><span data-toggle="tooltip" data-placement="top" title="Send Email to all leads"><i data-feather="mail" class="icons-sm"></i></span></button>&nbsp;&nbsp;
							<!-- sendemailtoall modal content -->
							<div id="sendemailtoall" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sendemailtoallLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="sendemailtoallLabel">Send Email</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<h6>Do you want to send Email to <?php echo $row_count_leads; ?> leads?</h6>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary send-mail-all" data-dismiss="modal">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
							<!----sendemailtoall modal end----------->
							<script type='text/javascript'>
								$(document).ready(function() {
									$('.send-mail-all').click(function() {
										// AJAX request
										$.ajax({
											url: 'ajax_leads/send_mail_all.php',
											success: function(response) {
												// Add response in Modal body
												$('#modal-body-mail-all').html(response);

												// Display Modal
												$('#sendemailtoallconfirm').modal('show');
											}
										});
									});
								});
							</script>
							<!-- confirm sendemailtoall modal content -->
							<div id="sendemailtoallconfirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sendemailtoallLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="sendemailtoallLabel">Send Email to <?php echo $row_count_leads ?> Leads</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body" id="modal-body-mail-all">
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
							<!----sendemailtoall modal end----------->

							<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Add new lead" onClick="mywizard()"><span data-toggle="modal" data-target="#leadadd"><i data-feather="user-plus"></i></span></button>&nbsp;&nbsp;
							<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Refresh data" onclick="window.location.reload();"><i data-feather="rotate-ccw" class="icon-sm"></i></button>&nbsp;&nbsp;
							<?php if ($data_get['User_Type'] == 'b2b') {  ?>
										<button class="btn btn-primary" id="ExportReporttoExcel" data-toggle="tooltip" data-placement="top" title="Export Data as CSV"><i data-feather="download" class="icon-sm"></i></button>&nbsp;&nbsp;
										<?php } ?>
										
										<script>
										$("#ExportReporttoExcel").on("click", function() {
											 var stage_id = $('.user_tabs.active').val();
												console.log(stage_id);
												$.ajax
												({
												type: "POST",
												url: "ajax_leads/export_leads2.php",
												data: { "stage_id": stage_id },
												success: function (data) {
													 /*
													* Make CSV downloadable
													*/
													console.log(data);
													var downloadLink = document.createElement("a");
													var fileData = ['\ufeff'+data];

													var blobObject = new Blob(fileData,{
														type: "text/csv;charset=utf-8;"
													});

													

													var url = URL.createObjectURL(blobObject);
													downloadLink.href = url;
													downloadLink.download = "Blackboard_Leads_<?php echo date("d-m-Y h:i a"); ?>.csv";

													/*
													* Actually download CSV
													*/
													document.body.appendChild(downloadLink);
													downloadLink.click();
													document.body.removeChild(downloadLink);
																									

												}
												});
												return false;
										});</script>

							<?php if ($data_get['Upload_Status'] == 'Y') {  ?>
								<button class="btn btn-primary" data-toggle="modal" data-target="#uploadleadsmodal"><span data-toggle="tooltip" data-placement="top" title="Upload Data"><i data-feather="upload" class="icon-sm"></i></span></button>&nbsp;&nbsp;
							<?php } ?>
							<!--------------Upload Leads Modal------------->
							<div id="uploadleadsmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadleads" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="uploadleads">Upload Leads</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form id="upload_csv" method="POST" enctype="multipart/form-data">

												<div class="form-group row">
													<label class="col-lg-9 col-form-label" for="example-fileinput">Upload File with Prospect Condidate Details</label>
													<div class="col-lg-3 col-sm-12">
														<a href="/assets/files/Blackboard Leads Sample.csv" download="Blackboard Leads Sample.csv">
															<p><i data-feather="file" class="icon-dual icon-xs"></i> Download Sample</p>
														</a>
													</div><br>
													<div class="col-lg-12">
														<input type="file" name="lead_file" class="form-control" style="border: 0ch;" accept=".csv">
													</div>
												</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
											<button type="submit" name="upload" id="upload" class="btn btn-primary">&nbsp;&nbsp;Upload&nbsp;&nbsp;</button>
										</div>
										</form>
									</div> <!-- /.modal-content -->
								</div> <!-- /.modal-dialog  -->
							</div> <!-- /.modal  -->
							<!----------------End---------------->
							<!--<div>
											<select id="exportLink" class="btn btn-primary custom-select-lg">
												<option><font style="font-size: 10px;">Export Data</font></option>
												<option id="csv">Export as CSV</option>
												<option id="pdf">Export as PDF</option>
												<option id="copy">Copy to clipboard</option>                                                
												
											</select>
										</div>-->
							<button class="btn btn-primary" data-toggle="modal" data-target="#filterModal"><span data-toggle="tooltip" data-placement="top" title="Filter Leads"><i data-feather="filter" class="icon-sm"></i></span></button>&nbsp;&nbsp;
						</ol>
					</nav>
					<!-- /lead add modal -->
					<div class="modal fade" id="leadadd" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-xl">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="myCenterModalLabel">Add New Lead</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>

								<div class="modal-body">
									<!-----Form goes here --------->
									<div id="smartwizard-arrows">
										<ul>
											<li><a href="#sw-arrows-step-1">Personal Details<small class="d-block">Leads</small></a></li>
											<li><a href="#sw-arrows-step-2">Address Details<small class="d-block">Leads</small></a></li>
											<li><a href="#sw-arrows-step-3">Source Details<small class="d-block">Leads</small></a></li>
											<li><a href="#sw-arrows-step-4">Education Details<small class="d-block">Admission</small></a></li>
											<li><a href="#sw-arrows-step-5">Course Details<small class="d-block">Admission</small></a></li>
										</ul>

										<div class="p-3">
											<div id="sw-arrows-step-1">
												<form id="step_1_form">
													<div class="row">
														<div class="col-12">
															<div class="form-group mt-3 mt-sm-0">
																<label>Stage<span style="color: #F64744; font-size: 12px">*</span></label>
																<select data-plugin="customselect" class="form-control" id="stage_select" name="stage_select">
																	<option disabled selected>Select Stage</option>
																	<?php
																	if ($_SESSION['User_Type'] != 'b2b') {
																		$result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type is NULL AND Status = 'Y'");
																	} else {
																		$result_stage = $conn->query("SELECT * FROM Stages WHERE User_Type = 'b2b' AND Status = 'Y'");
																	}

																	while ($stages = $result_stage->fetch_assoc()) {
																	?>
																		<option value="<?php echo $stages['ID']; ?>"><?php echo $stages['Name']; ?></option>
																	<?php } ?>
																</select>
															</div>
															<div class="form-group mt-3 mt-sm-0">
																<label>Reason<span style="color: #F64744; font-size: 12px">*</span></label>
																<select data-plugin="customselect" class="form-control" id="reason_select">
																	<option disabled selected>Select Reason</option>
																</select>
															</div>
															<div class="form-group">
																<label for="sw-arrows-first-name">Full Name<span style="color: #F64744; font-size: 12px">*</span></label>
																<input type="text" id="full_name" name="full_name" class="form-control" placeholder="Full Name" required="required">
															</div>
															<div class="form-group">
																<label for="sw-arrows-first-name">Email</label>
																<input type="email" id="email_id" name="email_id" class="form-control" placeholder="Email">
															</div>
															<div class="form-group">
																<label for="sw-arrows-first-name">Mobile Number<span style="color: #F64744; font-size: 12px">*</span></label>
																<input type="text" pattern="\d*" id="mobile_number" name="mobile_number" minlength="10" maxlength="10" class="form-control" placeholder="Mobile Number">
															</div>
															<div class="form-group">
																<label for="sw-arrows-first-name">Alternate Number<span style="color: #F64744; font-size: 12px">*</span></label>
																<input type="text" pattern="\d*" id="alternate_mobile_number" name="alternate_mobile_number" minlength="10" maxlength="10" class="form-control" placeholder="Alternate Mobile Number">
															</div>
															<div class="form-group">
																<label for="sw-arrows-first-name">Remarks<span style="color: #F64744; font-size: 12px">*</span></label>
																<input type="text" id="remarks" name="remarks" class="form-control" placeholder="Remarks">
															</div>
														</div>
													</div> <!-- end row -->
												</form>
											</div>
											<div id="sw-arrows-step-2">
												<form id="step_2_form">
													<div class="row">
														<div class="col-12">
															<div class="form-group">
																<label for="sw-arrows-first-name">Address</label>
																<input type="text" id="address" class="form-control" placeholder="Address">
															</div>
															<div class="form-group mt-3 mt-sm-0">
																<label>State<span style="color: #F64744; font-size: 12px">*</span></label>
																<select data-plugin="customselect" class="form-control" id="state_select" name="state_select">
																	<option disabled selected>Select State</option>
																	<?php
																	$result_state = $conn->query("SELECT * FROM States WHERE Country_ID = 101");
																	while ($states = $result_state->fetch_assoc()) {
																	?>
																		<option value="<?php echo $states['ID']; ?>"><?php echo $states['Name']; ?></option>
																	<?php } ?>
																</select>
															</div>
															<div class="form-group mt-3 mt-sm-0">
																<label>City<span style="color: #F64744; font-size: 12px">*</span></label>
																<select data-plugin="customselect" class="form-control" id="city_select">
																	<option disabled selected>Select City</option>
																</select>
															</div>
															<div class="form-group">
																<label for="sw-arrows-first-name">Pincode</label>
																<input type="text" pattern="\d*" minlength="6" maxlength="6" id="pin_code" name="pin_code" class="form-control" placeholder="Pincode">
															</div>
														</div> <!-- end col -->
													</div> <!-- end row -->
												</form>
											</div>
											<div id="sw-arrows-step-3">
												<form id="step_3_form">
													<div class="row">
														<div class="col-12">
															<div class="form-group mt-3 mt-sm-0">
																<label>Source<span style="color: #F64744; font-size: 12px">*</span></label>
																<select data-plugin="customselect" class="form-control" id="source_select" name="source_select">
																	<option disabled selected>Select Source</option>
																	<?php
																	$result_source = $conn->query("SELECT * FROM Sources WHERE Status = 'Y'");
																	while ($sources = $result_source->fetch_assoc()) {
																	?>
																		<option value="<?php echo $sources['ID']; ?>"><?php echo $sources['Name']; ?></option>
																	<?php } ?>
																</select>
															</div>
															<div class="form-group mt-3 mt-sm-0">
																<label>Sub-Source<span style="color: #F64744; font-size: 12px">*</span></label>
																<select data-plugin="customselect" class="form-control" id="subsource_select">
																	<option disabled selected>Select Sub-Source</option>
																</select>
															</div>
															<div class="form-group">
																<label for="sw-arrows-first-name">Campaign Name</label>
																<input type="text" id="campaign_name" class="form-control" value="General Enquiry">
															</div>
															<div class="form-group mt-3 mt-sm-0">
																<label>Counsellor</label>
																<select data-plugin="customselect" class="form-control" id="lead_owner">
																	<?php
																	$result_user = $conn->query("SELECT * FROM users WHERE Status = 'Y'");
																	while ($users = $result_user->fetch_assoc()) {
																	?>
																		<option value="<?php echo $users['ID']; ?>"><?php echo $users['Name']; ?></option>
																	<?php } ?>
																</select>
															</div>
															<div class="custom-control custom-checkbox mb-2">
																<input type="checkbox" class="custom-control-input" id="swemail" value="on" onclick="setEmailStatus();" checked>
																<label class="custom-control-label" for="swemail">Send Welcome Email</label>
															</div>
															<div class="custom-control custom-checkbox mb-2">
																<input type="checkbox" class="custom-control-input" id="swsms" value="on" onclick="setSMSStatus();" checked>
																<label class="custom-control-label" for="swsms">Send Welcome SMS</label>
															</div>
														</div> <!-- end col -->
													</div> <!-- end row -->
												</form>
											</div>
											<div id="sw-arrows-step-4">
												<form id="step_4_form">
													<div class="row">
														<div class="col-12">
															<div class="form-group">
																<label for="sw-arrows-first-name">School Name</label>
																<input type="text" id="school_name" class="form-control" placeholder="School Name">
															</div>
															<div class="form-group">
																<label for="sw-arrows-percentage">Percentage / Grade</label>
																<input type="text" id="percentage" class="form-control" placeholder="Percentage / Grade">
															</div>
															<div class="form-group">
																<label for="sw-arrows-qualification">Highest Qualification</label>
																<input type="text" id="qualification" class="form-control" placeholder="Highest Qualification">
															</div>
															<div class="form-group">
																<label for="sw-arrows-refer">Refer By</label>
																<input type="text" id="refer" class="form-control" placeholder="Refer by">
															</div>
														</div> <!-- end col -->
													</div> <!-- end row -->
												</form>
											</div>
											<div id="sw-arrows-step-5">
												<form id="step_5_form">
													<div class="row">
														<div class="col-12">
															<div class="form-group mt-3 mt-sm-0">
																<label>University</label>
																<select data-plugin="customselect" class="form-control" id="select_institute" name="select_institute">
																	<option disabled selected>Select Universty</option>
																	<?php
																	$result_institute = $conn->query("SELECT * FROM Institutes WHERE Status = 'Y' AND ID <> '0'");
																	while ($institutes = $result_institute->fetch_assoc()) {
																	?>
																		<option value="<?php echo $institutes['ID']; ?>"><?php echo $institutes['Name']; ?></option>
																	<?php } ?>
																</select>
															</div>
															<div class="form-group mt-3 mt-sm-0">
																<label>Course</label>
																<select data-plugin="customselect" class="form-control" id="select_course">
																	<option disabled selected>Select Course</option>
																</select>
															</div>
															<div class="form-group mt-3 mt-sm-0">
																<label>Specialization</label>
																<select data-plugin="customselect" class="form-control" id="select_specialization">
																	<option disabled selected>Select Specialization</option>
																</select>
															</div>

															<button class="btn btn-primary" type="button" id="add_lead" onclick="addLead();" style="float: right;">Add Lead</button>
														</div> <!-- end col -->
													</div> <!-- end row -->
												</form>
											</div>
										</div>
									</div>
									<!-----Form end --------->
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
					<!-- /.modal end-->
					<h4 class="mb-1 mt-0">Leads</h4>
				</div>
			</div>
		</div> <!-- container-fluid -->

		<div class="row">

			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div id="filterLeads">
							<div class="row">
								<div class="col-lg-12" style="padding-top: 20px; padding-bottom:20px;">
									<?php

									if ($data_get['User_Type'] != "b2b") {
										$allleads_query = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID in ($tree_ids) AND Counsellor_ID != '' ");
										$row_count_allleads = mysqli_num_rows($allleads_query);

										echo '<button id="all_lead" onclick="add_active(this.value);getList()" value="0" class="btn btn-light user_tabs active">All leads (' . $row_count_allleads . ')</button>';

										$manager_lead_query = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID = '" . $_SESSION['useremployeeid'] . "'");
										$row_count_manager_lead = mysqli_num_rows($manager_lead_query);

										echo '<button id="my_lead" value="9" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">TL Leads (' . $row_count_manager_lead . ')</button>';

										$newleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 1 AND Counsellor_ID in ($tree_ids)");
										$row_count_newleads = mysqli_num_rows($newleads_query);

										echo '<button id="new_lead" value="1" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">New leads (' . $row_count_newleads . ')</button>';

										$connectedleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 2 AND Counsellor_ID in ($tree_ids)");
										$row_count_connectedleads = mysqli_num_rows($connectedleads_query);

										echo '<button id="follow_up" value="2" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Follow-up (' . $row_count_connectedleads . ')</button>';

										$notintrestedleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 3 AND Counsellor_ID in ($tree_ids)");
										$row_count_notintrestedleads = mysqli_num_rows($notintrestedleads_query);

										echo '<button id="not_interested" value="3" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Not Interested (' . $row_count_notintrestedleads . ')</button>';

										$registrationleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 4 AND Counsellor_ID in ($tree_ids)");
										$row_count_registrationleads = mysqli_num_rows($registrationleads_query);

										echo '<button id="reg_done" value="4" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Registration Done (' . $row_count_registrationleads . ')</button>';

										$admissionleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 5 AND Counsellor_ID in ($tree_ids)");
										$row_count_admissionleads = mysqli_num_rows($admissionleads_query);

										echo '<button id="admi_done" value="5" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Admission Done (' . $row_count_admissionleads . ')</button>';

										$b2bleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 6 AND Counsellor_ID in ($tree_ids)");
										$row_count_b2bleads = mysqli_num_rows($b2bleads_query);

										echo '<button id="intr_b2b" value="6" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Interested for B2B (' . $row_count_b2bleads . ')</button>';

										$notconnectedleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 7 AND Counsellor_ID in ($tree_ids) AND Counsellor_ID != '' "); //new update
										$row_count_notconnectedleads = mysqli_num_rows($notconnectedleads_query);

										echo '<button id="not_connect" value="7" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Not Connected (' . $row_count_notconnectedleads . ')</button>';

										$reenquiredleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 8 AND Counsellor_ID in ($tree_ids)");
										$row_count_reenquiredleads = mysqli_num_rows($reenquiredleads_query);

										echo '<button id="re_enquiry" value="8" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Re-enquired (' . $row_count_reenquiredleads . ')</button>';

										$interested_for_next_session = $conn->query("SELECT COUNT(ID) FROM Leads WHERE Stage_ID = 10 AND Counsellor_ID in ($tree_ids)");

										echo '<button id="intr_for_next_session" value="10" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Interested For Next Session (' . (mysqli_fetch_row($interested_for_next_session)[0]) . ')</button>';
									}
									?>
									<?php
									if ($data_get['User_Type'] == "b2b") {
										$allleads_query = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID in ($tree_ids) AND Counsellor_ID != '' ");
										$row_count_allleads = mysqli_num_rows($allleads_query);

										echo '<button id="all_lead" onclick="add_active(this.value);getList()" value="0" class="btn btn-light user_tabs active">All leads (' . $row_count_allleads . ')</button>';

										$manager_lead_query = $conn->query("SELECT * FROM Leads WHERE Counsellor_ID = '" . $_SESSION['useremployeeid'] . "'");
										$row_count_manager_lead = mysqli_num_rows($manager_lead_query);

										echo '<button id="my_lead" value="9" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">TL Leads (' . $row_count_manager_lead . ')</button>';

										$newleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 1 AND Counsellor_ID in ($tree_ids)");
										$row_count_newleads = mysqli_num_rows($newleads_query);

										echo '<button id="new_lead" value="1" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">New leads (' . $row_count_newleads . ') </button>';

										$reenquiredleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 8 AND Counsellor_ID in ($tree_ids)");
										$row_count_reenquiredleads = mysqli_num_rows($reenquiredleads_query);

										echo '<button id="re_enquiry" value="8" onclick="add_active(this.value);getList()" class="btn btn-light user_tabs">Re-enquired (' . $row_count_reenquiredleads . ')</button>';

										$q = $conn->query("SELECT ID,Name FROM Stages WHERE User_Type = '" . $data_get['User_Type'] . "'");
										$var = 1;
										while ($stage = mysqli_fetch_assoc($q)) {

											$count = $conn->query("SELECT COUNT(ID) FROM Leads WHERE Stage_ID = '" . $stage['ID'] . "' ");
											$count = mysqli_fetch_row($count)[0];
									?>
											<button id='<?php print_r($stage['ID']) ?>' value='<?php print_r($stage['ID']) ?>' onclick="add_active(this.value);getList()" class="btn btn-light user_tabs"><?php print_r($stage['Name']) ?>(<?php echo ($count) ?>)</button>
									<?php

										}
									}
									?>
								</div>

							</div>

							<div class="row">
								<div class="custom-control custom-checkbox col-lg-6" style="padding-left: 44px; float: left;">
									<input type="checkbox" onclick="checkAllBoxes()" class="custom-control-input checkbox-function" id="select_all_boxes" value="not-checked">
									<label class="custom-control-label" for="select_all_boxes"></label>
								</div>
								<div class="col-lg-6">
									<button type="button" class="btn btn-primary " style="float: right;margin-left: 10px;" onclick="getList();">Submit</button>
									&nbsp;
									<input type="number" id="row-count" style="float: right; width: 25%;margin-left: 10px;" class="form-control col-lg-4" placeholder="25">&nbsp;&nbsp;<label class="col-form-label" for="row-count" style="float: right;">Number of rows:</label>
								</div>
							</div>

							<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
							<div class="row mb-40" id="subdis" style="display: none;">
								<div class="col-lg-3 col-md-3 col-sm-12">
									<div class="form-group mt-3 mt-sm-0">
										<label>Sub-dispositions</label>
										<select data-plugin="customselect" class="form-control" id="subdisposition_select" onchange="getList()">
											<option disabled selected>Select Sub-Disposition</option>
										</select>
									</div>
								</div>
							</div>
							<div id="divLoader" class="col-12" style="display: none; height: 100%;">
								<center>
									<div class="spinner-grow text-primary m-2" role="status">
										<span class="sr-only">Loading...</span>
									</div>
								</center>
							</div>
							<div class="table-responsive" style="padding-top: 20px;">
								<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
								<form id="checkbox-form" method="POST">
									<table class="table table-striped table-borderless table-hover">
										<tbody id="leads_tbody">


										</tbody>
									</table>
									<div><br><br></div>
								</form>
							</div>
						</div>

						<script>
							function pop() {
								$('[data-toggle="popover"]').popover();
							};
						</script>

						<script type='text/javascript'>
							// $(".checkbox-selectall").click(function () {
							// 	$(".checkbox-function").prop('checked', $(this).prop('checked'));
							// 	var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
							// 	console.log(data_count);
							// 	if(data_count>0){
							// 		$("#divShowHide1").css({display: "block"});
							// 		$("#divShowHide2").css({display: "block"});
							// 		$("#divShowHide3").css({display: "block"});
							// 		$("#divShowHide4").css({display: "block"});
							// 	}else{
							// 		$("#divShowHide1").css({display: "none"});
							// 		$("#divShowHide2").css({display: "none"});
							// 		$("#divShowHide3").css({display: "none"});
							// 		$("#divShowHide4").css({display: "none"});
							// 	}
							// });

							// $(".checkbox-function").change(function(){
							//     if (!$(this).prop("checked")){
							//         $(".checkbox-selectall").prop("checked",false);
							//     }
							// });

							function checkbox() {
								var data_count = $('#checkbox-form').find('input[name="id[]"]:checked').length;
								console.log(data_count);
								if (data_count > 0) {
									$("#divShowHide1").css({
										display: "block"
									});
									$("#divShowHide2").css({
										display: "block"
									});
									$("#divShowHide3").css({
										display: "block"
									});
									$("#divShowHide4").css({
										display: "block"
									});
									$('#divShowHide5').css({
										display: "block"
									});
								} else {
									$("#divShowHide1").css({
										display: "none"
									});
									$("#divShowHide2").css({
										display: "none"
									});
									$("#divShowHide3").css({
										display: "none"
									});
									$("#divShowHide4").css({
										display: "none"
									});
									$('#divShowHide5').css({
										display: "none"
									});
								}
							}
						</script>



						<!--------------------ICON MODAL-------------->

						<script type='text/javascript'>
							function followupmodal(id) {

								var userid = id;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/right_modal.php',
									type: 'post',
									data: {
										"userid": userid
									},
									success: function(response) {
										// Add response in Modal body
										$('#right-modal-body').html(response);
										$('.modal-backdrop').remove();

										// Display Modal
										$('#right_modal').modal('show');
									}
								});
							}
						</script>

						<script type='text/javascript'>
							function responsesmodal(id) {

								var responseid = id;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/right_modal.php',
									type: 'post',
									data: {
										"responseid": responseid
									},
									success: function(response) {
										// Add response in Modal body
										$('#right-modal-body').html(response);
										$('.modal-backdrop').remove();

										// Display Modal
										$('#right_modal').modal('show');
									}
								});
							}
						</script>



						<script type='text/javascript'>
							function re_enquiredmodal(id) {

								var renquiredid = id;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/right_modal.php',
									type: 'post',
									data: {
										"renquiredid": renquiredid
									},
									success: function(response) {
										// Add response in Modal body
										$('#right-modal-body').html(response);
										$('.modal-backdrop').remove();

										// Display Modal
										$('#right_modal').modal('show');
									}
								});
							}
						</script>
						<script type="text/javascript">
							$(document).ready(function() {
								$(document).ajaxStart(function() {
									$('#divLoader').css("display", "block");
								});
								$(document).ajaxStop(function() {
									$('#divLoader').css("display", "none");
								});
							});
						</script>

						<div class="modal fade" id="right_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-dialog-slideout modal-md">
								<div class="modal-content" id="right-modal-body">

								</div>
							</div>
						</div>
						<!--------------------ICON MODAL-------------->

						<!-------Refer Selected modal-------->
						<div class="modal fade" id="selected_refer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel2">Refer Selected Leads</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-refer-selected">

									</div>
								</div>
							</div>
						</div>
						<!-------End Refer Selected modal-------->
						<!-------SMS Selected modal-------->
						<div class="modal fade" id="selected_sms" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel2">Send SMS to Selected Leads</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-sms-selected">

									</div>
								</div>
							</div>
						</div>
						<!-------End SMS Selected modal-------->
						<!-------SMS Selected modal-------->
						<div class="modal fade" id="selected_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel2">Send Mail to Selected Leads</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-mail-selected">

									</div>
								</div>
							</div>
						</div>
						<!-------End SMS Selected modal-------->
						<!-------Delete Selected modal-------->
						<div class="modal fade" id="selected_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-xs">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel2">Delete Selected Leads</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-delete-selected">

									</div>
								</div>
							</div>
						</div>
						<!-------End Delete Selected modal-------->

						<script type='text/javascript'>
							function whatsapp(id) {

								var userid = id;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/whatsapp.php',
									type: 'post',
									data: {
										userid: userid
									},
									success: function(response) {
										// Add response in Modal body
										$('#modal-body-whatsapp').html(response);

										// Display Modal
										$('#whatsappmessage').modal('show');
									}
								});
							}
						</script>
						<!-------whatsapp modal-------->
						<div class="modal fade" id="whatsappmessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-xs" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Send WhatsApp Message</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-whatsapp">
									</div>
								</div>
							</div>
						</div>
						<!-------whatsapp modal-------->

						<script type='text/javascript'>
							function addfollowupmodal(val) {

								var userid = val;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/add_followup.php',
									type: 'post',
									data: {
										userid: userid
									},
									success: function(response) {
										// Add response in Modal body
										$('#modal-body-addfollowup').html(response);

										// Display Modal
										$('#addfollowup').modal('show');
									}
								});
							}
						</script>
						<!-------Add followup modal-------->
						<div class="modal fade" id="addfollowup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Add Followup</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-addfollowup">
									</div>
								</div>
							</div>
						</div>
						<!-------Add Followup modal-------->


						<script type='text/javascript'>
							function editlead(val) {

								var userid = val;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/edit_lead_form.php',
									type: 'post',
									data: {
										userid: userid
									},
									success: function(response) {
										// Add response in Modal body
										$('#modal-body-edit').html(response);

										// Display Modal
										$('#editlead').modal('show');
									}
								});
							}

							function editleaddob(userid) {
								$.ajax({
									url: 'ajax_leads/edit_lead_form.php',
									type: 'post',
									data: {
										userid: userid
									},
									success: function(response) {
										// Add response in Modal body
										$('#modal-body-edit').html(response);

										// Display Modal
										$('#editlead').modal('show');
									}
								});
							}
						</script>
						<!--------Edit Lead----------->
						<div class="modal fade" id="editlead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="myCenterModalLabel">Edit Lead Details</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-edit">
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>
						<!-- /.modal end-->
						<!--------Edit Lead End----------->


						<script type='text/javascript'>
							function leadhistory(val) {
								var userid = val;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/lead_history.php',
									type: 'post',
									data: {
										userid: userid
									},
									success: function(response) {

										// Add response in Modal body
										$('#modal-body-history').html(response);

										// Display Modal
										$('#viewhistory').modal('show');
									}
								});
							}
						</script>
						<!-------History modal-------->
						<div class="modal fade" id="viewhistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">History</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-history">

									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<!-------End History modal-------->

						<script type='text/javascript'>
							function referlead(val) {
								var userid = val;

								// AJAX request
								$.ajax({
									url: 'ajax_leads/refer_lead.php',
									type: 'post',
									data: {
										userid: userid
									},
									success: function(response) {
										console.log(response);
										// Add response in Modal body
										$('#modal-body-refer').html(response);

										// Display Modal
										$('#referlead').modal('show');
									}
								});
							}
						</script>
						<!--------refer lead---->
						<div id="referlead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referallleadsLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="referallleadsLabel">Refer Lead </h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-refer">
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div><!-- /.modal -->
						<!------End refer lead------>


						<script type='text/javascript'>
							$(document).ready(function() {

								$('.deletelead').click(function() {

									var userid = $(this).data('id');

									// AJAX request
									$.ajax({
										url: 'ajax_leads/del_lead.php',
										type: 'post',
										data: {
											userid: userid
										},
										success: function(response) {
											// Add response in Modal body
											$('#modal-body-delete').html(response);

											// Display Modal
											$('#deletelead').modal('show');
										}
									});
								});
							});
						</script>
						<!-------Delete Lead----->
						<div class="modal fade" id="deletelead" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-delete">

									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>
						<!-- /.modal -->
						<!--------End Delete Lead---------->

						<div id="referselectedlead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="referselectedleadLabel" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="referselectedleadLabel">Refer Leads to</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="form-group row">
											<label class="col-lg-2 col-form-label">Select Counsellor</label>
											<div class="col-lg-10">
												<select data-plugin="customselect" class="form-control" id="refer_lead_owner">
													<option disabled selected>Choose</option>
													<?php
													$result_refer_lead = $conn->query("SELECT * FROM users");
													while ($refer_leads = $result_refer_lead->fetch_assoc()) {
													?>
														<option value="<?php echo $refer_leads['ID']; ?>"><?php echo $refer_leads['Name']; ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-2 col-form-label">Add Comment</label>
											<div class="col-lg-10">
												<input type="text" class="form-control" id="referal_comment" placeholder="">
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">&nbsp;&nbsp;Confirm&nbsp;&nbsp;</button>
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div><!-- /.modal -->

						<!--------Respone Modal----------->
						<div class="modal fade" id="modal_mail" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="myCenterModalLabel">Delivery Report E-Mail</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body" id="modal-body-mail">
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>
						<!-- /.modal end-->
						<!--------Respons End----------->













					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal right fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
			<div class="modal-dialog" role="document" style="margin-right: 0px;">
				<div class="modal-content">

					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel2">Lead Filter</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					</div>

					<div class="modal-body" style="height: 659px;
    overflow-y: auto;">
						<div class="form-group">
							<label for="Counsellor">Counsellor</label>
							<select class="form-control" multiple data-plugin="customselect" id="counsellor_filter">

								<?php $get_couns = $conn->query("SELECT * FROM users WHERE ROLE='Counsellor' AND Reporting_To_User_ID='" . $_SESSION['useremployeeid'] . "' AND Status='Y'");
								while ($counsellor = $get_couns->fetch_assoc()) {
								?>
									<option value="<?php echo $counsellor['ID']; ?>"><?php echo $counsellor['Name']; ?></option>
								<?php } ?>


							</select>
						</div>
						<div class="form-group" style="display: none;">
							<label for="Institute">Institute</label>
							<select class="form-control" id="institute_filter">

								<?php
								$get_ins = $conn->query("SELECT * FROM Institutes WHERE ID='" . $data_get['Institute_ID'] . "'");
								$data2 = $get_ins->fetch_assoc() ?>
								<option value="<?php echo $data2['ID']; ?>" selected><?php echo $data2['Name']; ?></option>

							</select>
						</div>

						<!-- <div class="form-group">
					  	<label for="Campaign_Name">Campaign Name</label>
					  	<input type="text" name="campaign_name" class="form-control" placeholder="Campaign Name" id="campaign_name_filter">
					  </div>
				 -->

						<div class="form-group">
							<label for="Source">Source</label>
							<select class="form-control" multiple data-plugin="customselect" id="source_filter" onchange="showSub()">

								<?php $sql = $conn->query("SELECT * FROM Sources");
								while ($data = $sql->fetch_assoc()) { ?>
									<option value="<?php echo $data['ID']; ?>"><?php echo $data['Name']; ?></option>
								<?php } ?>

							</select>
						</div>

						<div class="form-group">
							<label for="Sub-Source">Sub-Source</label>
							<select class="form-control" multiple data-plugin="customselect" id="sub_source_filter">


							</select>
						</div>



						<div class="form-group">
							<label for="Course">Course</label>
							<select class="form-control" multiple data-plugin="customselect" id="course_filter" onchange="showSpec(this.value)">

								<?php
								$get_course = $conn->query("SELECT * FROM Courses WHERE Institute_ID='" . $data2['ID'] . "'");
								while ($data3 = $get_course->fetch_assoc()) { ?>
									<option value="<?php echo $data3['ID']; ?>"><?php echo $data3['Name']; ?></option>
								<?php } ?>
							</select>
						</div>

						<div class="form-group">
							<label for="Specialization">Specialization</label>
							<select class="form-control" multiple data-plugin="customselect" id="speclization_filter">


							</select>
						</div>

						<!-- <div class="form-group">
					    <label for="Course">Select Communication Type</label>
					   	<select class="form-control" multiple data-plugin="customselect" id="communication_type_filter">
					   	
					   		<option value="Email">Email</option>
					   		<option value="SMS">SMS</option>
					   	</select>
					  </div> -->

						<div class="form-group">
							<label for="Stage">Stage</label>
							<select class="form-control" multiple data-plugin="customselect" id="stage_filter">

								<?php
								if ($data_get['User_Type'] != 'b2b') {
									$get_stage = $conn->query("SELECT * FROM Stages WHERE User_Type is NULL AND Status = 'Y'");
								} else {
									$get_stage = $conn->query("SELECT * FROM Stages WHERE User_Type = 'b2b' or ID='1' AND Status = 'Y'");
								}

								while ($data5 = $get_stage->fetch_assoc()) { ?>
									<option value="<?php echo $data5['ID']; ?>"><?php echo $data5['Name']; ?></option>
								<?php } ?>
							</select>
						</div>

						<!-- <div class="form-group">
					    <label for="Reason">Reason</label>
					   	<select class="form-control" multiple data-plugin="customselect" id="reason_filter">
					   		
					   		<?php
								$get_reason  = $conn->query("SELECT * FROM Reasons");
								while ($data6 = $get_reason->fetch_assoc()) { ?>
					   		<option value="<?php echo $data6['ID']; ?>"><?php echo $data6['Name']; ?></option>
					   		<?php } ?>
					   	</select>
					  </div> -->
						<div class="form-group">
							<label for="Stage">State</label>
							<select class="form-control" multiple data-plugin="customselect" id="state_filter">

								<?php
								$query_state = $conn->query("SELECT * FROM States");
								while ($get_state = $query_state->fetch_assoc()) { ?>
									<option value="<?php echo $get_state['ID']; ?>"><?php echo $get_state['Name']; ?></option>
								<?php } ?>
							</select>
						</div>

						<div class="form-group">
							<label for="From_Date">Creation Date</label>
							<input type="text" class="form-control datepicker_range" placeholder="yyyy-mm-dd to yyyy-mm-dd" id="creation_date_filter">

						</div>

						<div class="form-group">
							<label for="To_Date">Update Date</label>
							<input type="text" class="form-control datepicker_range" placeholder="yyyy-mm-dd to yyyy-mm-dd" id="update_date_filter">

						</div>

						<div>
							<label for="number_of_rows">Number of Rows</label>
							<input type="text" class="form-control" placeholder="Enter Rows" id="number_rows">
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<?php if ($_SESSION['User_Type'] == "b2b") { ?>
								<button type="button" id="b2b" value="b2b" class="btn btn-primary" onclick="LeadFilter();">Search</button>
							<?php } else { ?>
								<button type="button" class="btn btn-primary" onclick="LeadFilter();">Search</button>
							<?php } ?>
						</div>



					</div><!-- modal-content -->
				</div><!-- modal-dialog -->
			</div><!-- modal -->
		</div> <!-- content -->

		<script>
			function setEmailStatus() {
				if ($('#swemail').val() == 'on') {
					$('#swemail').val('off');
				} else {
					$('#swemail').val('on');
				}
			}
		</script>

		<script>
			function setSMSStatus() {
				if ($('#swsms').val() == 'on') {
					$('#swsms').val('off');
				} else {
					$('#swsms').val('on');
				}
			}
		</script>

		<script type="text/javascript">
			function showSub() {
				var id = $('#source_filter').val();


				$.ajax({
					url: "ajax_leads/show_sub_source.php",
					type: "post",
					data: {
						"id": id
					},
					success: function(res) {
						console.log(res);
						$('#sub_source_filter').html(res);
					}
				})
			}

			function showSpec(val) {
				var id = $('#course_filter').val();
				console.log(id);
				var institute_id = $('#institute_filter').val();
				$.ajax({
					url: "ajax_leads/show_spec.php",
					type: "post",
					data: {
						"id": id,
						"institute_id": institute_id
					},
					success: function(res) {
						console.log(res);
						$('#speclization_filter').html(res);
					}
				})
			}
		</script>


		<script>
			function addLead() {
				if ($('#step_1_form').valid() && $('#step_2_form').valid() && $('#step_3_form').valid() && $('#step_4_form').valid() && $('#step_5_form').valid()) {
					var stageID = $('#stage_select').val();
					var reasonID = $('#reason_select').val();
					var fullName = $('#full_name').val();
					var emailID = $('#email_id').val();
					var mobileNumber = $('#mobile_number').val();
					var remarks = $('#remarks').val();

					var address = $('#address').val();
					var stateID = $('#state_select').val();
					var cityID = $('#city_select').val();
					var pincode = $('#pin_code').val();

					var sourceID = $('#source_select').val();
					var subsourceID = $('#subsource_select').val();
					var campaignName = $('#campaign_name').val();
					var leadOwner = $('#lead_owner').val();

					var swEmail = $('#swemail').val();
					var swSMS = $('#swsms').val();

					var schoolName = $('#school_name').val();
					var percentage = $('#percentage').val();
					var qualification = $('#qualification').val();
					var refer = $('#refer').val();

					var instituteID = $('#select_institute').val();
					var courseID = $('#select_course').val();
					var specializationID = $('#select_specialization').val();

					if (swEmail == 'on') {
						$.ajax({
							type: "POST",
							url: "/Mailer/send_mail.php",
							data: {
								"leadName": fullName,
								"emailID": emailID
							},
							success: function(data) {
								console.log(data);
								if (data.match("true")) {
									toastr.success('Welcome Email sent!');
								} else {
									toastr.error('Unable to send welcome Email');
								}
							}
						});
					}

					if (swSMS == 'on') {
						$.ajax({
							type: "POST",
							url: "/Twilio/index.php",
							data: {
								"leadName": fullName,
								"phoneNumber": mobileNumber
							},
							success: function(data) {
								console.log(data);
								if (data.match("true")) {
									toastr.success('Welcome SMS sent!');
								} else {
									toastr.error('Unable to send welcome SMS');
								}
							}
						});
					}

					$.ajax({
						type: "POST",
						url: "/ajax_leads/add_lead.php",
						data: {
							"stageID": stageID,
							"reasonID": reasonID,
							"fullName": fullName,
							"emailID": emailID,
							"mobileNumber": mobileNumber,
							"remarks": remarks,
							"address": address,
							"stateID": stateID,
							"cityID": cityID,
							"pincode": pincode,
							"sourceID": sourceID,
							"subsourceID": subsourceID,
							"campaignName": campaignName,
							"leadOwner": leadOwner,
							"schoolName": schoolName,
							"percentage": percentage,
							"qualification": qualification,
							"refer": refer,
							"instituteID": instituteID,
							"courseID": courseID,
							"specializationID": specializationID
						},
						success: function(data) {
							console.log(data);
							$('#leadadd').modal('hide');
							if (data.match("true")) {
								toastr.success('Lead added successfully');
								window.location.reload();
							} else {
								toastr.error('Unable to add lead');
							}
						}
					});
					return false;
				} else {
					if ('step_1_form'.valid() == false) {
						$('#smartwizard-arrows').smartWizard("goToStep", 0);
					}
					if ('step_2_form'.valid() == false) {
						$('#smartwizard-arrows').smartWizard("goToStep", 1);
					}
					if ('step_3_form'.valid() == false) {
						$('#smartwizard-arrows').smartWizard("goToStep", 2);
					}
					if ('step_4_form'.valid() == false) {
						$('#smartwizard-arrows').smartWizard("goToStep", 3);
					}
					if ('step_5_form'.valid() == false) {
						$('#smartwizard-arrows').smartWizard("goToStep", 4);
					}
				}
			}
		</script>


		<script>
			function mywizard() {
				$('#smartwizard-arrows').smartWizard({
					theme: 'arrows'
				});

				$('#step_1_form').validate({
					rules: {
						stage_select: {
							required: true
						},
						full_name: {
							required: true
						},
						email_id: {
							email: "Please enter a valid email address."
						},
						mobile_number: {
							required: true,
							minlength: 10,
							maxlength: 10
						},
						remarks: {
							required: true
						}
					},
					messages: {
						stage_select: {
							required: 'Please select a stage'
						},
						full_name: {
							required: 'Name is required'
						},
						mobile_number: {
							required: 'Mobile number is required',
							minlength: 'Invalid mobile number',
							maxlength: 'Invalid mobile number'
						},
						remarks: {
							required: 'Remarks are required'
						}
					},
					highlight: function(element) {
						$(element).addClass('error');
						$(element).closest('.form-control').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).removeClass('error');
						$(element).closest('.form-control').removeClass('has-error');
					}
				});

				$('#step_2_form').validate({
					rules: {
						state_select: {
							required: true
						},
						pin_code: {
							minlength: 6,
							maxlength: 6
						}
					},
					messages: {
						state_select: {
							required: 'Please select a state'
						},
						pin_code: {
							minlength: 'Invalid Pincode',
							minlength: 'Invalid Pincode'
						}
					},
					highlight: function(element) {
						$(element).addClass('error');
						$(element).closest('.form-control').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).removeClass('error');
						$(element).closest('.form-control').removeClass('has-error');
					}
				});

				$('#step_3_form').validate({
					rules: {
						source_select: {
							required: true
						}
					},
					messages: {
						source_select: {
							required: 'Please select a source'
						}
					},
					highlight: function(element) {
						$(element).addClass('error');
						$(element).closest('.form-control').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).removeClass('error');
						$(element).closest('.form-control').removeClass('has-error');
					}
				});

				$('#step_5_form').validate({
					rules: {
						select_institute: {
							required: true
						}
					},
					messages: {
						select_institute: {
							required: 'Please select an university'
						}
					},
					highlight: function(element) {
						$(element).addClass('error');
						$(element).closest('.form-control').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).removeClass('error');
						$(element).closest('.form-control').removeClass('has-error');
					}
				});

				$("#smartwizard-arrows").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
					if (currentStepIndex == 0) {
						if ($('#step_1_form').valid()) {
							return true;
						} else {
							return false;
						}
					}
					if (currentStepIndex == 1) {
						if ($('#step_2_form').valid()) {
							return true;
						} else {
							return false;
						}
					}
					if (currentStepIndex == 2) {
						if ($('#step_3_form').valid()) {
							return true;
						} else {
							return false;
						}
					}
					if (currentStepIndex == 3) {
						if ($('#step_4_form').valid()) {
							return true;
						} else {
							return false;
						}
					}
					if (currentStepIndex == 4) {
						if ($('#step_5_form').valid()) {
							return true;
						} else {
							return false;
						}
					}
				});
			};
		</script>

		<script>
			$(document).ready(function() {
				$('#stage_select').change(function() {

					$('#reason_select').html("<option disabled selected>Select Reason</option>");

					var selected_stage = $('#stage_select').val();
					$.ajax({
						type: "POST",
						url: "onselect/onSelect.php",
						data: {
							"stage_select": "",
							"selected_stage": selected_stage
						},
						success: function(data) {
							if (data != "") {
								$('#reason_select').html(data);
							} else {
								$('#reason_select').html("<option disabled selected>Select Reason</option>");
							}
						}
					});
				});
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#state_select').change(function() {

					$('#city_select').html("<option disabled selected>Select City</option>");

					var selected_state = $('#state_select').val();
					$.ajax({
						type: "POST",
						url: "onselect/onSelect.php",
						data: {
							"state_select": "",
							"selected_state": selected_state
						},
						success: function(data) {
							if (data != "") {
								$('#city_select').html(data);
							} else {
								$('#city_select').html("<option disabled selected>Select City</option>");
							}
						}
					});
				});
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#source_select').change(function() {

					$('#subsource_select').html("<option disabled selected>Select Sub-Source</option>");

					var selected_source = $('#source_select').val();
					$.ajax({
						type: "POST",
						url: "onselect/onSelect.php",
						data: {
							"source_select": "",
							"selected_source": selected_source
						},
						success: function(data) {
							if (data != "") {
								$('#subsource_select').html(data);
							} else {
								$('#subsource_select').html("<option disabled selected>Select Sub-Source</option>");
							}
						}
					});
				});
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#select_institute').change(function() {

					$('#select_course').html("<option disabled selected>Select Course</option>");

					var selected_institute = $('#select_institute').val();
					$.ajax({
						type: "POST",
						url: "onselect/onSelect.php",
						data: {
							"select_institute": "",
							"selected_institute": selected_institute
						},
						success: function(data) {
							if (data != "") {
								$('#select_course').html(data);
								jQuery('#select_course').trigger('change');
							} else {
								$('#select_course').html("<option disabled selected>Select Course</option>");
							}
						}
					});
				});
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#select_course').change(function() {
					console.log("yes");

					$('#select_specialization').html("<option disabled selected>Select Specialization</option>");

					var selected_course = $('#select_course').val();
					var selected_institute = $('#select_institute').val();

					console.log(selected_institute);

					$.ajax({
						type: "POST",
						url: "onselect/onSelect.php",
						data: {
							"select_course": "",
							"selected_course": selected_course,
							"selected_institute": selected_institute
						},
						success: function(data) {
							console.log(data);
							if (data != "") {
								$('#select_specialization').html(data);
							} else {
								$('#select_specialization').html("<option disabled selected>Select Specialization</option>");
							}
						}
					});
				});
			});
		</script>


		<!-----Edit Lead Script--------->
		<script>
			function updateLead(id) {
				var new_leadID = $('#new_leadID').val();
				var new_stageID = $('#new_stage_select').val();
				var new_reasonID = $('#new_reason_select').val();
				var new_fullName = $('#new_full_name').val();
				var new_emailID = $('#new_email_id').val();
				var new_mobileNumber = $('#new_mobile_number').val();
				var new_alternate_number = $('#new_alt_mobile_number').val();
				var new_remarks = $('#new_remarks').val();

				var new_address = $('#new_address').val();
				var new_stateID = $('#new_state_select').val();
				var new_cityID = $('#new_city_select').val();
				var new_pincode = $('#new_pin_code').val();

				var new_sourceID = $('#new_source_select').val();
				var new_subsourceID = $('#new_subsource_select').val();
				var new_campaignName = $('#new_campaign_name').val();
				var new_leadOwner = $('#new_select_counsellor').val();

				var new_schoolName = $('#new_school_name').val();
				var new_percentage = $('#new_percentage').val();
				var new_qualification = $('#new_qualification').val();

				var new_instituteID = $('#new_select_institute').val();
				var new_courseID = $('#new_select_course').val();
				var new_specializationID = $('#new_select_specialization').val();
				var dob = $('#dob').val();


				$.ajax({
					type: "POST",
					url: "/teamlead/ajax_leads/edit_lead.php",
					data: {
						"new_alternate_number": new_alternate_number,
						"new_leadID": new_leadID,
						"new_stageID": new_stageID,
						"new_reasonID": new_reasonID,
						"new_fullName": new_fullName,
						"new_emailID": new_emailID,
						"new_mobileNumber": new_mobileNumber,
						"new_remarks": new_remarks,
						"new_address": new_address,
						"new_stateID": new_stateID,
						"new_cityID": new_cityID,
						"new_pincode": new_pincode,
						"new_sourceID": new_sourceID,
						"new_subsourceID": new_subsourceID,
						"new_campaignName": new_campaignName,
						"new_leadOwner": new_leadOwner,
						"new_schoolName": new_schoolName,
						"new_percentage": new_percentage,
						"new_qualification": new_qualification,
						"new_instituteID": new_instituteID,
						"new_courseID": new_courseID,
						"new_specializationID": new_specializationID,
						"dob": dob
					},
					success: function(data) {
						$('.modal').modal('hide');
						if (data.match(".")) {
							toastr.success('Lead updated successfully');
							$('#row' + id).html(data);
						} else {
							toastr.error('Unable to update lead');
						}
					}
				});
				return false;

			}
		</script>

		<script>
			$(document).ready(function() {
				$('#new_stage_select').change(function() {

					$('#new_reason_select').html("<option disabled selected>Select Reason</option>");

					var new_selected_stage = $('#new_stage_select').val();
					$.ajax({
						type: "POST",
						url: "onselect/onSelect.php",
						data: {
							"stage_select": "",
							"selected_stage": new_selected_stage
						},
						success: function(data) {
							if (data != "") {
								$('#new_reason_select').html(data);
							} else {
								$('#new_reason_select').html("<option disabled selected>Select Reason</option>");
							}
						}
					});
				});
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#new_select_institute').change(function() {

					$('#new_select_course').html("<option disabled selected>Select Course</option>");

					var new_selected_institute = $('#new_select_institute').val();
					$.ajax({
						type: "POST",
						url: "onselect/onSelect.php",
						data: {
							"select_institute": "",
							"selected_institute": new_selected_institute
						},
						success: function(data) {
							if (data != "") {
								$('#new_select_course').html(data);
								getSpecializations();
							} else {
								$('#new_select_course').html("<option disabled selected>Select Course</option>");
							}
						}
					});
				});
			});
		</script>

		<script>
			function getSpecializations() {

				$('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");

				var new_selected_course = $('#new_select_course').val();

				$.ajax({
					type: "POST",
					url: "onselect/onSelect.php",
					data: {
						"select_course": "",
						"selected_course": new_selected_course
					},
					success: function(data) {
						if (data != "") {
							$('#new_select_specialization').html(data);
						} else {
							$('#new_select_specialization').html("<option disabled selected>Select Specialization</option>");
						}
					}
				});
			}
		</script>

		<script>
			function deleteLeads(id) {
				var delete_lead_id = $('#delete_lead_id'.concat(id)).val();

				$.ajax({
					type: "POST",
					url: "ajax_leads/delete_lead.php",
					data: {
						"delete_lead_id": delete_lead_id
					},
					success: function(data) {
						$('.modal').modal('hide');
						if (data.match("true")) {
							toastr.success('Lead deleted successfully');
							window.location.reload();
						} else {
							toastr.error('Unable to delete lead');
						}
					}
				});
				return false;
			}
		</script>

		<!-- Upload CSV script -->
		<!-- <script>  
      $(document).ready(function(){  
           $('#upload_csv').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "ajax_leads/upload_lead.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
						$('.modal').modal('hide');
						var des = data.split("@");
						console.log("hello");

						Swal.fire({
	                        icon: 'info',
	                        position: 'top-end',
	                        html: 'Total Leads : '+des[0]+'<br>Uploaded Leads : '+des[1]+'<br>Re-Enquired Leads : '+des[2]+'<br>',
	                        confirmButtonText: `OK`
	                    });
                     }  
                })  
           });  
      });  
 </script> -->

		<script>
			$(document).ready(function() {
				$('#upload_csv').on("submit", function(e) {
					e.preventDefault(); //form will not submitted  
					$.ajax({
						url: "ajax_leads/upload_lead.php",
						method: "POST",
						data: new FormData(this),
						contentType: false, // The content type used when sending data to the server.  
						cache: false, // To unable request pages to be cached  
						processData: false, // To send DOMDocument or non processed data file it is set to false  
						success: function(data) {
							console.log(data);
							$('.modal').modal('hide');
							if (data.match("true")) {
								toastr.success('Lead uploaded successfully');
								window.location.reload();
							} else {
								toastr.error('Unable to upload lead');
							}
						}
					})
				});
			});
		</script>

		<script type='text/javascript'>
			function updateOwner() {
				var lead_id = $('#lead_id').val();
				var lead_owner_id = $('#lead_owner').val();
				var new_stage = $('#follow_stage_select').val();
				var new_reason = $('#follow_reason').val();
				var refer_lead_owner_id = $('#refer_lead_owner').val();
				var refer_comment = $('#referal_comment').val();
				$.ajax({
					type: "POST",
					url: "ajax_leads/refer_lead_sql.php",
					data: {
						"lead_owner_id": lead_owner_id,
						"refer_lead_owner_id": refer_lead_owner_id,
						"refer_comment": refer_comment,
						"lead_id": lead_id,
						"new_stage": new_stage,
						"new_reason": new_reason
					},
					success: function(data) {
						console.log(data);
						$('.modal').modal('hide');
						if (data.match("true")) {
							toastr.success('Lead refered successfully');
							window.location.reload();
						} else {
							toastr.error('Unable to refer lead');
						}
					}
				});
				return false;
			}
		</script>

		<script>
			function add_active(val) {

				var oldURL = window.location.href.split("?")[0];

				var newURL = oldURL + "?tabIndex=" + val;

				history.pushState(null, '', newURL);

				$('.user_tabs').removeClass('active');
				if (val == 0) {
					$('#all_lead').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 1) {
					$('#new_lead').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 2) {
					$('#follow_up').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 3) {
					$('#not_interested').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 4) {
					$('#reg_done').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 5) {
					$('#admi_done').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 6) {
					$('#intr_b2b').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 7) {
					$('#not_connect').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 8) {
					$('#re_enquiry').addClass('active');
					$('#subdis').css({
						display: "block"
					});
				} else if (val == 9) {
					$('#my_lead').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 10) {
					$('#intr_for_next_session').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 11) {
					$('#11').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 12) {
					$('#12').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 13) {
					$('#13').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 14) {
					$('#14').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 15) {
					$('#15').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				} else if (val == 16) {
					$('#16').addClass('active');
					$('#subdis').css({
						display: "none"
					});
				}
				getSubDisps();

			}

			function getSubDisps() {

				var tabValue = $('.user_tabs.active').val();
				console.log(tabValue);
				if (tabValue > 0) {
					$("#subdis").css({display: "block"});
					var stage_id = tabValue;
					$.ajax({
						type: "POST",
						url: "onselect/onStageSelect_all.php",
						data: {"stage_id": stage_id},
						success: function(response) {
							console.log(response);
							if (response != "") {
								$('#subdisposition_select').html(response);
							} else {
								$('#subdisposition_select').html("<option disabled selected>Select Sub-Disposition</option>");
							}
						}
					});
				}
			}

			function getList(vars = "") {
				var lead_date = $('#range-datepicker').val();
				var searchstring = $('#searchbox').val();
				var tabValue = $('.user_tabs.active').val();
				var subdivision = $('#subdisposition_select').val();
				//alert(tabValue);

				var row_count = $('#row-count').val();
				if (row_count > 0) {
					var itemPerPage = row_count;
				} else {
					var itemPerPage = 25;
				}
				var page = 1;
				var totalPages = 0;
				if ($("#pagination_info").attr("data-totalpages") != undefined) {
					totalPages = $("#pagination_info").attr("data-totalpages");
				}

				if ($("#pagination_info").attr("data-currentpage") != undefined) {
					page = $("#pagination_info").attr("data-currentpage");
				}

				if (vars == 'next' && page > 0 && totalPages != page) {
					page = parseInt(page) + 1;
					// $('#next').attr('data-page',page);
					$("#pagination_info").attr("data-currentPage", page)
				} else {
					//$('#next').attr('data-page',1);
					$('#next').attr('disabled', 'disabled');
				}

				if (vars == 'pre' && page > 1) {
					page = parseInt(page) - 1;
					$('#pre').attr('data-page', page);
					// value='';
				} else {
					$('#pre').attr('data-page', 1);
					$('#pre').attr('disabled', 'disabled');
				}
				console.log(tabValue);
				$.ajax({
					url: "fetch_leads.php",
					type: "GET",
					global: true,
					data: {
						"lead_date": lead_date,
						"subdivision": subdivision,
						"tabValue": tabValue,
						"searchstring": searchstring,
						"page": page,
						"itemPerPage": itemPerPage
					},
					success: function(data) {
						console.log(data);
						$("#leads_tbody").html(data);
						$('html, body').animate({
							scrollTop: 0
						}, 'slow');

					}
				});
			}
			getList();
		</script>
		<script>
			function checkAllBoxes() {
				var value = $("#select_all_boxes").val();

				if (value === "not-checked") {
					$(".custom-control-input").prop('checked', true);
					$("#select_all_boxes").val("checked");
					checkbox();
				} else {
					$(".custom-control-input").prop('checked', false);
					$("#select_all_boxes").val("not-checked");
					checkbox();
				}
			}
		</script>

		<script type="text/javascript">
			function copyName(id) {
				var copyText = document.getElementById(id);
				var textArea = document.createElement("textarea");
				textArea.value = copyText.textContent;
				textArea.value = textArea.value.split(": ")[1];
				document.body.appendChild(textArea);
				textArea.select();
				document.execCommand("Copy");
				textArea.remove();

				if (id.includes("person_name")) {
					toastr.info("Name copied");
				} else if (id.includes("person_email")) {
					toastr.info("Email copied");
				} else if (id.includes("person_mobile")) {
					toastr.info("Mobile copied");
				}
			}
		</script>

		<?php

		if (isset($_GET['tabIndex'])) {
		?>
			<script type="text/javascript">
				$(document).ready(function() {
					var tabVal = '<?php echo $_GET['tabIndex']; ?>';

					add_active(tabVal);
					getList();
				});
			</script>
		<?php
		}

		?>
		<script>
			function LeadFilter(vars = "") {
				$('#searchbox').val('');
				var tabValue = $('.user_tabs.active').val();
				var itemPerPage = 25;
				var page = 1;
				var totalPages = 0;
				var number_rows = $('#number_rows').val();
				if (number_rows > 0) {
					var itemPerPage = number_rows;
				} else {
					var itemPerPage = 25;
				}
				if ($("#pagination_info").attr("data-totalpages") != undefined) {
					totalPages = $("#pagination_info").attr("data-totalpages");
				}

				if ($("#pagination_info").attr("data-currentpage") != undefined) {
					page = $("#pagination_info").attr("data-currentpage");
				}

				if (vars == 'next' && page > 0 && totalPages != page) {
					page = parseInt(page) + 1;
					// $('#next').attr('data-page',page);
					$("#pagination_info").attr("data-currentPage", page)
				} else {
					//$('#next').attr('data-page',1);
					$('#next').attr('disabled', 'disabled');
				}

				if (vars == 'pre' && page > 1) {
					page = parseInt(page) - 1;
					$('#pre').attr('data-page', page);
					// value='';
				} else {
					$('#pre').attr('data-page', 1);
					$('#pre').attr('disabled', 'disabled');
				}

				var counsellor_filter = $('#counsellor_filter').val();
				var campaign_name_filter = $('#campaign_name_filter').val();
				var source_filter = $('#source_filter').val();
				var sub_source_filter = $('#sub_source_filter').val();
				var institute_filter = $('#institute_filter').val();
				var course_filter = $('#course_filter').val();
				var speclization_filter = $('#speclization_filter').val();
				var communication_type_filter = $('#communication_type_filter').val();
				var stage_filter = $('#stage_filter').val();
				var reason_filter = $('#reason_filter').val();
				var creation_date_filter = $('#creation_date_filter').val();
				var update_date_filter = $('#update_date_filter').val();
				var sub_dis = $('#sub_dispostion').val();
				var state_filter = $('#state_filter').val();
				var user = $('#b2b').val();

				$.ajax({
					url: "fetch_lead_filter.php",
					type: "POST",
					data: (user == "b2b") ? {
						"state_filter": state_filter,
						"sub_dis": sub_dis,
						"tabValue": tabValue,
						"counsellor_filter": counsellor_filter,
						"campaign_name_filter": campaign_name_filter,
						"source_filter": source_filter,
						"sub_source_filter": sub_source_filter,
						"course_filter": course_filter,
						"speclization_filter": speclization_filter,
						"communication_type_filter": communication_type_filter,
						"stage_filter": stage_filter,
						"reason_filter": reason_filter,
						"creation_date_filter": creation_date_filter,
						"update_date_filter": update_date_filter,
						"page": page,
						"itemPerPage": itemPerPage
					} : {
						"state_filter": state_filter,
						"sub_dis": sub_dis,
						"tabValue": tabValue,
						"counsellor_filter": counsellor_filter,
						"campaign_name_filter": campaign_name_filter,
						"source_filter": source_filter,
						"sub_source_filter": sub_source_filter,
						"institute_filter": institute_filter,
						"course_filter": course_filter,
						"speclization_filter": speclization_filter,
						"communication_type_filter": communication_type_filter,
						"stage_filter": stage_filter,
						"reason_filter": reason_filter,
						"creation_date_filter": creation_date_filter,
						"update_date_filter": update_date_filter,
						"page": page,
						"itemPerPage": itemPerPage
					},
					success: function(res) {
						console.log(res);
						$('#filterModal').modal('hide');
						$('#filterLeads').empty();
						$('#filterLeads').html(res);
					}
				})

			}
		</script>

		<script type='text/javascript'>
			function callLeads(id, number) {
				var userid = id;
				// AJAX request
				$.ajax({
					url: '/ajax_leads/call_modal.php',
					type: 'post',
					data: {
						"userid": userid,
						"number": number
					},
					success: function(response) {
						// Add response in Modal body
						$('#call-modal-body').html(response);

						// Display Modal
						$('#confirmleadcallmodal').modal('show');
					}
				});
			}
		</script>

		<script type="text/javascript">
			function callSVUApi(id, dob, spec, email) {
				var spec = spec.trim();
				var email = email.trim();
				if (String(dob).match('0000-00-00')) {
					dob = null;
				}
				if (dob && spec && email) {
					$.ajax({
						url: '/callsvuApi.php',
						type: 'POST',
						data: {
							id: id
						},
						success: function(res) {
							if (res) {
								get_svuid(id, res);
							} else {
								toastr.error('Try Again');
							}

						}
					});
				} else {
					if (!dob && !spec) {
						toastr.error('Please add date of birth and specialization ');
						editleaddob(id);
					} else if (!dob) {
						toastr.error('Please add date of birth.');
						editleaddob(id);
					} else if (!spec || spec.length == 0) {
						toastr.error('Please add specialization.');
						editleaddob(id);
					} else if (!email || email.length == 0) {
						toastr.error('Please add Email.');
						editleaddob(id);
					}
				}

			}

			function get_svuid(id, res) {
				var res = res.trim();
				if (res.length != 0) {
					$.ajax({
						url: '/callsvuidupdate.php',
						type: 'POST',
						data: {
							id: id,
							res: res
						},
						async: true,
						success: function(data) {
							if (data.match('Updated')) {
								toastr.success('SVU ID has been generated successfully.');
								window.location.reload();
							} else {
								toastr.error('Try Again');
								window.location.reload();
							}
						}
					});
				} else {
					toastr.error('Try Again');
				}
			}
		</script>

		<script type="text/javascript">
			function callCuApi(id) {
				$.ajax({
					url: '/callCuApi.php',
					type: 'POST',
					data: {
						id: id
					},
					success: function(res) {
						// console.log(res);
						if (res.match('dobcity')) {
							toastr.error('Please add State, City and DOB.');
							editleaddob(id);
						} else if (res.match('city')) {
							toastr.error('Please add State, City.');
							editleaddob(id);
						} else if (res.match('dob')) {
							toastr.error('Please add DOB.');
							editleaddob(id);
						} else if (res.match('email')) {
							toastr.error('Please add Email.');
							editleaddob(id);
						} else if (res.match('updated')) {
							toastr.success("IDOL ID generated successfully!");
							update_idol_div(id);
						} else {
							get_cc_id(id, res);
						}
					}
				})

			}

			function get_cc_id(id, res) {
				var Data = $.ajax({
					url: 'https://api.cuchd.in/api/4/PostJsonRegistrationIDOLAPI?Data=' + res,
					type: 'GET',
					dataType: "json",
					async: true,
					complete: function(data) {
						var response = data.responseText;
						if (response.match("Already Exist")) {
							toastr.info("IDOL ID already generated!");
						} else if (response.match('saved')) {
							idol_update(id, response);
						} else {
							toastr.warning(response);
						}

					},
				});

				function idol_update(id, response) {
					$.ajax({
						url: '/callCuApiUpdate.php',
						type: 'POST',
						data: {
							id: id,
							data: response
						},
						success: function(data) {
							if (data.match('true')) {
								toastr.success("IDOL ID generated successfully!");
								update_idol_div(id);
							} else {
								toastr.error("Something went wrong!");
							}
						}
					})
				}
			}

			function update_idol_div(id) {
				$.ajax({
					url: '/ajax_leads/get_idol_id.php',
					data: {
						'id': id
					},
					type: 'POST',
					success: function(data) {
						$("#idolid" + id).html(data);
					}
				})
			}
		</script>

		<?php include 'filestobeincluded/footer-top.php' ?>
		<?php include 'filestobeincluded/footer-bottom.php' ?>
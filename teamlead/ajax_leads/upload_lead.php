<?php require "../filestobeincluded/db_config.php" ?>

<?php
if (session_status() === PHP_SESSION_NONE) session_start();



if (!empty($_FILES['lead_file']['tmp_name'])) {

	$total_leads_up = 0;
	$uploaded_leads = 0;
	$re_enquired_leads = 0;

	$output = '';
	$allowed_ext = array("csv");
	$extension = explode(".", $_FILES["lead_file"]["name"]);
	$extension = end($extension);
	if (in_array($extension, $allowed_ext)) {
		$file_data = fopen($_FILES['lead_file']['tmp_name'], 'r');

		$stage_arr = array();
		$stage_query = $conn->query("SELECT ID,Name FROM Stages");
		while ($row = mysqli_fetch_assoc($stage_query)) {
			$stage_arr[$row['Name']] = $row['ID'];
		}

		$reason_arr = array();
		$reason_query = $conn->query("SELECT ID,Name FROM Reasons");
		while ($row = mysqli_fetch_assoc($reason_query)) {
			$reason_arr[$row['Name']] = $row['ID'];
		}

		$institute_arr = array();
		$institute_query = $conn->query("SELECT ID,Name FROM Institutes");
		while ($row = mysqli_fetch_assoc($institute_query)) {
			$institute_arr[$row['Name']] = $row['ID'];
		}

		$course_arr = array();
		$course_query = $conn->query("SELECT ID,Name FROM Courses");
		while ($row = mysqli_fetch_assoc($course_query)) {
			$course_arr[$row['Name']] = $row['ID'];
		}

		$specialization_arr = array();
		$specialization_query = $conn->query("SELECT ID,Name FROM Specializations");
		while ($row = mysqli_fetch_assoc($specialization_query)) {
			$specialization_arr[$row['Name']] = $row['ID'];
		}

		$state_arr = array();
		$state_query = $conn->query("SELECT ID,Name FROM States");
		while ($row = mysqli_fetch_assoc($state_query)) {
			$state_arr[$row['Name']] = $row['ID'];
		}

		$source_arr = array();
		$source_query = $conn->query("SELECT ID,Name FROM Sources");
		while ($row = mysqli_fetch_assoc($source_query)) {
			$source_arr[$row['Name']] = $row['ID'];
		}

		$subsource_arr = array();
		$subsource_query = $conn->query("SELECT ID,Name FROM Sub_Sources");
		while ($row = mysqli_fetch_assoc($subsource_query)) {
			$subsource_arr[$row['Name']] = $row['ID'];
		}

		$employee_id_arr = array();
		$employee_id_query = $conn->query("SELECT ID,Name FROM users");
		while ($row = mysqli_fetch_assoc($employee_id_query)) {
			$employee_id_arr[$row['ID']] = $row['ID'];
		}

		fgetcsv($file_data);
		while ($row = fgetcsv($file_data)) {

			$fullname = mysqli_real_escape_string($conn, $row[0]);
			$fullname = utf8_encode($fullname);
			$email = mysqli_real_escape_string($conn, $row[1]);
			$phone = mysqli_real_escape_string($conn, $row[2]);
			$alt_phone = mysqli_real_escape_string($conn, $row[3]);
			$stage = mysqli_real_escape_string($conn, $row[4]);

			
			$stage_Id = isset($stage_arr[$stage]) ? $stage_arr[$stage] : " ";


			$reason = mysqli_real_escape_string($conn, $row[5]);
			$remarks = mysqli_real_escape_string($conn, $row[6]);

			

			$reason_Id = isset($reason_arr[$reason]) ? $reason_arr[$reason] : " ";

			$institute = mysqli_real_escape_string($conn, $row[7]);

			// $institute_query = $conn->query("SELECT * FROM Institutes WHERE Name = '$institute'");
			// if($institute_query->num_rows > 0) {
			// 	$institute_fet = mysqli_fetch_assoc($institute_query);
			// 	$institute_Id = $institute_fet['ID'];
			// }
			// else {
			// 	$institute_Id = '';
			// }

			$institute_Id = isset($institute_arr[$institute]) ? $institute_arr[$institute] : " ";

			if($_SESSION['User_Type'] == 'b2b'){
				$institute_Id = 64;
			}

			$course = mysqli_real_escape_string($conn, $row[8]);

			$course_query = $conn->query("SELECT * FROM Courses WHERE Name = '$course' AND Institute_ID = '$institute_Id'");
			if ($course_query->num_rows > 0) {
				$course_fet = mysqli_fetch_assoc($course_query);
				$course_Id = $course_fet['ID'];
			} else {
				$course_Id = '';
			}



			$specialization = mysqli_real_escape_string($conn, $row[9]);

			$specialization_query = $conn->query("SELECT * FROM Specializations WHERE Name = '$specialization' AND Institute_ID = '$institute_Id' AND Course_ID = '$course_Id'");
			if ($specialization_query->num_rows > 0) {
				$specialization_fet = mysqli_fetch_assoc($specialization_query);
				$specialization_Id = $specialization_fet['ID'];
			} else {
				$specialization_Id = '';
			}

			$state = mysqli_real_escape_string($conn, $row[10]);
			$city = mysqli_real_escape_string($conn, $row[11]);
			$source = mysqli_real_escape_string($conn, $row[12]);
			$subsource = mysqli_real_escape_string($conn, $row[13]);
			$employee_id = mysqli_real_escape_string($conn, $row[14]);

			// $state_query = $conn->query("SELECT * FROM States WHERE Name = '$state'");
			// if($state_query->num_rows > 0) {
			// 	$state_fet = mysqli_fetch_assoc($state_query);
			// 	$state_Id = $state_fet['ID'];
			// }
			// else {
			// 	$state_Id = '';
			// }

			$state_Id = $state_arr[$state];

			$city_query = $conn->query("SELECT * FROM Cities WHERE Name = '$city'");
			if ($city_query->num_rows > 0) {
				$city_fet = mysqli_fetch_assoc($city_query);
				$city_Id = $city_fet['ID'];
			} else {
				$city_Id = '';
			}

			// $source_query = $conn->query("SELECT * FROM Sources WHERE Name = '$source'");
			// if($source_query->num_rows > 0) {
			// 	$source_fet = mysqli_fetch_assoc($source_query);
			// 	$source_Id = $source_fet['ID'];
			// }
			// else {
			// 	$source_Id = '';
			// }

			$source_Id = isset($source_arr[$source]) ? $source_arr[$source] : " ";

			// $subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE Name = '$subsource'");
			// if($subsource_query->num_rows > 0) {
			// 	$subsource_fet = mysqli_fetch_assoc($subsource_query);
			// 	$subsource_Id = $subsource_fet['ID'];
			// }
			// else {
			// 	$subsource_Id = '';
			// }

			$subsource_Id = isset($subsource_arr[$subsource]) ? $subsource_arr[$subsource] : " ";

			// $employee_id_query = $conn->query("SELECT * FROM users WHERE ID = '$employee_id'");
			// if($employee_id_query->num_rows > 0) {
			// 	$employee_fet = mysqli_fetch_assoc($employee_id_query);
			// 	$employee_Id = $employee_fet['ID'];
			// }
			// else {
			// 	$employee_Id = '';
			// }

			$employee_Id = isset($employee_id_arr[$employee_id]) ? $employee_id_arr[$employee_id] : " ";

			$total_leads_up++;

			if ($alt_phone != '') {
				$check = $conn->query("SELECT * FROM `Leads` WHERE ((Alt_Mobile like '$alt_phone' OR Mobile like '$alt_phone'))  AND Institute_ID = '$institute_Id'");
			} elseif ($phone != '') {
				$check = $conn->query("SELECT * FROM `Leads` WHERE ((Mobile like '$phone' OR Alt_Mobile like '$phone')) AND Institute_ID = '$institute_Id'");
			} elseif ($email != '') {
				$check = $conn->query("SELECT * FROM `Leads` WHERE (Email like '$email') AND Institute_ID = '$institute_Id'");
			}
			if ($check->num_rows == 0) {
				$uploaded_leads++;
				$upload_lead_suc = $conn->query("INSERT INTO Leads(Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID)  VALUES ('$fullname', '$email', '$phone', '$alt_phone', '$stage_Id', '$reason_Id', '$remarks', '$institute_Id', '$course_Id', '$specialization_Id', '$state_Id', '$city_Id', '$source_Id', '$subsource_Id', '$employee_Id')");
			} else {
				$already_added_lead_details = mysqli_fetch_assoc($check);
				$lead_ID = $already_added_lead_details['ID'];


				// send_re_enquired_mail($conn, $lead_ID, $source_Id, $course_Id);

				$lead_stage_id = $already_added_lead_details['Stage_ID'];

				if (strcasecmp($lead_stage_id, "4") == 0 || strcasecmp($lead_stage_id, "5") == 0 || strcasecmp($lead_stage_id, "6") == 0) {
					$insert_new_lead = true;
				} else {
					$move_lead = $conn->query("UPDATE Leads SET Stage_ID = '8', Reason_ID = '' WHERE ID = '" . $lead_ID . "'");
				}

				$re_enquired_leads++;

				if ($_SESSION['User_Type'] == "b2b") {
					$add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`, `Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID` , `dob`,`exp`) SELECT * FROM Leads WHERE ID = '" . $lead_ID . "'");
				} else {
					$add_history = $conn->query("INSERT INTO History (`Lead_ID`, `TimeStamp`, `Created_at`, `Stage_ID`, `Reason_ID`, `Name`, `Email`, `Mobile`, `Alt_Mobile`, `Remarks`, `Address`, `State_ID`, `City_ID`, `Pincode`, `Source_ID`, `Subsource_ID`, `CampaignName`, `Previous_Owner_ID`, `School`, `Grade`, `Qualification`, `Refer`, `Institute_ID`, `Course_ID`, `Specialization_ID`, `Counsellor_ID` , `dob`) SELECT * FROM Leads WHERE ID = '" . $lead_ID . "'");
				}
				$upload_lead_suc = $conn->query("INSERT INTO Re_Enquired(Lead_ID, Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID)  VALUES ('$lead_ID', '$fullname', '$email', '$phone', '$alt_phone', '$stage_Id', '$reason_Id', '$remarks', '$institute_Id', '$course_Id', '$specialization_Id', '$state_Id', '$city_Id', '$source_Id', '$subsource_Id', '$employee_Id')");
			}
			// $upload_lead_suc = $conn->query("INSERT INTO Upload_Leads(Name, Email, Mobile, Alt_Mobile, Stage_ID, Reason_ID, Remarks, Institute_ID, Course_ID, Specialization_ID, State_ID, City_ID, Source_ID, Subsource_ID, Counsellor_ID)  VALUES ('$fullname', '$email', '$phone', '$alt_phone', '$stage_Id', '$reason_Id', '$remarks', '$institute_Id', '$course_Id', '$specialization_Id', '$state_Id', '$city_Id', '$source_Id', '$subsource_Id', '$employee_Id')");

		}

		echo $total_leads_up . "@" . $uploaded_leads . "@" . $re_enquired_leads;
	}
}

?>
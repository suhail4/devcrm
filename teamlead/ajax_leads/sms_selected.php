<?php 
require '../filestobeincluded/db_config.php';
$selected_leads = $_POST['data_id'];

$selected_leads = str_replace("%5B", "", $selected_leads);
$selected_leads = str_replace("%5D", "", $selected_leads);
$selected_leads = str_replace("id=", "", $selected_leads);
$selected_leads = str_replace("&", ",", $selected_leads);
$selected_leads = '('.$selected_leads.')';

$all_leads=array();
$leads_query_res = $conn->query("SELECT * FROM Leads WHERE ID in $selected_leads");
while($row = $leads_query_res->fetch_assoc()) {
    $all_leads[] = $row;
}
?>
<form method="POST" id="send_sms_form">

<div class="form-group row">
    <div class="col-lg-12">
        <select data-plugin="customselect" class="form-control" id="sms_template_selected">
            <option disabled selected>Choose Template</option>
            <?php
                $result_SMS_template = $conn->query("SELECT * FROM SMS_Templates WHERE Institute_ID = '".$_SESSION['INSTITUTE_ID']."'");
                while($sms_temp = $result_SMS_template->fetch_assoc()) {
            ?>
                <option value="<?php echo $sms_temp['id']; ?>"><?php echo $sms_temp['sms_template_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12">
        <textarea class="input-block-level form-control" rows="10" id="sms_textarea_selected">
                
        </textarea>
    </div>
</div>
    

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" id="sms_btn" class="btn btn-primary" onclick="sendSelectedSMS();">&nbsp;&nbsp;Send&nbsp;&nbsp;</button>
</div>
</form>

<script>
var url = window.location.pathname;
console.log(url);
function sendSelectedSMS() {
    if($('#send_sms_form').valid()) {
    var sms_temp_body = $('#sms_textarea_selected').val();
    var mobile = '<?php foreach($all_leads as $r_leads){ echo $r_leads['Mobile'].','; } ?>';
    var url = window.location.pathname;
    $.ajax
        ({
            type: "POST",
            url: "/Twilio/ajax_selected_sms.php",
            beforeSend: disableSendButton,
            complete: enableSendButton,
            data: { "sms_temp_body" :sms_temp_body, "mobile" :mobile },
            success: function (data) {
            console.log(data);
            if(data.match("true")) {
                $('.modal').modal('hide');
                $("#divShowHide1").css({display: "none"});
                $("#divShowHide2").css({display: "none"});
                $("#divShowHide3").css({display: "none"});
                $("#divShowHide4").css({display: "none"});
                toastr.success('SMS send successfully');
                $('#checkbox-form').find('input[name="id[]"]:checked').each(function () {
                    var id = $(this).val();
                    if(url=='/teamlead/myfollowup'){
                        $.ajax({
                            url:'ajax_leads/fetch_followup_lead_data.php',
                            data:{id:id},
                            type:'POST',
                            success:function(data){
                                $('#row'+id).html(data);
                            }
                        })
                    }else{
                        $.ajax({
                            url:'ajax_leads/fetch_lead_data.php',
                            data:{id:id},
                            type:'POST',
                            success:function(data){
                                $('#row'+id).html(data);
                            }
                        })        
                    }
                });
                $(".checkbox-function").prop('checked', false);
            }
            else {
                toastr.error('Unable to send SMS');
            }
            }
        });
    }
}
</script>

<script>
    function enableSendButton() {
        $("#sms_btn").prop("disabled", false);
        $('#sms_btn').css('cursor', 'pointer');
        $('#sms_btn').text('Send');
    }

    function disableSendButton() {
        $("#sms_btn").prop("disabled", true);
        $('#sms_btn').css('cursor', 'no-drop');
        $('#sms_btn').text('Sending...');
    }
</script>

<script>
    $(document).ready(function() {
        $('#send_sms_form').validate({
            rules: {
                sms_textarea_selected: {
                    required: true
                }
            },
            messages: {
                sms_textarea_selected: {
                    required: 'Please select a template'
                }
            },
            highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
            }
        });
    })    
</script>

<script>
    $(document).ready(function() {
        $('#sms_template_selected').change(function() {

            $('#sms_textarea').val("Select Template");
            var template_id = $('#sms_template_selected').val();

            $.ajax
            ({
                type: "POST",
                url: "ajax_leads/ajax_sms.php",
                data: { "sms_template_id": template_id },
                success: function(data) {
                    if(data != "") {
                        $('#sms_textarea_selected').val(data);
                    }
                    else {
                        $('#sms_textarea_selected').val("Select Template");
                    }
                }
            });
        });
    });
</script>
<?php
exit;
?>
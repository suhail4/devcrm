<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>CRM - Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="" name="description" />
        <meta content="Black Board & Virtual Analytics" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/blackboard.ico">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/toastr.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg">

    <?php require 'filestobeincluded/db_config.php' ?>
        

            <?php

            
        
            if(session_status() === PHP_SESSION_NONE) if(session_status() === PHP_SESSION_NONE) session_start();
            if(isset($_SESSION["useremployeeid"]) == true){
            	if(strcasecmp($_SESSION['role'], 'Administrator')==0) {
            		header("location: dashboard");
            	}
            	else if(strcasecmp($_SESSION['role'], 'Counsellor')==0) {
            		header("location: /counsellor/dashboard");
            	}else if(strcasecmp($_SESSION['role'], 'Manager')==0) {
            		header("location: /teamlead/dashboard");
            	}
            }
                global $ROLE;
                if(isset($_POST['loginbutton'])) {
                    $employee_id = $_POST['useremployeeid'];
                    $password = $_POST['userpass'];
                    $admin_array = array();
                    $admin_results = $conn->query("SELECT * FROM users WHERE ID = '".$employee_id."' && Password = AES_ENCRYPT('".$password."','60ZpqkOnqn0UQQ2MYTlJ')");
                    if($admin_results->num_rows == 1){
                        if($admin_results) {
                            while($row = $admin_results->fetch_assoc()) {
                                $admin_array[] = $row; 
                            }
                        }
                        foreach($admin_array as $admins) {
                            $STATUS = $admins['Status'];
                            $ROLE = $admins['Role'];
                            $_SESSION['INSTITUTE_ID'] = $admins['Institute_ID'];
                            $_SESSION['USERS_ID'] = $admins['ID'];
                            $_SESSION['User_Type'] = $admins['User_Type'];
                        }
                        if(strcasecmp($STATUS, 'Y')==0){
                            $login = $conn->query("SELECT * FROM users WHERE ID = '".$employee_id."' && Password = AES_ENCRYPT('".$password."','60ZpqkOnqn0UQQ2MYTlJ')");
                                if($login->num_rows == 1) {
                                    if($_SESSION['useremployeeid'] = $employee_id AND $_SESSION['userpass'] = $password){
                                        echo "<script>
                                        window.onload = function () {
                                            toastr.success('Success');
                                        };                            
                                        </script>";
                                        if($ROLE == 'Administrator'){
                                            $_SESSION['role'] = 'Administrator';
                                            echo '<script>';
                                            echo 'window.location.href = "dashboard";';                            
                                            echo '</script>';
                                        }else if($ROLE == 'Counsellor'){
                                            $_SESSION['role'] = 'Counsellor';
                                            echo '<script>';
                                            echo 'window.location.href = "/counsellor/dashboard";';                            
                                            echo '</script>';
                                        }else if($ROLE == 'Manager'){
                                            $_SESSION['role'] = 'Manager';
                                            echo '<script>';
                                            echo 'window.location.href = "/teamlead/dashboard";';                            
                                            echo '</script>';
                                        }else{
                                            echo "<script>
                                                window.onload = function () {
                                                    toastr.warning('Access Denied!');
                                                };                            
                                            </script>";
                                        }

                                    }
                                    
                                }else{
                                    echo "<script>
                                        window.onload = function () {
                                            toastr['error']('Check Email or password')
                                        };                            
                                    </script>";
                                }
                            }
                        }else {
                            echo "<script>
                            window.onload = function () {
                                toastr.warning('Invalid Email or Password');
                            };                            
                        </script>";
                        }
                }
            
        
        ?>
         
        <div class="account-pages my-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-md-12 p-5">
                                        <div class="mx-auto mb-5">
                                            <a href="/crm">
                                                <img src="assets/images/logo.png" alt="" height="48" />
                                            </a>
                                        </div>

                                        <h6 class="h5 mb-0 mt-4">Welcome back!</h6>
                                        <br><br>

                                        <form class="authentication-form" method="POST">
                                            <div class="form-group">
                                                <label class="form-control-label">Employee ID</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="briefcase"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" name="useremployeeid" id="employee_id" placeholder="hello@blackboard.com">
                                                </div>
                                            </div>

                                            <div class="form-group mt-4">
                                                <label class="form-control-label">Password</label>
                                                <a href="recoverpw" class="float-right text-muted text-unline-dashed ml-1">Forgot your password?</a>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="lock"></i>
                                                        </span>
                                                    </div>
                                                    <input type="password" class="form-control" id="password" name="userpass"
                                                        placeholder="Enter your password">
                                                </div>
                                            </div>

                                            <div class="form-group mb-4">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox-signin" checked>
                                                    <label class="custom-control-label" for="checkbox-signin">Remember
                                                        me</label>
                                                </div>
                                            </div>

                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-primary btn-block" name="loginbutton" type="submit"><i data-feather="log-in"></i> Log In
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="assets/js/vendor.min.js"></script>

        <!-- App js -->
        <script src="assets/js/app.min.js"></script>
        <script src="assets/js/toastr.min.js"></script>
        
        <script>
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
        </script>
        
    </body>
</html>
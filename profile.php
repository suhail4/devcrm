<?php include 'filestobeincluded/header-top.php' ?>
<?php include 'filestobeincluded/header-bottom.php' ?>
<?php include 'filestobeincluded/navigation.php' ?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                                <!--<ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Shreyu</a></li>
                                    <li class="breadcrumb-item"><a href="#">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Profile</li>
                                </ol>-->
                            </nav>
                            <h4 class="mb-1 mt-0">My Account</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="card-body" id="user_profile">
                                    <div class="text-center mt-3">
                                        <?php
                                            $user_info = array();
                                            $user_employee_id = $_SESSION['useremployeeid'];
                                            $user_query = $conn->query("SELECT *, CAST(AES_DECRYPT(password, '60ZpqkOnqn0UQQ2MYTlJ') AS CHAR(50)) pass FROM users WHERE ID = '$user_employee_id'");
                                            while($user_details = $user_query->fetch_assoc()) {
                                                $user_info[] = $user_details;
                                            }
                                            foreach($user_info as $ud){
                                                
                                        ?>
                                        <input value="<?php echo $ud['pass'] ?>" type="hidden" id="pre_passwd">
                                        <img src="assets/images/users/avatar-7.jpg" alt="" class="avatar-lg rounded-circle" />
                                        <h5 class="mt-2 mb-0"><?php echo ucfirst($ud['Name']); ?></h5>
                                        <h6 class="text-muted font-weight-normal mt-2 mb-0"><?php echo ($ud['Role']); ?>
                                        </h6>
                                        <br>
                                        
                                        
                                        
                                    </div>

                                    <!-- profile  -->
                                    
                                    <div class="mt-3 pt-2 border-top">
                                        <h4 class="mb-3 font-size-15">Contact Information</h4>
                                        <div class="table-responsive">
                                            <table class="table table-borderless mb-0 text-muted">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Email</th>
                                                        <td><?php echo $ud['Email']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Phone</th>
                                                        <td><?php echo $ud['Mobile']; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="mt-3 pt-2 border-top">
                                        <h4 class="mb-3 font-size-15 btn btn-warning update-password" style="cursor: pointer;">Update Password</h4>
                                        <div id="show_field" class="border-top" style="display: none;">
                                        <form method="POST">
                                                <input type="hidden" id="user_id" value="<?php echo $ud['ID']; ?>" />
                                                <div class="form-group row">
                                                    <label class="col-lg-12 col-form-label"
                                                        for="previous_pass">Enter Previous Password</label>
                                                    <div class="col-lg-12">
                                                        <input type="password" class="form-control" id="previous_pass" onblur="checkPrePasswd();">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-12 col-form-label"
                                                        for="new_pass">New Password</label>
                                                    <div class="col-lg-12">
                                                        <input type="password" class="form-control" id="new_pass"
                                                            placeholder="Enter New Password">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-12 col-form-label"
                                                        for="confirm_new_pass">Confirm New Password</label>
                                                    <div class="col-lg-12">
                                                        <input type="password" class="form-control" id="confirm_new_pass"
                                                            placeholder="Enter New Password">
                                                    </div>
                                                </div>
                                                <button type="button" style="float: right;" class="btn btn-primary" id="up-pass" disabled onClick="return Validate();">Update</button>
                                            </form>

                                            <script>
                                                function checkPrePasswd() {
                                                    var passwd = document.getElementById("pre_passwd").value;
                                                    var pre_password = document.getElementById("previous_pass").value;
                                                    if(passwd != pre_password){
                                                        toastr.error("Previous password not matched");
                                                        document.getElementById("up-pass").disabled = true;
                                                    }else{
                                                        document.getElementById("up-pass").disabled = false;
                                                    }
                                                }
                                            </script>
                                            <script type="text/javascript">
                                                function Validate() {
                                                    var pre_password = document.getElementById("previous_pass").value;
                                                    var password = document.getElementById("new_pass").value;
                                                    var confirmPassword = document.getElementById("confirm_new_pass").value;
                                                    if(password.length == 0){
                                                        toastr.warning("Please Enter Password");
                                                    }else if(password.length < 6){
                                                        toastr.warning("Minimum 6 character required");
                                                    }else if(password == pre_password){
                                                        toastr.info("Old and new passwords are same");
                                                    }
                                                    else if (password != confirmPassword) {
                                                        toastr.error('Password not match');
                                                        return false;
                                                    }
                                                    else{
                                                        var new_user_id = $('#user_id').val();
                                                        var new_user_password = $('#new_pass').val();
                                                        $.ajax
                                                        ({
                                                        type: "POST",
                                                        url: "settings_pages/ajax_user/update_user_pass.php",
                                                        data: { "new_user_id": new_user_id, "new_user_password": new_user_password },
                                                        success: function (data) {
                                                            //console.log(data);
                                                            if(data.match("true")) {
                                                                toastr.success('Password updated successfully');
                                                                window.location.reload();
                                                            }
                                                            else {
                                                                toastr.error('Unable to update password');
                                                            }
                                                        }
                                                        });
                                                        return false;
                                                    }
                                                    
                                                }
                                            </script>

                                        </div> 
                                    </div>
                                    <script>
                                        $(".update-password").click(function(){
                                            $("#show_field").toggle();
                                        });
                                    </script>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- end card -->

                        </div>

                        <div class="col-lg-9">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-pills navtab-bg nav-justified" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-activity-tab" data-toggle="pill"
                                                href="#pills-activity" role="tab" aria-controls="pills-activity"
                                                aria-selected="true">
                                                Activity
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-messages-tab" data-toggle="pill"
                                                href="#pills-messages" role="tab" aria-controls="pills-messages"
                                                aria-selected="false">
                                                Messages
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-projects-tab" data-toggle="pill"
                                                href="#pills-projects" role="tab" aria-controls="pills-projects"
                                                aria-selected="false">
                                                Projects
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-tasks-tab" data-toggle="pill"
                                                href="#pills-tasks" role="tab" aria-controls="pills-tasks"
                                                aria-selected="false">
                                                Tasks
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-files-tab" data-toggle="pill"
                                                href="#pills-files" role="tab" aria-controls="pills-files"
                                                aria-selected="false">
                                                Files
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-activity" role="tabpanel"
                                            aria-labelledby="pills-activity-tab">
                                            
                                        </div>

                                        <!-- messages -->
                                        <div class="tab-pane" id="pills-messages" role="tabpanel"
                                        aria-labelledby="pills-messages-tab">
                                        
                                        </div>

                                        <div class="tab-pane fade" id="pills-projects" role="tabpanel"
                                            aria-labelledby="pills-projects-tab">

                                            
                                        </div>

                                        <div class="tab-pane fade" id="pills-tasks" role="tabpanel"
                                            aria-labelledby="pills-tasks-tab">
                                            
                                        </div>

                                        <div class="tab-pane fade" id="pills-files" role="tabpanel"
                                            aria-labelledby="pills-files-tab">
                                            
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- end card -->
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container-fluid -->

            </div> <!-- content -->



    <?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>
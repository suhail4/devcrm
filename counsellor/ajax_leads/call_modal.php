<!----------------------->
<?php

if (session_status() === PHP_SESSION_NONE) session_start();
require '../filestobeincluded/db_config.php';
$lead_id = $_POST['userid'];
$get_lead = $conn->query("SELECT Leads.*,Institutes.Name as Institute_Name,Stages.Name as Stage_Name,Specializations.Name as Specialization_Name,Courses.Name as Course_Name FROM `Leads` LEFT JOIN Institutes ON Leads.Institute_ID=Institutes.ID LEFT JOIN Stages ON Leads.Stage_ID=Stages.ID LEFT JOIN Courses ON Leads.Course_ID = Courses.ID LEFT JOIN Specializations ON Leads.Specialization_ID = Specializations.ID WHERE Leads.ID = '" . $lead_id . "'");
$remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '" . $lead_id . "' AND Counsellor_ID = '" . $_SESSION['useremployeeid'] . "' ORDER BY ID DESC LIMIT 1");
$remark = mysqli_fetch_assoc($remark);
$lead = mysqli_fetch_assoc($get_lead);
$number = $_POST['number'];
?>

<script>
    var lead_id = '<?php echo $lead_id ?>';
    var timeIn;
    var number = '<?php echo $number ?>';
    $.ajax({
        url: '/tata_api/click_to_call.php',
        data: {
            "number": number
        },
        type: 'POST',
        success: function(data) {

            var res = $.parseJSON(data);
            if (res.message === "Originate successfully queued") {
                $("#call").html("Ringing...");
                localStorage.setItem('minutesLabel', 00);
                localStorage.setItem('secondsLabel', 00);
                localStorage.setItem('totalSeconds', 0);
                var track = setInterval(() => livecall(track), 1000);
                localStorage.setItem('wrapup','Y');

                
            } else if ((res.message).match("Token has")) {
                clearInterval(track);
                window.location.reload();
            }else if ((res.message).match("Missing required request parameters")) {
                clearInterval(track);
                $ajax({
                    url:'/tata_api/login.php',
                    type:'post',
                    success:function(res){
                        window.location.reload();
                    }
                });
            } else if ((res.message).match("Originate failed")) {
                $("#call").html("You didn't pick up the call");
                clearInterval(track);
                setTimeout(function() {
                    $(".modal").modal('hide');
                }, 1000);
            } else if ((res.message).match("Endpoint request timed out")) {
                $("#call").html("You didn't pick up the call");
                clearInterval(track);
                setTimeout(function() {
                    $(".modal").modal('hide');
                }, 1000);
            } else if (res.message) {
                clearInterval(track);
                $("#call").html(res.message);

            }
        }
    });
    
    function livecall(track) {
        
        $.ajax({
            url: '/tata_api/live_call.php',
            type: 'POST',
            success: function(res1) {
                var res = $.parseJSON(res1);
                
                if (res.length != 0 && res[0]['state'].match('Answered')) {

                    $("#call").html("Call Answered...");
                    var secondsLabel = localStorage.getItem('secondsLabel');
                    var minutesLabel = localStorage.getItem('minutesLabel');
                    var totalSeconds = localStorage.getItem('totalSeconds');

                    timeIn = setInterval(function(){setTime();}, 1000);

                    function setTime() {
                        ++totalSeconds;
                        localStorage.setItem('totalSeconds', totalSeconds);

                        var x = pad(totalSeconds % 60);
                        $('#seconds').text(x);
                        localStorage.setItem('secondsLabel', x);

                        var y = pad(parseInt(totalSeconds / 60));
                        $('#minutes').text(y);
                        localStorage.setItem('minutesLabel', y);

                    }

                    function pad(val) {
                        var valString = val + "";
                        if (valString.length < 2) {
                            return "0" + valString;
                        } else {
                            return valString;
                        }
                    }
                    
                } else if (res.length != 0 && res[0]['state'].match('Ringing')) {

                    $("#call").html("Call Ringing...");
                } else {
                    
                    $("#call").html("Call Hangup...");
                    setTimeout(function() {
                        $("#confirmleadcallmodal").modal('hide');
                        
                    }, 1000);
                    
                    if(localStorage.getItem('wrapup') === 'Y'){
                        addFollowUp_ajax(lead_id);
                        localStorage.setItem('wrapup','N');
                    }
                    // window.location.reload();
                    clearInterval(track);
                }
            }

        })
    }
    localStorage.setItem('minutesLabel', 00);
    localStorage.setItem('secondsLabel', 00);
    localStorage.setItem('totalSeconds', 0);
    
</script>
<div class="row">
    <div class="col-md-12 text-center">

        <center>
            <h4>Calling <?php echo $lead['Name']; ?></h4>
            <br><br>
            <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
            <lottie-player src="https://assets2.lottiefiles.com/private_files/lf30_daep5h.json" background="transparent" speed="1" style="width: 250px; height: 250px;" loop autoplay></lottie-player>
            <br><br>
            <style>
                .ml12 {
                    font-weight: 400;
                    font-size: 1.2em;
                    text-transform: uppercase;
                    letter-spacing: 0.5em;
                }

                .ml12 .letter {
                    display: inline-block;
                    line-height: 1em;
                }
            </style>
            <h6 style="font-size: 12px;" class="ml12" id="call">Please accept call...</h6>
            <h6 style="font-size: 14px;" class="" id="call"><span id="minutes">00</span>:<span id="seconds">00</span></h6>




            <script>
                // Wrap every letter in a span
                var textWrapper = document.querySelector('.ml12');
                textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

                anime.timeline({
                        loop: true
                    })
                    .add({
                        targets: '.ml12 .letter',
                        translateX: [40, 0],
                        translateZ: 0,
                        opacity: [0, 1],
                        easing: "easeOutExpo",
                        duration: 1200,
                        delay: (el, i) => 500 + 30 * i
                    }).add({
                        targets: '.ml12 .letter',
                        translateX: [0, -30],
                        opacity: [1, 0],
                        easing: "easeInExpo",
                        duration: 1100,
                        delay: (el, i) => 100 + 30 * i
                    });
            </script>
            <style>
                #buttonfooter {
                    position: fixed;
                    bottom: 0;
                    width: 100%;
                }
            </style>
            <div class="col-lg-12" style="padding-top: 70px;">
                <div class="row">
                    <div class="col"><h6>Email:</h6> <span><?php echo $lead['Email']; ?></span></div>
                    <div class="col"><h6>Mobile:</h6> <span><?php echo $lead['Mobile']; ?></span></div>
                </div>
                <div class="row">
                    <div class="col"><h6>Alternate Mobile:</h6> <span><?php echo $lead['Alt_Mobile']; ?></span></div>
                    <div class="col"><h6>Course:</h6> <span><?php echo $lead['Course_Name']; ?></span></div>
                </div>
                <div class="row">
                    <div class="col"><h6>Specialization: </h6><span><?php echo $lead['Specialization_Name']; ?></span></div>
                    <div class="col"><h6><?php if($lead['Institute_ID']==64){echo 'SVU ID:';}else if($lead['Institute_ID']==51){echo 'IDOL ID:';}?></h6><span><?php echo $lead['urnno']; ?></span></div>
                </div>    
                <div class="row">
                    <div class="col"><h6>Stage:</h6> <span><?php print_r($lead['Stage_Name'])?></span></div>
                    <div class="col"><h6>Lead Remarks:</h6> <span><?php print_r($remark['Remark'])  ?></span></div>
                </div>
                
                
                <!-- <center><button class="btn btn-light" id="end_call_btn" style="height: 80px; width:80px; border-radius:100px;" data-dismiss="modal" onclick="end_call();"><img src="/assets/phone-call-end.svg" width="40px;"></button></center> -->
            </div>
        </center>
    </div>
</div>

<script>
    function end_call() {
        $("#end_call_btn").prop('disabled', true);
        $.ajax({
            url: '/tata_api/live_call.php',
            type: 'POST',
            success: function(data) {
                var res = $.parseJSON(data);

                // console.log(res[0].call_id);
                hangup(res[0].call_id);

            }
        })
    }
</script>
<script>
    function hangup(call_id) {
        $.ajax({
            url: '/tata_api/hangupcall.php',
            type: 'POST',
            data: {
                call_id: call_id
            },
            success: function(res2) {
                var data = $.parseJSON(res2);
                // console.log(data);
                if (data.message === "Call hangup successful") {
                    $("#call").html("Call hangup...");
                    $(".modal").modal('hide');

                }
            }

        })
    }
</script>
<?php 
if(session_status() === PHP_SESSION_NONE) session_start();
require '../filestobeincluded/db_config.php';
?>
<form method="POST">
<div class="form-group row">
    <div class="col-lg-12">
        <select data- plugin="customselect" class="form-control" id="email_template">
            <option disabled selected>Choose Template</option>
            <?php
                $result_email_template = $conn->query("SELECT * FROM Email_Templates WHERE Institute_ID = '".$_SESSION['INSTITUTE_ID']."'");
                while($email_temp = $result_email_template->fetch_assoc()) {
            ?>
                <option value="<?php echo $email_temp['id']; ?>"><?php echo $email_temp['template_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-lg-12">
        <div id="mailshow" style="border: 1px solid #e6e6e6; border-radius:5px;">
            <div class="controls">
                <textarea class="summernote input-block-level" id="mail_textarea" required>
                        
                </textarea>
            </div>
        </div>
    </div>
</div>
    

<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
    <button type="button" onclick="sendMailAll();" class="btn btn-primary">&nbsp;&nbsp;Send&nbsp;&nbsp;</button>
</div>
</form>

<script>
function sendMailAll() {
    
    var mail_id = $('#email_templates').val();
    $.ajax
        ({
            type: "POST",
            url: "Mailer/ajax_send_mail.php",
            data: { "mail_id" :mail_id },
            success: function (data) {
            console.log(data);
            if(data.match("true")) {
                $('.modal').modal('hide');
                toastr.success('Mails send successfully');
                
            }
            else {
                toastr.error('Unable to send Mails');
            }
            }
        });
    
}
</script>

<script>
    $(document).ready(function() {
        $('#email_template').change(function() {

            $('#email_textarea').val("Select Template");
            var template_id = $('#email_template').val();

            $.ajax
            ({
                type: "POST",
                url: "ajax_leads/ajax_email.php",
                data: { "email_template_id": template_id },
                success: function(data) {
                    if(data != "") {
                        $('#mail_textarea').summernote('code', data);
                    }
                    else {
                        $('#mail_textarea').summernote('pasteHTML', "<b>Select Template</b>");
                    }
                }
            });
        });
    });
</script>
<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 330,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
    });
</script>
<?php
exit;
?>
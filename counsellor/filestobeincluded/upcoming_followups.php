<?php require "db_config.php" ?>

<?php

$empID = $_POST['counsellor_id'];

date_default_timezone_set('Asia/Kolkata');
$current_timestamp = date('Y-m-d H:i:s');
$today_date = date('Y-m-d');
$today_date = $today_date." 00:00:00";

$coming_follow_ups_query = $conn->query("SELECT * FROM Follow_Ups WHERE Counsellor_ID = '".$empID."' AND Followup_Timestamp > '".$today_date."' AND Status = 'NOT_NOTIFIED' ORDER BY Followup_Timestamp");

$to_notify = array();
$followup_ids = array();

while($followup = $coming_follow_ups_query->fetch_assoc()) {
	$followup_timestamp = $followup['Followup_Timestamp'];
	$followp_lead_id = $followup['Lead_ID'];

	$get_lead_stage_query = $conn->query("SELECT * FROM Leads WHERE ID = '".$followp_lead_id."'");
	$lead_stage_dets = mysqli_fetch_assoc($get_lead_stage_query);

	if($lead_stage_dets != null) {
		if(strcasecmp($lead_stage_dets['Stage_ID'], '3')!=0) {
			$minutes_left = (strtotime($followup_timestamp) - strtotime($current_timestamp))/60;
			if($minutes_left > 0 && $minutes_left <= 10) {
				$to_notify[] = $followup['Lead_ID'];
				$followup_ids[] = $followup['ID'];
			}
		}
	}
}

$usernames = "";

foreach ($to_notify as $notify) {
	$get_lead_dets_query = $conn->query("SELECT * FROM Leads WHERE ID = '".$notify."' AND Counsellor_ID = '".$empID."'");
	$ld_res = mysqli_fetch_assoc($get_lead_dets_query);

	$usernames = $usernames.ucwords($ld_res['Name']).", ";
}

foreach ($followup_ids as $id) {
	$update_follow_up = $conn->query("UPDATE Follow_Ups SET Status = 'NOTIFIED' WHERE ID = '".$id."' AND Counsellor_ID = '".$empID."'");
}

$usernames = substr($usernames, 0, -2);

echo $usernames;

?>
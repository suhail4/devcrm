$(document).ready(function(){
    var buttonCommon = {
        init: function (dt, node, config) {
          var table = dt.table().context[0].nTable;
          if (table) config.title = $(table).data('export-title')
        },
        title: 'default title'
      };
    
    $("#basic-datatable").DataTable({language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}});
    
    var a=$("#datatable-buttons").DataTable({lengthChange:!1,dom: 'Brfrtip',buttons:[$.extend( true, {}, buttonCommon, {
    extend: 'csv'
} ),
$.extend( true, {}, buttonCommon, {
    extend: 'copy',
    orientation: 'landscape',
    exportOptions: {
      columns: 'th:not(:last-child)'
    }
} ),
$.extend( true, {}, buttonCommon, {
    extend: 'pdf',
    exportOptions: {
      columns: 'th:not(:last-child)'
    },
    orientation: 'landscape'
} )],initComplete: function() {
    var $buttons = $('.dt-buttons').hide();
    $('#exportLink').on('change', function() {
      var btnClass = $(this).find(":selected")[0].id 
        ? '.buttons-' + $(this).find(":selected")[0].id 
        : null;
      if (btnClass) $buttons.find(btnClass).click(); 
    })
  },
  "bSortable":false,
	"orderable": false,
	"ordering": false,
  'columnDefs': [{
    'targets': 0,
    'width': '1%',
    'bsort': false,
    "orderable": false,
 }],"pageLength": 50,language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}});$("#selection-datatable").DataTable({select:{style:"multi"},language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}}),$("#key-datatable").DataTable({keys:!0,language:{paginate:{previous:"<i class='uil uil-angle-left'>",next:"<i class='uil uil-angle-right'>"}},drawCallback:function(){$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}}),a.buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)")});
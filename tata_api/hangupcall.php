<?php
    require "../filestobeincluded/db_config.php";
    if(session_status() === PHP_SESSION_NONE) session_start();

    $caller_id = mysqli_real_escape_string($conn,$_POST['call_id']);
    $d = json_encode(array("call_id"=>$caller_id));

    


$curl = curl_init();

curl_setopt_array($curl, [
  CURLOPT_URL => "https://api-cloudphone.tatateleservices.com/v1/call/hangup",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $d,
  CURLOPT_HTTPHEADER => [
    "Accept: application/json",
    'Authorization: '.$_SESSION['static_token'].' ',
    "Content-Type: application/json"
  ],
]);

$result = curl_exec($curl);

if (curl_errno($curl)) {
    echo 'Error:' . curl_error($curl);
}else{
    print_r($result);
}
curl_close($curl);

<?php require "filestobeincluded/db_config.php" ?>
<?php
$all_users = array();
$users_query_res = $conn->query("SELECT *, CAST(AES_DECRYPT(password, '60ZpqkOnqn0UQQ2MYTlJ') AS CHAR(50)) pass FROM users");
while ($row = $users_query_res->fetch_assoc()) {
    $all_users[] = $row;
}
?>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>

<?php

$all_institutes = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> '0'");
while ($row = $institutes_query_res->fetch_assoc()) {
    $all_institutes[] = $row;
}
$theins = "";
foreach ($all_institutes as $ins) {
    $theins = $theins . "<option selected value='" . $ins['ID'] . "'>" . $ins['Name'] . "</option>";
}

?>
<button class="btn btn-primary" onclick="eid();">Dialer Eid Update</button>
<button class="btn btn-primary" data-toggle="modal" data-target="#uploadusermodal"><i data-feather="upload" class="icon-xs"></i> Upload Data</button>
<!--------------Upload Leads Modal------------->
<div id="uploadusermodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadleads" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadleads">Upload Users</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="upload_user" method="POST" enctype="multipart/form-data">

                    <div class="form-group row">
                        <label class="col-lg-9 col-form-label" for="example-fileinput">Upload File with Prospect User Details</label>
                        <div class="col-lg-3 col-sm-12">
                            <a href="/assets/files/Blackboard User Data Sample.csv" download="Blackboard User Data Sample.csv">
                                <p><i data-feather="file" class="icon-dual icon-xs"></i> Download Sample</p>
                            </a>
                        </div><br>
                        <div class="col-lg-12">
                            <input type="file" name="lead_file" class="form-control" style="border: 0ch;" accept=".csv">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                <button type="submit" name="upload" id="upload" class="btn btn-primary">&nbsp;&nbsp;Upload&nbsp;&nbsp;</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(document).ready(function() {
        $('.modal').on('hidden.bs.modal', function() {
            $(this).find('form').trigger('reset');
        });
        $('#upload_user').on("submit", function(e) {
            e.preventDefault(); //form will not submitted  
            $.ajax({
                url: "settings_pages/ajax_user/upload_user.php",
                method: "POST",
                data: new FormData(this),
                contentType: false, // The content type used when sending data to the server.  
                cache: false, // To unable request pages to be cached  
                processData: false, // To send DOMDocument or non processed data file it is set to false  
                success: function(data) {
                    console.log(data);
                    $('.modal').modal('hide');
                    if (data.match("true")) {
                        toastr.success('User uploaded successfully');
                        $("#all_users").load(location.href + " #all_users");
                    } else {
                        toastr.error('Unable to upload user');
                    }
                }
            })
        });
    });
</script>
<!----------------End---------------->

<button class="btn btn-primary" data-toggle="modal" data-target="#addusermodal"> <i class="uil uil-user-plus"></i> Add User</button>
<button class="btn btn-primary" id="ExportReporttoExcel"> <i class="fa fa-download"></i> Download</button>
<button class="btn btn-primary" onclick="export_bulk()"><i class="fa fa-download"></i> Download Login Details</button>
<script>
    $("#ExportReporttoExcel").on("click", function() {

        $.ajax({
            type: "POST",
            url: "/ajax_leads/export_users.php",
            data: {},
            success: function(data) {
                /*
                 * Make CSV downloadable
                 */
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff' + data];

                var blobObject = new Blob(fileData, {
                    type: "text/csv;charset=utf-8;"
                });



                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = "Blackboard_Users_<?php echo date("d-m-Y h:i a"); ?>.csv";

                /*
                 * Actually download CSV
                 */
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);


            }
        });
        return false;
    });
</script>
<!----Add Source Modal-------->
<div class="modal fade" id="addusermodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myCenterModalLabel">Add New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 20px;">
                <form method="POST" action="" id="add_user_form">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="firstname">Full Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Full Name" required>
                        </div>
                        <label class="col-lg-2 col-form-label" for="mobilenumber">Mobile Number</label>
                        <div class="col-lg-4">
                            <input type="tel" class="form-control" id="mobilenumber" name="mobilenumber" placeholder="Mobile Number" maxlength="13" minlength="10" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="mobilenumber">Employee ID</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="employee_id" name="employee_id" placeholder="Employee ID" required>
                        </div>
                        <label class="col-lg-2 col-form-label" for="userpassword">Password</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="userpassword" onblur="checkPrePasswd()" placeholder="Minimum 6 Characters Required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="email">Email</label>
                        <div class="col-lg-4">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required>
                        </div>
                        <label class="col-lg-2 col-form-label" for="emailpass">Email Password</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="emailpass" id="emailpass" placeholder="Email Password" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="role">Role</label>
                        <div class="col-lg-4">
                            <select class="form-control custom-select" id="select_role" name="role" required>
                                <option selected disabled>Choose</option>
                                <option value="Administrator">Administrator</option>
                                <option value="Sr Manager">Senior Manager</option>
                                <option value="Manager">Manager</option>
                                <option value="Counsellor">Counsellor</option>
                            </select>
                        </div>
                        <label class="col-lg-2 col-form-label" for="designation">Designation</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="designation" id="designation" placeholder="Designation" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="institute">University</label>
                        <div class="col-lg-4">
                            <select class="form-control custom-select" name="institute" id="users_select_institute" required>
                                <option disabled selected>Choose</option>
                                <?
                                foreach ($all_institutes as $institute) {
                                ?>
                                    <option value="<?php echo ($institute['ID']); ?>"><?php echo $institute['Name'];  ?></option>
                                <?
                                }
                                ?>
                            </select>
                        </div>
                        <label class="col-lg-2 col-form-label" for="reportingto">Reporting to User</label>
                        <div class="col-lg-4">
                            <select class="form-control custom-select" id="select_reportingto" name="reportingto" required>
                                <option selected value="<?php echo $au['Reporting_To_User_ID'] ?>"><?
                                                                                                    $get_reporting_to_name = $conn->query("SELECT * FROM users");
                                                                                                    $reporter_name = mysqli_fetch_assoc($get_reporting_to_name);
                                                                                                    echo $reporter_name['Name'];
                                                                                                    ?></option>
                                <?
                                foreach ($all_users as $all_u) {
                                ?>
                                    <option value="<?php echo ($all_u['ID']); ?>"><?php echo $all_u['Name'];  ?></option>
                                <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="user_extension">Dialer Extension</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="extension" id="user_extension" placeholder="Extension">
                        </div>

                        <label class="col-lg-2 col-form-label" for="dialer_pass">Dialer Password</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="dialer_pass" placeholder="Dialer Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="user_type" class="col-lg-2 col-form-label">User Type</label>
                        <div class="col-lg-4">
                            <select class="form-control custom-select" name="user_type" id="user_type">
                                <option disabled selected>Choose</option>
                                <!-- <option value="b2c">B2C</option> -->
                                <option value="b2b">B2B Partners</option>
                            </select>
                        </div>
                    </div>





                    <script>
                        function checkPrePasswd() {
                            var passwd = document.getElementById("userpassword").value;

                            if (passwd.length < 6) {
                                toastr.error("Minimum 6 Characters Required");
                                document.getElementById("up-pass").disabled = true;
                            } else {
                                document.getElementById("up-pass").disabled = false;
                            }
                        }
                    </script>

                    <button class="btn btn-primary float-right" id="up-pass" disabled type="button" onclick="addUser();">Save</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<br><br>
<div id="all_users">
    <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
    <div class="table-responsive">
        <table id="basic-datatable1" class="table table-hover table-striped">
            <thead>
                <tr>
                    <!-- <th>#</th> -->
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
                    <th>Password</th>
                    <th>Extension</th>
                    <th>Reporting to</th>
                    <th>Status</th>
                    <th>Download Permission</th>
                    <th>Upload Permission</th>
                    <th>Actions</th>
                    <th>Login Details</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $user_counter = 0;
                foreach ($all_users as $au) {
                    $user_counter++;
                    $users_employee_id = $au['ID'];
                    $full_name_users = $au['Name'];
                    $users_email = strtolower($au['Email']);
                    $users_email_pass = $au['Email_Password'];
                    $users_mobile = $au['Mobile'];
                    $users_role = $au['Role'];
                    $users_pass = $au['pass'];
                    $users_designation = $au['Designation'];
                    $time_login = $conn->query("SELECT * FROM Time_Details WHERE Type like 'login' AND User_ID='" . $users_employee_id . "' ");
                    $time_logout = $conn->query("SELECT * FROM Time_Details WHERE Type like 'logout' AND User_ID='" . $users_employee_id . "' ");
                    $row = array();
                    $i = 0;
                    while ($r = mysqli_fetch_assoc($time_login)) {
                        $row[$i][0] = $r['Time'];
                        $i++;
                    }
                    $i = 0;
                    while ($r = mysqli_fetch_assoc($time_logout)) {
                        $row[$i][1] = $r['Time'];
                        $i++;
                    }



                    if (is_null($au['Extension']) || strcasecmp($au['Extension'], ' ') == 0 || strcasecmp($au['Extension'], '') == 0) {
                        $users_extension = 'NA';
                    } else {
                        $users_extension = $au['Extension'];
                    }
                    $users_id = $au['ID'];
                    $users_status = $au['Status'];
                    if (strcasecmp($users_status, 'Y') == 0) {
                        $status_user = 'checked';
                    } else {
                        $status_user = 'not';
                    }

                    $get_reporting_user_dets = $conn->query("SELECT * FROM users WHERE ID = '" . $au['Reporting_To_User_ID'] . "'");
                    $reporting_user_dets = mysqli_fetch_assoc($get_reporting_user_dets);

                ?>
                    <tr>
                        <td><?php echo $full_name_users; ?></td>
                        <td><?php echo $users_email; ?></td>
                        <td><?php echo $users_mobile; ?></td>
                        <td><?php echo $users_role; ?></td>
                        <td><?php echo $users_pass; ?></td>
                        <td><?php echo $users_extension; ?></td>
                        <td><?php echo $reporting_user_dets['Name']; ?></td>
                        <td>
                            <div class="custom-control custom-switch mb-2">
                                <input type="checkbox" class="custom-control-input" <?php echo $status_user ?> id="customSwitch1<?php echo $users_id ?>" onclick="updateUserStatus('<?php echo $users_id ?>');" value="<?php echo $status_user ?>">
                                <label class="custom-control-label" for="customSwitch1<?php echo $users_id ?>"></label>
                            </div>
                        </td>
                        <td>
                            <?php if ($au['Role'] == 'Manager') { ?>
                                <div class="custom-control custom-switch mb-2">
                                    <input type="checkbox" class="custom-control-input" <?php if ($au['Download_Status'] == 'Y') {
                                                                                            echo 'checked';
                                                                                        } ?> id="customDSwitch<?php echo $users_id ?>" onclick="DownloadUserStatus('<?php echo $users_id ?>');">
                                    <label class="custom-control-label" for="customDSwitch<?php echo $users_id ?>"></label>
                                </div>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if ($au['Role'] == 'Manager') { ?>
                                <div class="custom-control custom-switch mb-2">
                                    <input type="checkbox" class="custom-control-input" <?php if ($au['Upload_Status'] == 'Y') {
                                                                                            echo 'checked';
                                                                                        } ?> id="customUSwitch<?php echo $users_id ?>" onclick="UploadUserStatus('<?php echo $users_id ?>');">
                                    <label class="custom-control-label" for="customUSwitch<?php echo $users_id ?>"></label>
                                </div>
                            <?php } ?>
                        </td>

                        <td>
                            <i class="fa fa-edit" data-toggle="modal" data-target="#editusermodal<?php echo $users_id ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                            <!----Edit Source Modal-------->
                            <div class="modal fade" id="editusermodal<?php echo $users_id ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myCenterModalLabel">Edit User Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="">
                                                <input id="new_userid<?php echo $users_id ?>" value="<?php echo $users_id ?>" type="hidden">
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="firstname<?php echo $users_id ?>">Full Name</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="firstname<?php echo $users_id ?>" value="<?php echo $full_name_users ?>" placeholder="Full Name" required>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label" for="mobilenumber<?php echo $users_id ?>">Mobile Number</label>
                                                    <div class="col-lg-4">
                                                        <input type="tel" class="form-control" id="mobilenumber<?php echo $users_id ?>" value="<?php echo $users_mobile ?>" placeholder="+91-XXXXXXXXXX" maxlength="10" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="employee_id<?php echo $users_id ?>">Employee ID</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="employee_id<?php echo $users_id ?>" value="<?php echo $users_employee_id ?>" placeholder="Employee ID" required>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label" for="userpassword<?php echo $users_id ?>">Password</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" value="<?php echo $users_pass ?>" class="form-control" id="userpassword<?php echo $users_id ?>" placeholder="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="email<?php echo $users_id ?>">Email</label>
                                                    <div class="col-lg-4">
                                                        <input type="email" class="form-control" value="<?php echo $users_email ?>" id="email<?php echo $users_id ?>" placeholder="counsellor@blacboard.com" required>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label" for="emailpass<?php echo $users_id ?>">Email Password</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php echo $users_email_pass ?>" id="emailpass<?php echo $users_id ?>" placeholder="Email Password" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="new_usersrole<?php echo $users_id ?>">Role</label>
                                                    <div class="col-lg-4">
                                                        <select class="form-control custom-select" id="new_usersrole<?php echo $users_id ?>" required>
                                                            <option selected value="<?php echo $au['Role']; ?>"><?php echo $au['Role']; ?></option>
                                                            <option value="Manager">Manager</option>
                                                            <option value="Counsellor">Counsellor</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label" for="designation<?php echo $users_id ?>">Designation</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="designation<?php echo $users_id ?>" value="<?php echo $users_designation ?>" placeholder="Designation" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="new_users_selectinstitute<?php echo $users_id ?>">University</label>
                                                    <div class="col-lg-4">
                                                        <select class="form-control custom-select" id="new_users_selectinstitute<?php echo $users_id ?>" required>
                                                            <option selected value="<?php echo $au['Institute_ID'] ?>"><?
                                                                                                                        $get_institute_name = $conn->query("SELECT * FROM Institutes WHERE ID = '" . $au['Institute_ID'] . "'");
                                                                                                                        $institute_name = mysqli_fetch_assoc($get_institute_name);
                                                                                                                        echo $institute_name['Name'];
                                                                                                                        ?></option>
                                                            <?
                                                            foreach ($all_institutes as $institute) {

                                                            ?>
                                                                <option value="<?php echo ($institute['ID']); ?>"><?php echo $institute['Name'];  ?></option>
                                                            <?
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label" for="new_users_select_reportingto<?php echo $users_id ?>">Reporting to User</label>
                                                    <div class="col-lg-4">
                                                        <select class="form-control custom-select" id="new_users_select_reportingto<?php echo $users_id ?>" required>
                                                            <option selected value="<?php echo $au['Reporting_To_User_ID'] ?>"><?
                                                                                                                                $get_reporting_to_name = $conn->query("SELECT * FROM users WHERE ID = '" . $au['Reporting_To_User_ID'] . "'");
                                                                                                                                $reporter_name = mysqli_fetch_assoc($get_reporting_to_name);
                                                                                                                                echo $reporter_name['Name'];
                                                                                                                                ?></option>
                                                            <?
                                                            foreach ($all_users as $all_u) {
                                                            ?>
                                                                <option value="<?php echo ($all_u['ID']); ?>"><?php echo $all_u['Name'];  ?></option>
                                                            <?
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="new_user_extension<?php echo $users_id ?>">Dialer Extension</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="new_user_extension<?php echo $users_id ?>" value="<?php echo $users_extension ?>" placeholder="Extension">
                                                    </div>

                                                    <label class="col-lg-2 col-form-label" for="dialer_pass<?php echo $users_id ?>">Dialer Password</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="dialer_pass<?php echo $users_id ?>" value="<?php echo $au['Dialer_Password']; ?>" placeholder="Dialer Password">
                                                    </div>
                                                </div>

                                                <button class="btn btn-primary float-right" type="button" onclick="updateuser('<?php echo $users_id ?>')">Update</button>
                                            </form>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deleteusermodal<?php echo $users_id ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                            <!----delete Source Modal-------->
                            <div class="modal fade" id="deleteusermodal<?php echo $users_id ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST">
                                                <input type="hidden" id="delete_user_id<?php echo $users_id; ?>" value="<?php echo ($au['ID']); ?>">
                                                <center><button class="btn btn-danger textS-center" type="button" onclick="deleteUser('<?php echo $au['ID']; ?>')">&nbsp;&nbsp;Yes&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                            </form>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </td>
                        <td>
                            <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#timedetailsmodal<?php echo $users_id ?>">Details</button>
                            <div class="modal fade" id="timedetailsmodal<?php echo $users_id ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myCenterModalLabel">Login and Logout Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col">
                                                    <h5>Login Time</h5>
                                                </div>
                                                <div class="col">
                                                    <h5>Logout Time</h5>
                                                </div>
                                            </div>
                                            <?php foreach ($row as $r) { ?>
                                                <div class="row">
                                                    <div class="col"><?php print_r($r[0]) ?></div>
                                                    <div class="col"><?php print_r($r[1]) ?></div>
                                                </div>
                                            <? } ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info btn-sm" onclick="export_time('<?php echo $au['ID']; ?>');">Export</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    function export_time(id) {
        $.ajax({
            type: "post",
            url: "settings_pages/ajax_user/export_time.php",
            data: {
                id: id
            },
            success: function(data) {
                /*
                 * Make CSV downloadable
                 */
                //console.log(data);
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff' + data];

                var blobObject = new Blob(fileData, {
                    type: "text/csv;charset=utf-8;",
                });

                downloadLink.href = URL.createObjectURL(blobObject);
                downloadLink.download = "Timings_<?php echo date("d-m-Y h:i a"); ?>.csv";

                /*
                 * Actually download CSV
                 */
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#basic-datatable1").DataTable({
            "pageLength": 10000,

            "aoColumns": [{
                    "bSearchable": true
                },
                {
                    "bSearchable": true
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
                {
                    "bSearchable": false
                },
            ],
            language: {
                paginate: {
                    previous: "<i class='uil uil-angle-left'>",
                    next: "<i class='uil uil-angle-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#add_user_form').validate({
            rules: {
                firstname: {
                    required: true
                },
                employee_id: {
                    required: true
                },
                email: {
                    required: true,
                    email: "Please enter valid email"
                },
                emailpass: {
                    required: true
                },
                mobilenumber: {
                    required: true,
                    minlength: 10,
                    maxlength: 13
                },
                designation: {
                    required: true
                },
                role: {
                    required: true
                },
                institute: {
                    required: true
                },
                reportingto: {
                    required: true
                }
            },
            messages: {
                firstname: {
                    required: "Name is required"
                },
                employee_id: {
                    required: "Employee ID is required"
                },
                mobilenumber: {
                    required: "Mobile number is required",
                    minlength: "Invalid mobile number",
                    maxlength: "Invalid mobile number"
                },
                emailpass: {
                    required: "Email password is required"
                },
                designation: {
                    required: "Designation is required"
                },
                role: {
                    required: "Role is required"
                },
                institute: {
                    required: "University is required"
                },
                reportingto: {
                    required: "Reporting To User is required"
                }
            },
            highlight: function(element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
                $(element).closest('.small').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
                $(element).closest('.small').removeClass('has-error');
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#select_role').change(function() {

            var selected_role = $('#select_role').val();
            var theins = "<?php echo $theins; ?>";
            if (selected_role == 'Administrator') {
                $('#users_select_institute').html("<option selected value='0'>None</option>");

                $('#select_reportingto').html("<option selected value='1'>None</option>");
            } else if (selected_role == 'Manager') {
                $('#select_reportingto').html("<option selected value='1'>Administrator</option>");

                $('#users_select_institute').prop('disabled', false);
                $("#users_select_institute").html(theins);
            } else if (selected_role == 'Sr Manager') {
                $('#select_reportingto').html("<option selected value='1'>Administrator</option>");

                $('#users_select_institute').prop('disabled', false);
                $("#users_select_institute").html(theins);
            } else {
                $('#users_select_institute').prop('disabled', false);
                $('#select_reportingto').prop('disabled', false);

                $("#users_select_institute").html(theins);
                jQuery('#users_select_institute').trigger('change');
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#users_select_institute').change(function() {

            var selected_role = $('#select_role').val();
            if (selected_role == 'Counsellor') {
                $('#select_reportingto').html("<option disabled selected>Choose</option>");

                $.ajax({
                    type: "POST",
                    url: "onselect/onAssignmentSelect.php",
                    data: {
                        "users_select_institute": ""
                    },
                    success: function(data) {
                        if (data != "") {
                            $('#select_reportingto').html(data);
                        } else {
                            $('#select_reportingto').html("<option disabled selected>Choose</option>");
                        }
                    }
                });
            }
        });
    });
</script>

<script>
    function updateUserStatus(id) {

        var status_user = $('#customSwitch1'.concat(id)).val();

        $.ajax({
            type: "POST",
            url: "settings_pages/ajax_user/update_user_status.php",
            data: {
                "user_id": id,
                "status_user": status_user
            },
            success: function(data) {
                if (data.match("true")) {
                    console.log(status_user);
                    if (status_user.match("checked")) {
                        toastr.success('User deactivated successfully');
                        $("#all_users").load(location.href + " #all_users");
                    } else {
                        toastr.success('User activated successfully');
                        $("#all_users").load(location.href + " #all_users");
                    }
                } else {
                    toastr.error('Unable to update user status');
                }
            }
        });
        return false;
    }
</script>

<script>
    function addUser() {

        if ($('#add_user_form').valid()) {

            var user_first_name = $('#firstname').val();
            var user_employee_id = $('#employee_id').val();
            var user_email = $('#email').val();
            var user_email_pass = $('#emailpass').val();
            var user_mobile = $('#mobilenumber').val();
            var user_password = $('#userpassword').val();
            var user_designation = $('#designation').val();
            var user_institute = $('#users_select_institute').val();
            var user_role = $('#select_role').val();
            var user_reporting_to = $('#select_reportingto').val();
            var user_extension = $('#user_extension').val();
            var dialer_pass = $('#dialer_pass').val();
            var user_type = $('#user_type').val();


            $.ajax({
                type: "POST",
                url: "settings_pages/ajax_user/add_user.php",
                data: {
                    "user_first_name": user_first_name,
                    "user_employee_id": user_employee_id,
                    "user_email": user_email,
                    "user_email_pass": user_email_pass,
                    "user_mobile": user_mobile,
                    "user_password": user_password,
                    "user_designation": user_designation,
                    "user_institute": user_institute,
                    "user_role": user_role,
                    "user_reporting_to": user_reporting_to,
                    "user_extension": user_extension,
                    "dialer_pass": dialer_pass,
                    "user_type": user_type
                },
                success: function(data) {
                    console.log(data);
                    $('#addusermodal').modal('hide');

                    if (data.match("true")) {
                        console.log(data);
                        $("#all_users").load(location.href + " #all_users");
                        toastr.success('User added successfully');
                    } else if (data.match("exists")) {
                        toastr.warning('Email already exists');
                    } else {
                        toastr.error('Unable to add user');
                    }
                }
            });
            return false;
        }
    }
</script>

<script>
    function updateuser(id) {
        var new_userid = $('#new_userid'.concat(id)).val();
        var new_user_first_name = $('#firstname'.concat(id)).val();
        var new_user_employee_id = $('#employee_id'.concat(id)).val();
        var new_user_email = $('#email'.concat(id)).val();
        var new_user_email_pass = $('#emailpass'.concat(id)).val();
        var new_user_mobile = $('#mobilenumber'.concat(id)).val();
        var new_user_password = $('#userpassword'.concat(id)).val();
        var new_user_designation = $('#designation'.concat(id)).val();
        var new_user_institute = $('#new_users_selectinstitute'.concat(id)).val();
        var new_user_role = $('#new_usersrole'.concat(id)).val();
        var new_user_reporting_to = $('#new_users_select_reportingto'.concat(id)).val();
        var new_user_extension = $('#new_user_extension'.concat(id)).val();
        var dialer_pass = $('#dialer_pass'.concat(id)).val();


        $.ajax({
            type: "POST",
            url: "settings_pages/ajax_user/update_user.php",
            data: {
                "new_userid": new_userid,
                "new_user_first_name": new_user_first_name,
                "new_user_employee_id": new_user_employee_id,
                "new_user_email": new_user_email,
                "new_user_email_pass": new_user_email_pass,
                "new_user_mobile": new_user_mobile,
                "new_user_password": new_user_password,
                "new_user_designation": new_user_designation,
                "new_user_institute": new_user_institute,
                "new_user_role": new_user_role,
                "new_user_reporting_to": new_user_reporting_to,
                "new_user_extension": new_user_extension,
                "dialer_pass": dialer_pass
            },
            success: function(data) {
                console.log(data);
                $('.modal').modal('hide');

                if (data.match("true")) {
                    $("#all_users").load(location.href + " #all_users");
                    toastr.success('User details updated successfully');

                } else {
                    toastr.error('Unable to update user details');
                }
            }
        });
        return false;
    }
</script>

<script>
    function deleteUser(id) {
        var user_id = $('#delete_user_id'.concat(id)).val();

        $.ajax({
            type: "POST",
            url: "settings_pages/ajax_user/delete_user.php",
            data: {
                "user_id": user_id
            },
            success: function(data) {
                $('.modal').modal('hide');
                if (data.match("true")) {
                    toastr.success('User deleted successfully');
                    $("#all_users").load(location.href + " #all_users");
                } else {
                    toastr.error('Unable to delete user');
                }
            }
        });
        return false;
    }
</script>
<script>
    function eid() {
        $.ajax({
            url: 'tata_api/all_agent.php',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(index, element) {
                    var eid = element.eid;
                    var call_num = element.allowed_caller_ids[0];
                    var call_id = call_num['number'];
                    console.log(eid);
                    // var call_id = JSON.parse(call_num);\
                    update_eid(call_id, eid);
                });
            }
        })
    }

    function update_eid(id, eid) {
        $.ajax({
            url: 'settings_pages/ajax_user/update_eid.php',
            data: {
                id: id,
                eid: eid
            },
            type: 'POST',
            success: function(data) {
                if (data.match('true')) {
                    toastr.success('Dialer EID updated successfully!');
                } else {
                    toastr.error('Unable to update EID!');
                }
            }
        })
    }
</script>
<script>
    function DownloadUserStatus(id) {
        $.ajax({
            url: "settings_pages/ajax_user/download_user_status.php",
            data: {
                id: id
            },
            type: "POST",
            success: function(res) {
                if (res.match('Y')) {
                    toastr.success('Download Permission Allowed');
                } else {
                    toastr.error('Download Permission Denied');
                }
            }
        })
    }
</script>
<script>
    function UploadUserStatus(id) {
        $.ajax({
            url: "settings_pages/ajax_user/upload_user_status.php",
            data: {
                id: id
            },
            type: "POST",
            success: function(res) {
                if (res.match('Y')) {
                    toastr.success('Upload Permission Allowed');
                } else {
                    toastr.error('Upload Permission Denied');
                }
            }
        })
    }
</script>
<script>
    function export_bulk(){
        $.ajax({
            type: "post",
            url: "settings_pages/ajax_user/export_bulk_time.php",
            success: function(data) {
                /*
                 * Make CSV downloadable
                 */
                //console.log(data);
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff' + data];

                var blobObject = new Blob(fileData, {
                    type: "text/csv;charset=utf-8;",
                });

                downloadLink.href = URL.createObjectURL(blobObject);
                downloadLink.download = "Users_Timings_<?php echo date("d-m-Y h:i a"); ?>.csv";

                /*
                 * Actually download CSV
                 */
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })
    }
</script>
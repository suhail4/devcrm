<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_stages = array();

$stages_query_res = $conn->query("SELECT * FROM Stages");
while($row = $stages_query_res->fetch_assoc()) {
    $all_stages[] = $row;
}

?>

<div class="card mb-0 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        <div class="card-header" id="headingThree"><h5 class="m-0 font-size-16">Stages <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
        <div class="card-body text-muted">

<button class="btn btn-primary" data-toggle="modal" data-target="#uploadstagemodal"><i data-feather="upload" class="icon-xs"></i> Upload Stage Data</button>
<!--------------Upload Leads Modal------------->
<div id="uploadstagemodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadleads" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadleads">Upload Stages</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="upload_stage" method="POST" enctype="multipart/form-data">
                
                <div class="form-group row">
                    <label class="col-lg-9 col-form-label"
                        for="example-fileinput">Upload File with Prospect Stage Name</label>
                        <div class="col-lg-3 col-sm-12">
                        <a href="/assets/files/Blackboard Stage Data Sample.csv" download="Blackboard Stage Data Sample.csv"><p><i data-feather="file" class="icon-dual icon-xs"></i> Download Sample</p></a>
                    </div><br>
                    <div class="col-lg-12">
                        <input type="file" name="lead_file" class="form-control" style="border: 0ch;"
                            accept=".csv">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                <button type="submit" name="upload" id="upload" class="btn btn-primary">&nbsp;&nbsp;Upload&nbsp;&nbsp;</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>  
      $(document).ready(function(){  
           $('#upload_stage').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "settings_pages/ajax_stage/upload_stage.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
                     	console.log(data);
						$('.modal').modal('hide');
						if(data.match("true")) {
							toastr.success('Stage uploaded successfully');
							$("#accordionExample").load(location.href + " #accordionExample" , function () {
                                $("#headingThree").click();
                            });
						}
						else {
							toastr.error('Unable to upload stages');
						}  
                     }  
                })  
           });  
      });  
 </script>
<!----------------End---------------->

        <button class="btn btn-primary" data-toggle="modal" data-target="#addstagesmodal"> <i class="uil uil-plus-circle"></i> Add Stages</button>
        <button class="btn btn-primary" id="ExportStagetoExcel"> <i class="fa fa-download"></i> Download</button>
<script>
                                        $("#ExportStagetoExcel").on("click", function() {
                                             
                                                $.ajax
                                                ({
                                                type: "POST",
                                                url: "/ajax_leads/export_stage.php",
                                                data: {  },
                                                success: function (data) {
                                                     /*
                                                    * Make CSV downloadable
                                                    */
                                                    var downloadLink = document.createElement("a");
                                                    var fileData = ['\ufeff'+data];

                                                    var blobObject = new Blob(fileData,{
                                                        type: "text/csv;charset=utf-8;"
                                                    });

                                                    

                                                    var url = URL.createObjectURL(blobObject);
                                                    downloadLink.href = url;
                                                    downloadLink.download = "Blackboard_Stage_<?php echo date("d-m-Y h:i a"); ?>.csv";

                                                    /*
                                                    * Actually download CSV
                                                    */
                                                    document.body.appendChild(downloadLink);
                                                    downloadLink.click();
                                                    document.body.removeChild(downloadLink);
                                                                                                    

                                                }
                                                });
                                                return false;
                                        });</script>
            <!----Add Sub-Source Modal-------->
            <div class="modal fade" id="addstagesmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Stage</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">Stage</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="stage_name" placeholder="Stage">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"
                                        for="b2b2b2c">B2B/B2C</label>
                                    <div class="col-lg-9">
                                        <select class="form-control custom-select" name="b2bb2c" id="b2bb2c">
                                        <option selected disabled>Choose</option>
                                        <option value="b2b">B2B</option>
                                        <option value="b2c">B2C</option>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="saveStage()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_stages" class="table-responsive">
                <br>
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0 basic-datatable">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Stages</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_stages as $stage) {
                            $counter++;
                            if(strcasecmp($stage['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $stage['Name']; ?></td>
                                <td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editstagemodal<?php echo $counter; ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editstagemodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Stage</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="stage_id<?php echo $counter; ?>" value="<?php echo($stage['ID']); ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">Stage</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_stage_name<?php echo $counter; ?>" placeholder="Stage Name" value="<?php echo $stage['Name']; ?>">
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateStage(<?php echo $counter; ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletestagemodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletestagemodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_stage_id<?php echo $counter; ?>" value="<?php echo($stage['ID']); ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteStage(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	function saveStage() {
		var stage_name = $('#stage_name').val();
        var type = $('#b2bb2c').val();
      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_stage/add_stage.php",
          data: { "stage_name": stage_name , "type" : type },
          success: function (data) {
            $('#addstagesmodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Stage added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingThree").click();
                });
            }
            else {
                toastr.error('Unable to add stage');
            }
          }
        });
        return false;
	}
</script>

<script>
    function updateStage(id) {
        var stage_id = $('#stage_id'.concat(id)).val();
        var new_stage_name = $('#new_stage_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_stage/update_stage.php",
          data: { "stage_id": stage_id, "new_stage_name": new_stage_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Stage updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingThree").click();
                });
            }
            else {
                toastr.error('Unable to update stage');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteStage(id) {
        var stage_id = $('#delete_stage_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_stage/delete_stage.php",
          data: { "stage_id": stage_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Stage deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingThree").click();
                });
            }
            else {
                toastr.error('Unable to delete stage');
            }
          }
        });
        return false;
    }
</script>
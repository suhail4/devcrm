<div id="emailtable">
<button class="btn btn-primary add-email float-right">Add Template</button>
<script type='text/javascript'>
	$(document).ready(function(){

		$('.add-email').click(function(){
			
			// AJAX request
			$.ajax({
				url: 'ajax_leads/add_email_modal.php',
				success: function(response){ 
					// Add response in Modal body
					$('#add_email_modal').html(response); 

					// Display Modal
					$('#addemailtempmodal').modal('show'); 
				}
			});
		});
	});
</script>
    <!----Add Email Template Modal-------->
    <div class="modal fade" id="addemailtempmodal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myCenterModalLabel">Add New Email Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="add_email_modal">
                    
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <br><br>
    <div class="table-responsive" style="padding-top: 20px;">
    <?php
    $all_emails = array();
    $email_query_res = $conn->query("SELECT * FROM Email_Templates");
    while ($row = $email_query_res->fetch_assoc()) {
        $all_emails[] = $row;
    }

    $all_institutes = array();
    $institute_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> 0");
    while ($row = $institute_query_res->fetch_assoc()) {
        $all_institutes[] = $row;
    }
    ?>
    <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
    <table class="table table-hover mb-0 basic-datatable">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Template Name</th>
            <th scope="col">Subject</th>
            <th scope="col">University</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $email_counter = 0;
                foreach($all_emails as $em){
                    $email_counter++;
                    $ID = $em['id'];
                    $emails_temp_name = $em['template_name'];
                    $emails_temp_subject = $em['subject'];
                    $emails_temp_content = $em['email_template'];
                
            ?>
            
            <tr>
            <th scope="row"><?php echo $email_counter ?></th>
            <td><?php echo $em['template_name'];?></td>
            <td><?php echo $em['subject']; ?></td>
            <td><?php
            $univ_name_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$em['Institute_ID']."'");
            $the_name = mysqli_fetch_assoc($univ_name_query);
            $unive_name = $the_name['Name'];
            echo $unive_name;
             ?></td>
            <td>
                <i class="fa fa-edit" onclick="editmail(<?php echo $ID ?>);" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;

                
                <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deleteemailtempmodal<?php echo $ID ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                <!----delete Source Modal-------->
                <div class="modal fade" id="deleteemailtempmodal<?php echo $ID ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form method="post">
                                <input type="hidden" id="email_temp_id<?php echo $ID; ?>" value="<?php echo $ID ?>">
                                <center><button class="btn btn-danger textS-center" type="button" onclick="deleteemailtemp(<?php echo $ID; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                            </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                </td>
            </tr>
                <?php } ?>
            
        </tbody>
    </table>
</div>


<script type='text/javascript'>
	function editmail(id){
        var emailid = id;
        // AJAX request
        $.ajax({
            url: 'ajax_leads/edit_email_modal.php',
            type: 'post',
            data: {emailid: emailid},
            success: function(response){ 
                // Add response in Modal body
                $('#edit_email_modal').html(response); 

                // Display Modal
                $('#editemailtempmodal').modal('show'); 
            }
        });
    }
	
</script>

<!----Edit Source Modal-------->
<div class="modal fade" id="editemailtempmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myCenterModalLabel">Edit Email Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="edit_email_modal">
                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</div>


<script>
    function updateemailtemp(id) {
        var temp_email_id = $('#temp_email_id'.concat(id)).val();
        var email_temp_name_id = $('#temp_name_email'.concat(id)).val();
        var email_temp_subject_id = $('#temp_subject_email'.concat(id)).val();
        var email_temp_institute_id = $('#email_institute_id'.concat(id)).val();
        var email_temp_content_id = $('#email_contents_update'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_email/update_email.php",
          data: { "temp_email_id": temp_email_id, "email_temp_name_id": email_temp_name_id, "email_temp_subject_id": email_temp_subject_id, "email_temp_content_id":  email_temp_content_id, "email_temp_institute_id": email_temp_institute_id},
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Email Template updated successfully');
                $("#emailtable").load(location.href + " #emailtable");
                                
            }
            else {
                toastr.error('Unable to update email template');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteemailtemp(id) {
        var email_temp_id = $('#email_temp_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_email/delete_email.php",
          data: { "email_temp_id": email_temp_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Email Template deleted successfully');
                $("#emailtable").load(location.href + " #emailtable");
            }
            else {
                toastr.error('Unable to delete email template');
            }
          }
        });
        return false;
    }
</script>

<?php
//include database configuration file
ini_set('max_input_time', '600');
if (session_status() === PHP_SESSION_NONE) if (session_status() === PHP_SESSION_NONE) session_start();
require "../../filestobeincluded/db_config.php";

date_default_timezone_set('Asia/Kolkata');
$time =  date('Y-m-d h:i:s a');
$full_time = date("F j", strtotime($time));
$users = $conn->query("SELECT * FROM users");

if ($users->num_rows > 0) {
    $delimiter = ",";
    $filename = "Time_" . date('Y-m-d') . ".csv";

    //create a file pointer
    $f = fopen('php://memory', 'w');

    //set column headers
    $fields = array('User', 'Last Login Time', 'Last Logout Time');

    fputcsv($f, $fields, $delimiter);

    //output each row of the data, format line as csv and write to file pointer
    while ($row = mysqli_fetch_assoc($users)) {

        $time_login = $conn->query("SELECT * FROM Time_Details WHERE Type like 'login' AND Time like '%$full_time%' AND User_ID= '" . $row['ID'] . "' ORDER BY Time ASC ");
        $time_logout = $conn->query("SELECT * FROM Time_Details WHERE Type like 'logout' AND Time like '%$full_time%' AND User_ID= '" . $row['ID'] . "' ORDER BY Time DESC ");
        $time_login = mysqli_fetch_assoc($time_login);
        $time_logout = mysqli_fetch_assoc($time_logout);

        $lineData = array($row['Name'], $time_login['Time'], $time_logout['Time']);
        fputcsv($f, $lineData, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);

    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');

    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

<?php require "filestobeincluded/db_config.php" ?>

<?php

$all_stages = array();
$all_reasons = array();

$stages_query_res = $conn->query("SELECT * FROM Stages");
while($row = $stages_query_res->fetch_assoc()) {
    $all_stages[] = $row;
}

$reasons_query_res = $conn->query("SELECT * FROM Reasons");
while ($row = $reasons_query_res->fetch_assoc()) {
    $all_reasons[] = $row;
}

?>

<div class="card mb-1 shadow-none border">
    <a href="" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
        <div class="card-header" id="headingFour"><h5 class="m-0 font-size-16">Reasons <i class="uil uil-angle-down float-right accordion-arrow"></i></h5></div>
    </a>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
        <div class="card-body text-muted">
<button class="btn btn-primary" data-toggle="modal" data-target="#uploadreasonmodal"><i data-feather="upload" class="icon-xs"></i> Upload Reason Data</button>
<!--------------Upload Leads Modal------------->
<div id="uploadreasonmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="uploadleads" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadleads">Upload Reasons</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="upload_reason" method="POST" enctype="multipart/form-data">
                
                <div class="form-group row">
                    <label class="col-lg-9 col-form-label"
                        for="example-fileinput">Upload File with Prospect Reason Data</label>
                        <div class="col-lg-3 col-sm-12">
                        <a href="/assets/files/Blackboard Reason Data Sample.csv" download="Blackboard Reason Data Sample.csv"><p><i data-feather="file" class="icon-dual icon-xs"></i> Download Sample</p></a>
                    </div><br>
                    <div class="col-lg-12">
                        <input type="file" name="lead_file" class="form-control" style="border: 0ch;"
                            accept=".csv">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                <button type="submit" name="upload" id="upload" class="btn btn-primary">&nbsp;&nbsp;Upload&nbsp;&nbsp;</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>  
      $(document).ready(function(){  
           $('#upload_reason').on("submit", function(e){  
                e.preventDefault(); //form will not submitted  
                $.ajax({  
					url: "settings_pages/ajax_reason/upload_reason.php",
                     method:"POST",  
                     data:new FormData(this),  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){  
                     	console.log(data);
						$('.modal').modal('hide');
						if(data.match("true")) {
							toastr.success('Reason uploaded successfully');
							$("#accordionExample").load(location.href + " #accordionExample" , function () {
                                $("#headingFour").click();
                            });
						}
						else {
							toastr.error('Unable to upload reasons');
						}  
                     }  
                })  
           });  
      });  
 </script>
<!----------------End---------------->


        <button class="btn btn-primary" data-toggle="modal" data-target="#addreasonmodal"> <i class="uil uil-plus-circle"></i> Add Reason</button>
        <button class="btn btn-primary" id="ExportReasontoExcel"> <i class="fa fa-download"></i> Download</button>
<script>
                                        $("#ExportReasontoExcel").on("click", function() {
                                             
                                                $.ajax
                                                ({
                                                type: "POST",
                                                url: "/ajax_leads/export_reason.php",
                                                data: {  },
                                                success: function (data) {
                                                     /*
                                                    * Make CSV downloadable
                                                    */
                                                    var downloadLink = document.createElement("a");
                                                    var fileData = ['\ufeff'+data];

                                                    var blobObject = new Blob(fileData,{
                                                        type: "text/csv;charset=utf-8;"
                                                    });

                                                    

                                                    var url = URL.createObjectURL(blobObject);
                                                    downloadLink.href = url;
                                                    downloadLink.download = "Blackboard_Reason_<?php echo date("d-m-Y h:i a"); ?>.csv";

                                                    /*
                                                    * Actually download CSV
                                                    */
                                                    document.body.appendChild(downloadLink);
                                                    downloadLink.click();
                                                    document.body.removeChild(downloadLink);
                                                                                                    

                                                }
                                                });
                                                return false;
                                        });</script>
            <!----Add Sub-Source Modal-------->
            <div class="modal fade" id="addreasonmodal" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myCenterModalLabel">Add New Reason</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label"
                                        for="simpleinput">Reason</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="reason_name" placeholder="Reason">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Stage</label>
                                    <div class="col-lg-10">
                                        <select id="reasons_stage_id" class="form-control custom-select">
                                            <?
                                            foreach ($all_stages as $stage) {
                                                ?>
                                                <option value="<?php echo($stage['ID']); ?>"><?php echo $stage['Name'];  ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary float-right" type="button" onclick="addReason()">Save</button>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <div id="all_reasons" class="table-responsive">
                <br>
            <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
                <table class="table table-hover mb-0 basic-datatable">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Reason</th>
                        <th scope="col">Stage</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $counter = 0;
                        foreach ($all_reasons as $reason) {
                            $counter++;
                            if(strcasecmp($reason['Status'], 'Y')==0) {
                                $switch_status = 'checked';
                                $update_status = 'N';
                            }
                            else {
                                $switch_status = 'unchecked';
                                $update_status = 'Y';
                            }

                            $get_stage_dets = $conn->query("SELECT * FROM Stages WHERE ID = '".$reason['Stage_ID']."'");
                            $stage_dets = mysqli_fetch_assoc($get_stage_dets);
                            ?>
                            <tr>
                                <th scope="row"><?php echo $counter; ?></th>
                                <td><?php echo $reason['Name']; ?></td>
                                <td><?php echo $stage_dets['Name']; ?></td>
                                <td>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" <?php echo $switch_status; ?> id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1"></label>
                                    </div>
                                </td>
                                <td>
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#editreasonmodal<?php echo $counter; ?>" style="cursor: pointer;" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>&nbsp;&nbsp;
                                    <!----Edit Source Modal-------->
                                    <div class="modal fade" id="editreasonmodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Edit Reason</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="">
                                                        <input type="hidden" id="reason_id<?php echo $counter; ?>" value="<?php echo $reason['ID']; ?>">
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label"
                                                                for="simpleinput">Reason</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" class="form-control" id="new_reason_name<?php echo $counter; ?>" placeholder="Reason" value="<?php echo $reason['Name']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Stage</label>
                                                            <div class="col-lg-10">
                                                                <select id="reasons_new_stage_id<?php echo $counter; ?>" class="form-control custom-select">
                                                                    <?php
                                                                    foreach ($all_stages as $stage) {
                                                                        ?>
                                                                        <option value="<?php echo($stage['ID']); ?>"><?php echo $stage['Name'];  ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary float-right" type="button" onclick="updateReason(<?php echo $counter; ?>)">Update</button>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    <i class="fa fa-trash" aria-hidden="true" style="cursor: pointer;" data-toggle="modal" data-target="#deletereasonmodal<?php echo $counter; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <!----delete Source Modal-------->
                                    <div class="modal fade" id="deletereasonmodal<?php echo $counter; ?>" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST">
                                                        <input type="hidden" id="delete_reason_id<?php echo $counter; ?>" value="<?php echo $reason['ID']; ?>">
                                                        <center><button class="btn btn-danger textS-center" type="button" onclick="deleteReason(<?php echo $counter; ?>)">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-info" data-dismiss="modal">Cancel</button></center>
                                                    </form>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	function addReason() {
		var reason_name = $('#reason_name').val();
		var stage_id = $('#reasons_stage_id').val();

      $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_reason/add_reason.php",
          data: { "reason_name": reason_name, "stage_id": stage_id },
          success: function (data) {
            $('#addreasonmodal').modal('hide');
            if(data.match("true")) {
                toastr.success('Reason added successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingFour").click();
                });
            }
            else {
                toastr.error('Unable to add reason');
            }
          }
        });
        return false;
	}
</script>

<script>
    function updateReason(id) {
        var reason_id = $('#reason_id'.concat(id)).val();
        var new_stage_id = $('#reasons_new_stage_id'.concat(id)).val();
        var new_reason_name = $('#new_reason_name'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_reason/update_reason.php",
          data: { "reason_id": reason_id, "new_stage_id": new_stage_id, "new_reason_name": new_reason_name },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Reason updated successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingFour").click();
                });
            }
            else {
                toastr.error('Unable to update reason');
            }
          }
        });
        return false;
    }
</script>

<script>
    function deleteReason(id) {
        var reason_id = $('#delete_reason_id'.concat(id)).val();

        $.ajax
        ({
          type: "POST",
          url: "settings_pages/ajax_reason/delete_reason.php",
          data: { "reason_id": reason_id },
          success: function (data) {
            $('.modal').modal('hide');
            if(data.match("true")) {
                toastr.success('Reason deleted successfully');
                $("#accordionExample").load(location.href + " #accordionExample" , function () {
                    $("#headingFour").click();
                });
            }
            else {
                toastr.error('Unable to delete reason');
            }
          }
        });
        return false;
    }
</script>
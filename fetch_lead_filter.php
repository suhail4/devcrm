<?php require 'filestobeincluded/db_config.php'; ?>
<?php
$itemPerPage = 25;
$page = 1;
if (!empty($_POST['itemPerPage']))
	$itemPerPage = $_POST['itemPerPage'];
if (!empty($_POST['page']))
	$page = $_POST['page'];

$offset = (($page - 1) * $itemPerPage);
$flag = 1;

if (!empty($_POST['manager_filter'])) {
	$manager_tree__id = implode(',', $_POST['manager_filter']);

	$man_tree_variable = explode(",", $manager_tree__id);
	$man_tree_variable = implode("','", $man_tree_variable);
	$all_array = array();
	$coun_array = array();
	$get_tree = $conn->query("SELECT * FROM users WHERE Reporting_To_User_ID IN ('" . $man_tree_variable . "')");
	if ($get_tree->num_rows > 0) {
		while ($row = $get_tree->fetch_assoc()) {
			$all_array[] = $row;
		}
		foreach ($all_array as $univ) {

			$get_counsellors = $conn->query("SELECT * FROM users WHERE Role = 'Counsellor' AND Reporting_To_User_ID IN ('" . $man_tree_variable . "')");
			while ($rows = $get_counsellors->fetch_assoc()) {
				$coun_array[] = $rows;
			}
		}
		foreach ($coun_array as $cuniv) {
			$couns_array[] = $cuniv['ID'];
		}
		$imp = "'" . implode("','", ($couns_array)) . "'";
		$tree_ids = $imp . ",'" . $man_tree_variable . "'";
	} else {
		$tree_ids = "'" . $man_tree_variable . "'";
	}
}



$var = '';

if (!empty($_POST['manager_filter']) && !empty($_POST['counsellor_filter'])) {

	$couns_id = implode(',', $_POST['counsellor_filter']);

	$variable = explode(",", $couns_id);
	$variable = implode("','", $variable);




	$var = $var . "(Counsellor_ID IN ('" . $variable . "')) AND ";
} elseif (!empty($_POST['manager_filter'])) {

	$manager_id = implode(',', $_POST['manager_filter']);

	$man_variable = explode(",", $manager_id);
	$man_variable = implode("','", $man_variable);
	$var = $var . "(Counsellor_ID IN ('" . $man_variable . "') OR Counsellor_ID IN ($tree_ids)) AND ";
} elseif (!empty($_POST['counsellor_filter'])) {

	$couns_id = implode(',', $_POST['counsellor_filter']);

	$variable = explode(",", $couns_id);
	$variable = implode("','", $variable);




	$var = $var . "(Counsellor_ID IN ('" . $variable . "')) AND ";
}

if (!empty($_POST['manager_filter']) && !empty($_POST['counsellor_filter'])) {

	$couns_id = implode(',', $_POST['counsellor_filter']);

	$variable = explode(",", $couns_id);
	$variable = implode("','", $variable);




	$var = $var . "(Counsellor_ID IN ('" . $variable . "')) AND ";
}

if (!empty($_POST['source_filter'])) {
	$s_id = implode(',', $_POST['source_filter']);

	$variable1 = explode(",", $s_id);
	$variable1 = implode("','", $variable1);

	$var = $var . "(Source_ID IN ('" . $variable1 . "')) AND ";
}
// if(!empty($_POST['campaign_name_filter'])) {
// 	$var = $var.'campaign_name_filter = '.$campaign_name_filter.' AND ';	
// }
if (!empty($_POST['sub_source_filter'])) {
	$sub_id = implode(',', $_POST['sub_source_filter']);
	$variable2 = explode(",", $sub_id);
	$variable2 = implode("','", $variable2);

	$var = $var . "(Subsource_ID IN ('" . $variable2 . "')) AND ";
}
if (!empty($_POST['institute_filter'])) {
	$i_id = implode(',', $_POST['institute_filter']);
	$variable3 = explode(",", $i_id);
	$variable3 = implode("','", $variable3);
	$var = $var . "(Institute_ID IN ('" . $variable3 . "')) AND ";
}
if (!empty($_POST['course_filter'])) {
	$course_array = array();

	$co_id = implode(',', $_POST['course_filter']);
	$variable4 = explode(",", $co_id);


	$variable4 = implode("','", $variable4);

	$sql = $conn->query("SELECT ID FROM Courses WHERE Name IN ('" . $variable4 . "')");
	while ($course_id = mysqli_fetch_assoc($sql)) {
		$course_array[] = $course_id['ID'];
	}

	$courses_id = implode("','", $course_array);


	$var = $var . "(Course_ID IN ('" . $courses_id . "')) AND ";
}
if (!empty($_POST['speclization_filter'])) {
	$sp_id = implode(',', $_POST['speclization_filter']);
	$variable5 = explode(",", $sp_id);
	$variable5 = implode("','", $variable5);
	$var = $var . "(Specialization_ID IN ('" . $variable5 . "')) AND ";
}
if (!empty($_POST['state_filter'])) {
	$state_id = implode(',', $_POST['state_filter']);
	$state_variable = explode(",", $state_id);
	$state_variable = implode("','", $state_variable);
	$var = $var . "(State_ID IN ('" . $state_variable . "')) AND ";
}
// if(!empty($_POST['communication_type_filter'])) {
// 	$var = $var.'Institute_ID IN ('.$_POST['communication_type_filter'].') AND ';	
// }

// if(!empty($_POST['reason_filter'])) {
// 	$r_id = implode(',', $_POST['reason_filter']);
// 	$variable7=explode(",", $r_id);
// 	$variable7=implode("','", $variable7);
// 	$reason_id = str_replace(',', " OR ", $variable7);
// 	$var = $var."(Reason_ID = '".$reason_id."') AND ";	
// }
if ($_POST['creation_date_filter']) {
	if (strpos($_POST['creation_date_filter'], 'to') > 0) {
		$date = explode('to', $_POST['creation_date_filter']);
		$from_date = date($date[0] . $_POST['creation_to_filter']);
		$to_date = date($date[1] . " " . $_POST['creation_from_filter']);
		$var = $var . "((Created_at) >= '" . $from_date . "' AND (Created_at) <= '" . $to_date . "') AND ";
	} else {
		$from_date = date($_POST['creation_date_filter'] . " " . $_POST['creation_to_filter']);
		$to_date = date($_POST['creation_date_filter'] . " " . $_POST['creation_from_filter']);
		// $var = $var . "((Created_at) = '" . $from_date . "') AND ";
		// print_r($from_date.' '.$to_date);
		$var = $var . "((Created_at) >= '" . $from_date . "' AND date(Created_at) <= '" . $to_date . "') AND ";
	}
}

if ($_POST['update_date_filter']) {
	if (strpos($_POST['update_date_filter'], 'to') > 0) {
		$date1 = explode('to', $_POST['update_date_filter']);
		$from_date1 = date($date1[0] . " " . $_POST['update_to_filter']);
		$to_date1 = date($date1[1] . " " . $_POST['update_to_filter']);
		$var = $var . "((TimeStamp) >= '" . $from_date1 . "' AND (TimeStamp) <= '" . $to_date1 . "') AND ";
	} else {
		$from_date1 = date($_POST['update_date_filter']);
		$to_date1 = date($_POST['update_date_filter']);
		// $var = $var . "(date(TimeStamp) = '" . $from_date1 . "') AND ";
		$var = $var . "((TimeStamp) >= '" . $from_date1 . "' AND date(TimeStamp) <= '" . $to_date1 . "') AND ";
	}
}




$tabValue = '';



if (isset($_POST['tabValue']) != '') {


	$tabValue = $_POST['tabValue'];
	$get_reason = $conn->query("SELECT * FROM Reasons WHERE Stage_ID = '" . $tabValue . "'");
}
$sub_dis = '';
if (isset($_POST['sub_dis']) != '') {
	$sub_dis = $_POST['sub_dis'];
}

$without_stage = substr($var, 0, strlen($var) - 4);
$without = $var;
if (!empty($_POST['stage_filter'])) {
	$stag_id = implode(',', $_POST['stage_filter']);
	$variable6 = explode(",", $stag_id);
	$variable6 = implode("','", $variable6);
	$var = $var . "(Stage_ID IN ('" . $variable6 . "')) AND ";
}

$var = substr($var, 0, strlen($var) - 4);








// $campaign_name_filter = $_POST['campaign_name_filter'];


// $communication_type_filter = $_POST['communication_type_filter'];



$result = $conn->query("SELECT * FROM Leads WHERE $var ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
$totRecords = $conn->query("SELECT ID FROM Leads WHERE $var ORDER BY TimeStamp DESC")->num_rows;


if ($tabValue > 0  && $tabValue < 17 && $sub_dis == '' && $without_stage == '') {
	$result = $conn->query("SELECT * FROM Leads WHERE Stage_ID = '" . $tabValue . "' ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
	$totRecords = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = '" . $tabValue . "' ORDER BY TimeStamp DESC")->num_rows;
}
if ($tabValue > 0  && $tabValue < 17 && $sub_dis == '' && $without_stage != '') {
	$result = $conn->query("SELECT * FROM Leads WHERE $without_stage AND Stage_ID = '" . $tabValue . "' ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
	$totRecords = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = '" . $tabValue . "' ORDER BY TimeStamp DESC")->num_rows;
}
if ($tabValue > 0  && $tabValue < 17 && $sub_dis != '' && $without_stage == '') {
	$result = $conn->query("SELECT * FROM Leads WHERE Stage_ID = '" . $tabValue . "' AND Reason_ID = '" . $sub_dis . "' ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
	$totRecords = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = '" . $tabValue . "' AND Reason_ID = '" . $sub_dis . "' ORDER BY TimeStamp DESC")->num_rows;
}
if ($tabValue > 0  && $tabValue < 17 && $sub_dis != '' && $without_stage != '') {
	$result = $conn->query("SELECT * FROM Leads WHERE $without_stage AND Stage_ID = '" . $tabValue . "' AND Reason_ID = '" . $sub_dis . "' ORDER BY TimeStamp DESC LIMIT $itemPerPage OFFSET $offset");
	$totRecords = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = '" . $tabValue . "' AND Reason_ID = '" . $sub_dis . "' ORDER BY TimeStamp DESC")->num_rows;
}

$totalPages = ceil($totRecords / $itemPerPage);

//  $without = $without."(Stage_ID = '".$tabValue."')"



?>

<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-lg-12" style="padding-top: 20px; padding-bottom:20px;">
				<?php if (!empty($_POST['stage_filter'])) {
					$allleads_query = $conn->query("SELECT * FROM Leads WHERE $var");
					$row_count_allleads = mysqli_num_rows($allleads_query); ?>
					<?php if ($tabValue == '' || $tabValue == 0) { ?>
						<button id="all_lead" onclick="add_active(this.value);LeadFilter()" value="0" class="btn btn-light user_tabs active">All leads (<?php echo $row_count_allleads; ?>)</button>
					<?php } else { ?>
						<button id="all_lead" onclick="add_active(this.value);LeadFilter()" value="0" class="btn btn-light user_tabs">All leads (<?php echo $row_count_allleads; ?>)</button>
					<?php } ?>

					<?php if (in_array("1", $_POST['stage_filter']) && $tabValue == 1) { ?>
						<button id="new_lead" value="1" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">New leads (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("1", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$newleads_query = $conn->query("SELECT * FROM Leads WHERE $without_stage AND Stage_ID = 1");
						} else {
							$newleads_query = $conn->query("SELECT * FROM Leads WHERE Stage_ID = 1");
						}

						$row_count_newleads = mysqli_num_rows($newleads_query); ?>
						<button id="new_lead" value="1" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">New leads (<?php echo $row_count_newleads; ?>)</button>
					<?php } else { ?>
						<button id="new_lead" value="1" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">New leads (0)</button>
					<?php } ?>

					<?php if (in_array("2", $_POST['stage_filter']) && $tabValue == 2) { ?>
						<button id="follow_up" value="2" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Follow-up (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("2", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$connectedleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 2");
						} else {
							$connectedleads_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = 2");
						}

						$row_count_connectedleads = mysqli_num_rows($connectedleads_query);
					?>
						<button id="follow_up" value="2" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Follow-up (<?php echo $row_count_connectedleads; ?>)</button>
					<?php } else { ?>
						<button id="follow_up" value="2" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Follow-up (0)</button>
					<?php } ?>

					<?php if (in_array("3", $_POST['stage_filter']) && $tabValue == 3) { ?>
						<button id="not_interested" value="3" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Not Interested (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("3", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$notintrestedleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 3");
						} else {
							$notintrestedleads_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = 3");
						}

						$row_count_notintrestedleads = mysqli_num_rows($notintrestedleads_query);
					?>
						<button id="not_interested" value="3" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Not Interested (<?php echo $row_count_notintrestedleads; ?>)</button>
					<?php } else { ?>
						<button id="not_interested" value="3" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Not Interested (0)</button> <?php } ?>

					<?php if (in_array("4", $_POST['stage_filter']) && $tabValue == 4) { ?>
						<button id="reg_done" value="4" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Registration Done (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("4", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$registrationleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 4");
						} else {
							$registrationleads_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = 4");
						}

						$row_count_registrationleads = mysqli_num_rows($registrationleads_query);
					?>
						<button id="reg_done" value="4" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Registration Done (<?php echo $row_count_registrationleads; ?>)</button>
					<?php } else { ?>
						<button id="reg_done" value="4" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Registration Done (0)</button>
					<?php } ?>


					<?php if (in_array("5", $_POST['stage_filter']) && $tabValue == 5) { ?>
						<button id="admi_done" value="5" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Admission Done (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("5", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$admissionleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 5");
						} else {
							$admissionleads_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = 5");
						}

						$row_count_admissionleads = mysqli_num_rows($admissionleads_query);
					?>
						<button id="admi_done" value="5" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Admission Done (<?php echo $row_count_admissionleads; ?>)</button>
					<?php } else { ?>
						<button id="admi_done" value="5" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Admission Done (0)</button>
					<?php } ?>

					<?php if (in_array("6", $_POST['stage_filter']) && $tabValue == 6) { ?>
						<button id="intr_b2b" value="6" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Intrested for B2B (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("6", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$b2bleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 6");
						} else {
							$b2bleads_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = 6");
						}

						$row_count_b2bleads = mysqli_num_rows($b2bleads_query);
					?>
						<button id="intr_b2b" value="6" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Intrested for B2B (<?php echo $row_count_b2bleads; ?>)</button>

					<?php } else { ?>
						<button id="intr_b2b" value="6" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Intrested for B2B (0)</button>
					<?php } ?>

					<?php if (in_array("7", $_POST['stage_filter']) && $tabValue == 7) { ?>
						<button id="not_connect" value="7" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Not Connected (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("7", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$notconnectedleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 7");
						} else {
							$notconnectedleads_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = 7");
						}

						$row_count_notconnectedleads = mysqli_num_rows($notconnectedleads_query);
					?>
						<button id="not_connect" value="7" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Not Connected (<?php echo $row_count_notconnectedleads; ?>)</button>
					<?php } else { ?>
						<button id="not_connect" value="7" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Not Connected (0)</button>
					<?php } ?>


					<?php if (in_array("8", $_POST['stage_filter']) && $tabValue == 8) { ?>
						<button id="re_enquiry" value="8" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Re-enquired (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("8", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$reenquiredleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 8");
						} else {
							$reenquiredleads_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID = 8");
						}

						$row_count_reenquiredleads = mysqli_num_rows($reenquiredleads_query);
					?>
						<button id="re_enquiry" value="8" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Re-enquired (<?php echo $row_count_reenquiredleads; ?>)</button>
					<?php } else { ?>
						<button id="re_enquiry" value="8" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Re-enquired (0)</button>
					<?php } ?>

					<?php if (in_array("10", $_POST['stage_filter']) && $tabValue == 10) { ?>
						<button id="intr" value="10" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Interested For Next Session (<?php echo $totRecords; ?>)</button>
					<?php } elseif (in_array("10", $_POST['stage_filter'])) {
						if ($without_stage != '') {
							$intr_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID like '10'");
						} else {
							$intr_query = $conn->query("SELECT ID FROM Leads WHERE Stage_ID like '10' ");
						}

						$row_count_intr = mysqli_num_rows($intr_query);

					?>
						<button id="intr" value="10" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Interested For Next Session (<?php echo $row_count_intr; ?>)</button>
					<?php } else { ?>
						<button id="intr" value="10" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Interested For Next Session (0)</button>
					<?php } ?>
					


				<?php } else {
				?>
					<?php
					$allleads_query = $conn->query("SELECT ID FROM Leads WHERE $var");
					$row_count_allleads = mysqli_num_rows($allleads_query);
					if ($tabValue == '' || $tabValue == 0) {
						
					?>

						<button id="all_lead" onclick="add_active(this.value);LeadFilter()" value="0" class="btn btn-light user_tabs active">All leads (<?php echo $row_count_allleads ?>)</button>
					<?php } else { ?>
						<button id="all_lead" onclick="add_active(this.value);LeadFilter()" value="0" class="btn btn-light user_tabs">All leads (<?php echo $row_count_allleads ?>)</button>
					<?php }
					$newleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 1");
					$row_count_newleads = mysqli_num_rows($newleads_query);
					if ($tabValue == 1) {
					?>
						<button id="new_lead" value="1" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">New leads (<?php echo $totRecords; ?>)</button>
					<?php } else { ?>
						<button id="new_lead" value="1" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">New leads (<?php echo $row_count_newleads ?>)</button>
					<?php }
					$connectedleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 2");
					$row_count_connectedleads = mysqli_num_rows($connectedleads_query);
					if ($tabValue == 2) {
					?>
						<button id="follow_up" value="2" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Follow-up (<?php echo $totRecords; ?>)</button>
					<?php } else { ?>
						<button id="follow_up" value="2" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Follow-up (<?php echo $row_count_connectedleads ?>)</button>
					<?php }
					$notintrestedleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 3");
					$row_count_notintrestedleads = mysqli_num_rows($notintrestedleads_query);
					if ($tabValue == 3) {
					?>
						<button id="not_interested" value="3" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Not Interested (<?php echo $totRecords; ?>)</button>
					<?php } else { ?>
						<button id="not_interested" value="3" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Not Interested (<?php echo $row_count_notintrestedleads ?>)</button>
					<?php }
					$registrationleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 4");
					$row_count_registrationleads = mysqli_num_rows($registrationleads_query);
					if ($tabValue == 4) {
					?>
						<button id="reg_done" value="4" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Registration Done (<?php echo $totRecords; ?>)</button>
					<?php } else { ?>
						<button id="reg_done" value="4" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Registration Done (<?php echo $row_count_registrationleads ?>)</button>
					<?php  }
					$admissionleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 5");
					$row_count_admissionleads = mysqli_num_rows($admissionleads_query);
					if ($tabValue == 5) {
					?>
						<button id="admi_done" value="5" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Admission Done (<?php echo $totRecords; ?>)</button>
					<?php } else { ?>
						<button id="admi_done" value="5" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Admission Done (<?php echo $row_count_admissionleads ?>)</button>
					<?php }
					$b2bleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 6");
					$row_count_b2bleads = mysqli_num_rows($b2bleads_query);
					if ($tabValue == 6) {
					?>
						<button id="intr_b2b" value="6" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Intrested for B2B (<?php echo $totRecords ?>)</button>
					<?php } else { ?>
						<button id="intr_b2b" value="6" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Intrested for B2B (<?php echo $row_count_b2bleads ?>)</button>
					<?php }
					$notconnectedleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 7");
					$row_count_notconnectedleads = mysqli_num_rows($notconnectedleads_query);
					if ($tabValue == 7) {
					?>
						<button id="not_connect" value="7" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Not Connected (<?php echo $totRecords ?>)</button>
					<?php } else { ?>
						<button id="not_connect" value="7" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Not Connected (<?php echo $row_count_notconnectedleads ?>)</button>
					<?php }
					$reenquiredleads_query = $conn->query("SELECT ID FROM Leads WHERE $without_stage AND Stage_ID = 8");
					$row_count_reenquiredleads = mysqli_num_rows($reenquiredleads_query);
					if ($tabValue == 8) {
					?>
						<button id="re_enquiry" value="8" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Re-enquired (<?php echo $totRecords; ?>)</button>
					<?php } else { ?>
						<button id="re_enquiry" value="8" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Re-enquired (<?php echo $row_count_reenquiredleads; ?>)</button>

					<?php }

					$intr_query = $conn->query("SELECT COUNT(ID) as count FROM Leads WHERE $without_stage AND Stage_ID = 10");
					$row_count_intr = mysqli_fetch_assoc($intr_query)['count'];
					if ($tabValue == 10) {
					?>
						<button id="intr" value="10" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs active">Interested For Next Session (<?php echo $totRecords; ?>)</button>
					<?php } else { ?>
						<button id="intr" value="10" onclick="add_active(this.value);LeadFilter()" class="btn btn-light user_tabs">Interested For Next Session (<?php echo $row_count_intr; ?>)</button>

				<?php }
				} ?>
			</div>
			<div class="row">
				<div class="custom-control custom-checkbox col-lg-6" style="padding-left: 44px; float: left;">
					<input type="checkbox" onclick="checkAllBoxes()" class="custom-control-input checkbox-function" id="select_all_boxes" value="not-checked">
					<label class="custom-control-label" for="select_all_boxes"></label>
				</div>
				<div class="col-lg-6">

				</div>
			</div>
			<br />
			<?php if ($tabValue != '' && $tabValue > 0) {  ?>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group mt-3 mt-sm-0">
							<label>Sub-dispositions</label>
							<select class="form-control" onchange="LeadFilter()" id="sub_dis">
								<option value=''>Select Sub-Disposition</option>
								<?php
								while ($gr = $get_reason->fetch_assoc()) {


									$totReason = $conn->query("SELECT * FROM Leads WHERE $var AND Stage_ID = '" . $tabValue . "' AND Reason_ID='" . $gr['ID'] . "' ORDER BY TimeStamp DESC")->num_rows;

								?>
									<option value="<?php echo $gr['ID']; ?>" <?php echo ($sub_dis ==  $gr['ID']) ? ' selected="selected"' : ''; ?>><?php echo $gr['Name'] . ' (' . $totReason . ')'; ?></option>

								<?php
								}

								?>
							</select>
						</div>
					</div>
				</div>
			<?php } ?>
			<br>
			<div id="divLoader" class="col-12" style="display: none; height: 100%;">
				<center>
					<div class="spinner-grow text-primary m-2" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</center>
			</div>

			<br>
			<form id="checkbox-form" method="POST">
				<table class="table table-hover table-striped">
					<tbody id="leads_tbody">

						<?php



						if ($totalPages > 1) {
							echo '<div id="pagination" class="candidates-list" style="float:right;margin-bottom:10px;">
                      <span id="pagination_info" data-totalpages ="' . $totalPages . '"  data-currentpage ="' . $page . '" >
                          Page ' . $page . ' out of ' . $totalPages . '  Pages
                      </span>
                      <a href="javascript:void(0)" data-page="' . $page . '" id="pre" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" onClick="LeadFilter(`pre`)";>
                          Previous
                      </a>
                      <a href="javascript:void(0)" id="next" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" data-page ="' . $page . '" onClick="LeadFilter(`next`)";>
                          Next
                      </a>
                </div><br><br>';
						}


						while ($row = $result->fetch_assoc()) {
							echo '<tr>
	<td>
		<div class="row" style="padding-top: 10px;" id="row' . $row['ID'] . '">
			<div class="col-lg-1">
				<div class="custom-control custom-checkbox">
				<input type="checkbox" onclick="checkbox()" class="custom-control-input checkbox-function" name="id[]" id="customCheck2' . $row['ID'] . '" value="' . $row['ID'] . '">
				<label class="custom-control-label" for="customCheck2' . $row['ID'] . '"></label>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p id="person_name' . $row['ID'] . '" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy name"><b>Name:</b> ' . $row['Name'] . '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p id="person_email' . $row['ID'] . '" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy email"><b>Email:</b> ' . $row['Email'] . ' </p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p id="person_mobile' . $row['ID'] . '" onclick="copyName(this.id)" style="cursor: pointer;" title="Copy mobile"><b>Mobile:</b> <a href="tel:' . $row['Mobile'] . '">' . $row['Mobile'] . '</a></p>
					</div>';
							if ($row['Alt_Mobile'] != '') {
								echo '<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Alt Mobile:</b> <a href="tel:' . $row['Alt_Mobile'] . '">' . $row['Alt_Mobile'] . '</a></p>
						</div>';
							}
							echo '
					<div class="col-lg-12 col-md-3 col-sm-12">'; ?>
							<?php
							$get_followup_remark = $conn->query("SELECT Remark FROM Follow_Ups WHERE Lead_ID = '" . $row['ID'] . "' ORDER BY ID DESC LIMIT 1");
							if ($get_followup_remark->num_rows > 0) {
								$re_mark = mysqli_fetch_assoc($get_followup_remark);
								if (strlen($re_mark['Remark']) > 40) {
									echo '<p><b>Remark:</b>&nbsp;' . substr($re_mark['Remark'], 0, 40) . '...<button type="button" onclick="pop();" class="btn btn-link btn-sm" data-container="body" title=""
									data-toggle="popover" data-placement="left"
									data-content= "' . $re_mark['Remark'] . '"
									data-original-title="Remark">
									Read More
								</button>';
								} else {
									echo '<p><b>Remark:</b>&nbsp;' . $re_mark['Remark'];
								}
							} else {
								echo '';
							}

							?>



							<?php echo '</p>
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu mb-0">
							<li class="nav-link" role="button" aria-haspopup="false" aria-expanded="false">';
							$fsql = "SELECT COUNT(Lead_ID) as Leadid FROM Follow_Ups WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
							$fresult = $conn->query($fsql);
							if ($fresult->num_rows > 0) {
								while ($frow = $fresult->fetch_assoc()) {
									$gfc = $frow["Leadid"];
								}
							} else {
								$gfc = "0";
							}
							echo '<font style="font-size: 24px; cursor:pointer;" onclick="followupmodal(' . $row['ID'] . ');"><i class="fas fa-user-tie"></i></font>
								<span><mark class="mark1">&nbsp;' . $gfc . '&nbsp;</mark></span>
							</a></li>
							<li class="nav-link" role="button" aria-haspopup="false"
								aria-expanded="false">';
							$elsql = "SELECT COUNT(Lead_ID) as Leadid FROM Email_Logs WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
							$elresult = $conn->query($elsql);
							if ($elresult->num_rows > 0) {
								while ($elrow = $elresult->fetch_assoc()) {
									$gelc = $elrow["Leadid"];
								}
							} else {
								$gelc = "0";
							}
							$slsql = "SELECT COUNT(Lead_ID) as Leadid FROM SMS_Logs WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
							$slresult = $conn->query($slsql);
							if ($slresult->num_rows > 0) {
								while ($slrow = $slresult->fetch_assoc()) {
									$gslc = $slrow["Leadid"];
								}
							} else {
								$gslc = "0";
							}
							$clsql = "SELECT COUNT(Lead_ID) as Leadid FROM Call_Logs WHERE Lead_ID = '" . $row['ID'] . "' GROUP BY Lead_ID";
							$clresult = $conn->query($clsql);
							if ($clresult->num_rows > 0) {
								while ($clrow = $clresult->fetch_assoc()) {
									$gclc = $clrow["Leadid"];
								}
							} else {
								$gclc = "0";
							}
							$add_both = $gelc + $gslc + $gclc;
							echo '<font style="font-size: 24px; cursor:pointer;" onclick="responsesmodal(' . $row['ID'] . ');"><i class="fas fa-user-graduate"></i></font>
								<span><mark class="mark2">&nbsp;' . $add_both . '&nbsp;</mark></span>
							</a></li>
							<li class="nav-link" role="button" aria-haspopup="false"
								aria-expanded="false">';
							$lead_name = mysqli_real_escape_string($conn, $row['Name']);
							$rsql = "SELECT COUNT(ID) as Leadid FROM Re_Enquired WHERE Name = '" . $lead_name . "' AND Email = '" . $row['Email'] . "' AND Mobile = '" . $row['Mobile'] . "' AND Institute_ID = '" . $row['Institute_ID'] . "'";
							$rresult = $conn->query($rsql);
							if ($rresult->num_rows > 0) {
								while ($rrow = $rresult->fetch_assoc()) {
									$grc = $rrow["Leadid"];
								}
							} else {
								$grc = "0";
							}
							echo '<font style="font-size: 24px; cursor:pointer;" onclick="re_enquiredmodal(' . $row['ID'] . ');"><i class="fas fa-user-tie"></i></font>
								<span><mark class="mark3">&nbsp;' . $grc . '&nbsp;</mark></span>
							</a></li>
						</ul>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">';

							$counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '" . $row['Counsellor_ID'] . "'");
							$counsellor = mysqli_fetch_assoc($counsellor_query);
							if ($counsellor_query->num_rows > 0) {
								$couns = $counsellor['Name'];
							} else {
								$counsellor['Name'] = ' ';
								$couns = $counsellor['Name'];
							}

							echo '<p><b>Counsellor:</b> ' . $couns . '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12" style="padding-top: 15px;">';
							$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '" . $row['Institute_ID'] . "'");
							$univ = mysqli_fetch_assoc($univ_query);
							$suv_id_query = $conn->query("SELECT urnno from Leads where ID ='" . $row['ID'] . "'");
							$suv = mysqli_fetch_assoc($suv_id_query);
							$newdate = date("d-M-Y", strtotime($row['dob']));
							if (($univ['ID'] == 51 || $univ['ID'] == 60) && $suv['urnno']) {
								echo '<p><b>IDOL Password:</b> ';
								echo strtoupper(str_replace('-', '', $newdate)) . '</p>';
							}
							echo '
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>University:</b>';
							$univ_query = $conn->query("SELECT * FROM Institutes WHERE ID = '" . $row['Institute_ID'] . "'");
							$univ = mysqli_fetch_assoc($univ_query);
							if ($univ_query->num_rows > 0) {
								if (strcasecmp($univ['Name'], 'Admin') == 0) {
									$univ['Name'] = '';
									echo $univ['Name'];
								} else {
									echo $univ['Name'];
								}
							} else {
								$univ['Name'] = ' ';
								echo $univ['Name'];
							}

							echo '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Course:</b>';
							$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '" . $row['Course_ID'] . "'");
							$course = mysqli_fetch_assoc($course_query);
							if ($course_query->num_rows > 0) {
								echo $course['Name'];
							} else {
								$course['Name'] = ' ';
								echo $course['Name'];
							}

							echo '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Specialization:</b>';
							$specialization_query = $conn->query("SELECT * FROM Specializations WHERE ID = '" . $row['Specialization_ID'] . "'");
							$specialization = mysqli_fetch_assoc($specialization_query);
							if ($specialization_query->num_rows > 0) {
								echo substr($specialization['Name'], 0, 27);
							} else {
								$specialization['Name'] = ' ';
								echo $specialization['Name'];
							}
							echo '</p>
					</div>';
							if ($univ['ID'] == 64) {
								echo '<div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>SVU ID:</b>';

								if ($suv['urnno']) {
									echo $suv['urnno'];
								} else {
									echo '<button type="button" onclick="callSVUApi(' . $row['ID'] . ',' . $row['dob'] . ')" class="btn btn-sm btn-outline-primary mx-1">Generate SVU ID</button>';
								}
								echo '</p>
						</div>';
							} else if ($univ['ID'] == 51 || $univ['ID'] == 60) {
								echo '<div class="col-lg-12 col-md-3 col-sm-12" id="idolid' . $row['ID'] . '">
						<p><b>IDOL ID:</b>';
								$suv_id_query = $conn->query("SELECT urnno from Leads where ID ='" . $row['ID'] . "'");
								$suv = mysqli_fetch_assoc($suv_id_query);
								if ($suv['urnno']) {
									echo $suv['urnno'];
								} else {
									echo '<button type="button" onclick="callCuApi(&#39;' . $row['ID'] . '&#39;);" class="btn btn-sm btn-outline-primary mx-1">Generate IDOL ID</button>';
								}
								echo '</p>
						</div>';
							}

							echo '<div class="col-lg-12 col-md-3 col-sm-12">'; ?>
							<?php
							$get_followup_date = $conn->query("SELECT Followup_Timestamp FROM Follow_Ups WHERE Lead_ID = '" . $row['ID'] . "' ORDER BY ID DESC LIMIT 1");
							if ($get_followup_date->num_rows > 0) {
								$current_timestamp = date('Y-m-d h:i:s');
								$date = mysqli_fetch_assoc($get_followup_date);
								if ($current_timestamp < $date['Followup_Timestamp']) {
									echo '<p><b>Next Follow-up Date:</b>&nbsp;' . date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
								} else {
									echo '<p><b>Previous Follow-up Date:</b>&nbsp;' . date("F j, Y g:i a", strtotime($date["Followup_Timestamp"]));
								}
							} else {
								echo '';
							}

							?>

							<?php echo '</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">';

							$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '" . $row['Stage_ID'] . "'");
							$stage = mysqli_fetch_assoc($stage_query);
							if ($stage_query->num_rows > 0) {
								$lead_stage = $stage['Name'];
							} else {
								$stage['Name'] = ' ';
								$lead_stage = $stage['Name'];
							}

							echo '<p><b>Stage:</b> ' . $lead_stage . '</p>
					</div>
					<div class="col-lg-12 col-md-3 col-sm-12">';

							$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '" . $row['Reason_ID'] . "'");
							$reason = mysqli_fetch_assoc($reason_query);
							if (strcasecmp($stage['Name'], "NEW") == 0 || strcasecmp($stage['Name'], "FRESH") == 0) {
								$badge = "success";
							} else if (strcasecmp($stage['Name'], "COLD") == 0) {
								$badge = "warning";
							} else {
								$badge = "danger";
							}

							echo '<p><b>Reason:</b> <span class="badge badge-soft-' . $badge . ' py-1">';
							if ($reason_query->num_rows > 0) {
								echo $reason['Name'];
							} else {
								$reason['Name'] = ' ';
								echo $reason['Name'];
							}
							echo '</span>
						</p>
					</div>

					<div class="col-lg-12 col-md-3 col-sm-12">';
							$state_query = $conn->query("SELECT * FROM States WHERE ID = '" . $row['State_ID'] . "'");
							$state = mysqli_fetch_assoc($state_query);
							if ($state_query->num_rows > 0) {
								$lead_state = $state['Name'];
							} else {
								$state['Name'] = ' ';
								$lead_state = $state['Name'];
							}
							echo '<p><b>State:</b>' . $lead_state . '</p>
							  </div>';
							if ($row['Stage_ID'] == 8) {
								echo '<div class="col-lg-12 col-md-3 col-sm-12">';

								$source_query = $conn->query("SELECT * FROM Sources WHERE ID = '" . $row['Source_ID'] . "'");
								$source = mysqli_fetch_assoc($source_query);
								if ($source_query->num_rows > 0) {
									$lead_source = $source['Name'];
								} else {
									$source['Name'] = ' ';
									$lead_source = $source['Name'];
								}

								echo '<p><b>Source:</b> ' . $lead_source . '</p>
						</div>';
							}

							echo '<div class="col-lg-12 col-md-3 col-sm-12">';

							$subsource_query = $conn->query("SELECT * FROM Sub_Sources WHERE ID = '" . $row['Subsource_ID'] . "'");
							$subsource = mysqli_fetch_assoc($subsource_query);
							if ($subsource_query->num_rows > 0) {
								$lead_sub = $subsource['Name'];
							} else {
								$subsource['Name'] = ' ';
								$lead_sub = $subsource['Name'];
							}

							echo '<p><b>Sub-Source:</b> ' . $lead_sub . '</p>
					</div>';
							if ($row['Stage_ID'] == 8) {
								$renquired_date = $conn->query("SELECT Sources.Name,Re_Enquired.TimeStamp FROM Re_Enquired LEFT JOIN Sources ON Re_Enquired.Source_ID = Sources.ID  WHERE Re_Enquired.Lead_ID = '" . $row['ID'] . "' ORDER BY Re_Enquired.TimeStamp DESC");
								$renquired = mysqli_fetch_assoc($renquired_date);
								echo '<div class="col-lg-12 col-md-3 col-sm-12">
							<p><b>Renquired Date:</b>'; ?>
								<?php


								//echo $renquired["TimeStamp"];
								if (isset($renquired["TimeStamp"]) != '') {
									echo date("F j, Y g:i a", strtotime($renquired["TimeStamp"]));
								} else {
									echo '';
								}

								?>

								<?php echo '</p>
						</div><div class="col-lg-12 col-md-3 col-sm-12">
						<p><b>Renquired Source:</b>'; ?>
								<?php


								//echo $renquired["TimeStamp"];
								if (isset($renquired["Name"]) != '') {
									echo $renquired["Name"];
								} else {
									echo '';
								}

								?>

							<?php echo '</p>
					</div>';
							}
							echo '<div class="col-lg-12 col-md-3 col-sm-12">
							<p><b>Creation Date:</b>'; ?>
							<?php


							echo date("F j, Y g:i a", strtotime($row["Created_at"]));


							?>

						<?php echo '</p>
						</div>
				</div>
			</div>
			<div class="col-lg-1">
				<div class="row">
					<div class="col-lg-12 col-md-3 col-sm-12">
						<div class="btn-group">
							<span data-toggle="tooltip" data-placement="top" data-original-title="Send WhatsApp Message" title=""><p style="font-size: 20px;"><i class="fa fa-whatsapp whatsapp" style="cursor: pointer;" onclick="whatsApp(' . $row['ID'] . ');" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
							
							<span data-container="body" title="" data-toggle="popover" data-placement="bottom" onclick="callLeads(' . $row['ID'] . ');" data-content="" data-original-title=""><p style="font-size: 20px;"><i class="fa fa-phone" style="cursor: pointer;" aria-hidden="true"></i></p></span>&nbsp;&nbsp;&nbsp;&nbsp;
							<span data-toggle="dropdown"><p style="font-size: 20px;"><i class="fa fa-ellipsis-v re_hide" style="cursor: pointer;" aria-hidden="true"></i></p></span>
							<div class="dropdown-menu dropdown-menu-right" >
								<span class="dropdown-item"><i class="fas fa-notes-medical" style="font-size: 16px; color: #6C757D;"></i> <font class="addfollowupmodal" onclick="addFollowUp_ajax(' . $row['ID'] . ');" style="cursor: pointer;">Add Followup</font></span>';
								if ($_SESSION['useremployeeid'] != 'CV_2021_SVU01') {
								echo '<span class="dropdown-item"><i class="fas fa-user-edit" style="font-size: 16px; color: #6C757D;"></i> <font class="editlead" onclick="editLead(' . $row['ID'] . ');" style="cursor: pointer;">Edit Lead</font></span>';
								}
								echo '<span class="dropdown-item"><i class="fas fa-history" style="font-size: 16px; color: #6C757D;"></i> <font class="leadhistory" onclick="viewLeadHistory(' . $row['ID'] . ');" style="cursor: pointer;">View History</font></span>';
								if ($_SESSION['useremployeeid'] != 'CV_2021_SVU01') {
								echo '<span class="dropdown-item"><i class="fas fa-share-alt" style="font-size: 16px; color: #6C757D;"></i> <font class="referlead" onclick="referLead(' . $row['ID'] . ');" style="cursor: pointer;">Refer Lead</font></span>';
								echo '<span class="dropdown-item"><i class="fas fa-trash" style="font-size: 16px; color: #6C757D;"></i> <font class="deletelead" onclick="deleteLead(' . $row['ID'] . ');" style="cursor: pointer;">Delete</font></span>';
								}
							echo '</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</td>
</tr>';
						}
						?>
					</tbody>
				</table>

				<?php

				if ($totalPages > 1) {
					echo '<div id="pagination" class="candidates-list" style="float:right">
                                <span id="pagination_info" data-totalpages ="' . $totalPages . '"  data-currentpage ="' . $page . '">
                                    Page ' . $page . ' out of ' . $totalPages . '  Pages
                                </span>
                                <a href="javascript:void(0)" data-page="' . $page . '" id="pre" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" onClick="LeadFilter(`pre`)";>
                                    Previous
                                </a>
                                <a href="javascript:void(0)" id="next" style="background: #fff;padding: 5px 20px; border-radius: 2px; margin-left: 10px; color: #777;border: 1px solid #ddd;" data-page ="' . $page . '" onClick="LeadFilter(`next`)";>
                                    Next
                                </a>
</form>
								
								
</div>
						</div>
					</div>';
				}

				?>
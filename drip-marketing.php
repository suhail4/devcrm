<?php include 'filestobeincluded/header-top.php' ?>
<style type="text/css">
    .dataTables_filter {display: block !important;}
</style>
<?php include 'filestobeincluded/header-bottom.php' ?>
<?php include 'filestobeincluded/navigation.php' ?>
<script src="https://use.fontawesome.com/f4b83e121b.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<?php require 'filestobeincluded/db_config.php'; ?>

<?php

$all_recurring_rules = array();
$rr_query = $conn->query("SELECT * FROM Drip_Marketing ORDER BY Added_On DESC");
while($row = $rr_query->fetch_assoc()) {
  $all_recurring_rules[] = $row;
}

?>

<style type="text/css">
    .fa-times{
        font-weight: 400;
    font-size: 18px;
    border: 2px solid;
    border-radius: 5px;
    margin-top: 5px;
    }
    .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 15px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

.dependent-if-then-label {
    margin-top: 40px;
    text-align: center;
    color: #4a4a4a;
}
</style>
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="row page-title">
                        <div class="col-md-12">
                            <nav aria-label="breadcrumb" class="float-right mt-1">
                            	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addmarketingrule">Add Drip Marketing Rule</button>
                                <!---Rule Modal------>
                                    <div class="modal fade" id="addmarketingrule" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myCenterModalLabel">Add Rule</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                	<form id="dripForm" method="POST" action="ajax_marketing/drip-sql.php">
                                                    <p style="font-size: 12px; text-align:left;"><i>A new rule can be added using this dialog, you need to select Rules and actions to be performed based on the Rules</i></p>
                                                    
                                                        <div class="form-group row">
                                                            <div class="col-lg-6" style="padding-top: 20px;">
                                                                <input class="form-control" name="marketing_rule_name" type="text" id="marketing_rule_name" placeholder="Name of the Rule">
                                                            </div>
                                                            <div class="col-lg-3" style="padding-top: 20px;">
                                                                <input type="text" id="basic-datepicker" name="start_date" class="form-control" placeholder="Start Date">
                                                            </div>
                                                            <div class="col-lg-3" style="padding-top: 20px;">
                                                                <input type="text" name="start_time" id="basic-timepicker" class="form-control" placeholder="Start Time">
                                                            </div>
                                                        </div>
                                                        <ul class="nav nav-tabs">
                                                            <li class="nav-item">
                                                                <a href="#ifcondition" data-toggle="tab" aria-expanded="false"
                                                                    class="nav-link active">
                                                                    <span class="d-block d-sm-none">IF</span>
                                                                    <span class="d-none d-sm-block">IF</span>
                                                                </a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a href="#thencondition" data-toggle="tab" aria-expanded="true"
                                                                    class="nav-link">
                                                                    <span class="d-block d-sm-none">THEN</span>
                                                                    <span class="d-none d-sm-block">THEN</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content p-3 text-muted">
                                                        	
                                                            <div class="tab-pane show active" id="ifcondition">
                                                            	
                                                                <div class="row" style="padding-top: 20px; padding-bottom:20px;">
                                                                    <div class="col-lg-3">
                                                                        <ul class="nav nav-pills navtab-bg nav-justified">
                                                                            <li class="nav-item">
                                                                                <a href="#and" data-toggle="tab" id="and" aria-expanded="false"
                                                                                    class="nav-link">
                                                                                    
                                                                                    <span class="d-none d-sm-block" >AND</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="nav-item">
                                                                                <a href="#or" data-toggle="tab" id="or" aria-expanded="true"
                                                                                    class="nav-link active">
                                                                                    
                                                                                    <span class="d-none d-sm-block" >OR</span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <script>
                                                                    $('#and').click(function(){
                                                                        $('.operator').val('AND');
                                                                    });
                                                                    $('#or').click(function(){
                                                                        $('.operator').val('OR');
                                                                    });
                                                                </script>
                                                                

                                                                <div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                                    <div class="element" style="display: block;" id="ele">
                                                                        <div class="row" style="padding-top: 15px;">
                                                                            <div class="col-xl-3 col-sm-12">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select trigger_type" name="trigger_select[]" id="trigger_type_1" onclick="trigger(this.id,this.value)">
                                                                                        <option disabled selected>Select Trigger</option>
                                                                                        <option value="state">State</option>
                                                                                        <option value="course">Course</option>
                                                                                        <option value="university">University</option>
                                                                                        <option value="stage">Stage</option>
                                                                                        <option value="reason">Reason</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-3 col-sm-12" id="balance_select">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select" id="trigger_operator_1" name="balance_select[]">
                                                                                        <option disabled selected>Select Operator</option>
                                                                                        <option value="equalto">EqualsTo</option>
                                                                                        <option value="notequalto">NotEqualsTo</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-sm-12" id="data_select">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select trigger_show" id="trigger_show_1"  name="data_select[]">
                                                                                       
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-2 col-sm-12" id="more_buttons">
                                                                                <button type="button" class="btn btn-link remove" style="float: right;"><i class="fa fa-times" style="margin-top: 0px;"></i></button>
                                                                                <button type="button" class="btn btn-link clone" style="float: right;" onclick="addFunction();"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button>
                                                                            </div>
                                                                            
                                                                            
                                                                        </div>
                                                                         <div class="results" id="result">
                                                                    </div>
                                                                    </div>
                                                                   
                                                                    
                                                                    
                                                                </div>

                                                                <div id="append_div" style="margin-top: 10px;"></div>

                                                                
													
                                                       
                                                                

                                                            </div>



                                                       
                                                            <div class="tab-pane" id="thencondition">
                                                            	 
                                                                <div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                                                    <div class="element" style="display: block;" >
                                                                        <div class="row" style="padding-top: 15px;">
                                                                            <div class="col-xl-3 col-sm-12">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select trigger_type" name="lead_select[]" id="lead_1" onchange="Lead(this.id,this.value)">
                                                                                        <option disabled selected>Select Lead Attribute</option>
                                                                                        <option value="state">State</option>
                                                                                        <option value="course">Course</option>
                                                                                        <option value="university">University</option>
                                                                                        <option value="stage">Stage</option>
                                                                                        <option value="reason">Reason</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-3 col-sm-12" id="balance_select">
                                                                                <div class="form-group mt-3 mt-sm-0" style="text-align: center;">
                                                                                    <label>Should be</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-sm-12" id="data_select">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select lead_show" id="lead_show_1"  name="lead_data_select[]">
                                                                                       
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-2 col-sm-12" id="more_buttons">
                                                                                <!-- <button class="btn btn-link remove" style="float: right;"><i class="fa fa-times" style="margin-top: 0px;"></i></button> -->
                                                                                <button type="button" class="btn btn-link clone" style="float: right;" onclick="addLead();"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button>
                                                                            </div> 
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div id="then_lead_result">
                                                                    
                                                                </div>
                                                                <div style="margin-top: 10px;">
                                                                    <input type="text" disabled="" class="btn btn-primary btn-sm operator1" value="AND">
                                                                </div>
                                                                
                                                                <div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px; margin-top: 10px;">
                                                                    <div class="element" style="display: block;" >
                                                                        <div class="row" style="padding-top: 15px;">
                                                                            <div class="col-xl-3 col-sm-12">
                                                                                <div class="form-group mt-3 mt-sm-0">
                                                                                    <select class="form-control custom-select" name="communication_action[]" id="communication_action"  onchange="commAction(this.value)">
                                                                                        <option disabled selected>Select Communication Action</option>
                                                                                        <option value="Immediate">Immediate</option>
                                                                                        <!-- <option value="No. Of Occurences">No. Of Occurences</option> -->
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-sm-12" id="communi_number">
                                                                                
                                                                            </div>
                                                                            <div class="col-xl-4 col-sm-12" id="communi_count">
                                                                                
                                                                            </div>

                                                                    </div>


                                                                    <!-- SMS BLOCK Start-->
                                                                    <div class="row" id="sms_block" style="display: none;">
                                                                        <div class="col-md-12">
                                                                            <h4>Select SMS Templates</h4>
                                                                        </div>
                                                                        <div id="sms_contact" class="col-md-6" style="display: flex; margin-top: 10px;" >
                                                                            <label class="container">Primary Mobile
                                                                              <input name="sms_contact[]" class="sms_contact" type="checkbox" id="sms_primary" value="P" checked>
                                                                              <span class="checkmark"></span>
                                                                            </label>
                                                                            <label class="container">Alternate Mobile
                                                                              <input name="sms_contact[]" class="sms_contact" value="" type="checkbox" id="sms_alt" disabled>
                                                                              <span class="checkmark"></span>
                                                                            </label>
                                                                        </div>
                                                                        <div id="sms_communi">
                                                                        <div class="col-md-12" style="margin-top: 10px;">
																			<p>Communication Settings</p>
																		</div>
																		<div class="row" style="padding: 5px 14px;"><div class="col-md-4">
																			<select class="form-control custom-select sms_template" name="sms_template[]" id="selected_sms_template" disabled>
																				<option disabled selected>Select Template</option><?php $sql = $conn->query("SELECT ID,sms_template_name FROM SMS_Templates");while($result=mysqli_fetch_assoc($sql)){ ?><option value="<?php echo $result['ID']?>"><?php echo $result['sms_template_name']; ?>
																					
																				</option>
																			<?php }?>
																			</select>
																		</div>
																		<div class="col-md-4">
																			<select class="form-control custom-select sms_time" name="sms_time[]" id="sms_time" disabled>
																				<option disabled selected>Time Gap</option>
																				<option value="1">1 hr</option>
																				<option value="4">4 hrs</option>
																				<option value="8">8 hrs</option>
																				<option value="24">1 day</option>
																			</select>
																		</div>
																		</div>
                                                                        </div>
                                                                        </div>
                                                                         <!-- SMS BLOCK END-->
                                                                         
                                                                       
                                                                        <!-- EMAIL BLOCK Start-->
                                                                        <div class="row" id="email_block" style="display: none;">
                                                                            <div class="col-md-12">
                                                                            <h4>Select Email Templates</h4>
                                                                        </div>
                                                                        
                                                                       
                                                                        <div class="col-md-6" id="email_contact" style="display: flex; margin-top: 10px;">
                                                                            <label class="container">Primary Email
                                                                              <input name="email_contact[]" id="email_primary" type="checkbox" value="P" checked>
                                                                              <span class="checkmark"></span>
                                                                            </label>
                                                                        </div>
                                                                        <div id="email_communi">
                                                                        <div class="col-md-12" style="margin-top: 10px;">
																			<p>Communication Settings</p>
																		</div>
																		<div class="row" style="padding: 5px 14px;">
																			<div class="col-md-4">
																				<select class="form-control custom-select email_template" name="email_template[]" id="selected_email_template" disabled>
																					<option disabled selected>Select Template</option><?php $sql = $conn->query("SELECT ID,template_name FROM Email_Templates");while($result=mysqli_fetch_assoc($sql)){ ?>
																						<option value="<?php echo $result['ID']?>"><?php echo $result['template_name']; ?></option>
																						<?php }?>
																					</select>
																				</div>
																				<div class="col-md-4">
																					<select class="form-control custom-select email_time" name="email_time[]" id="email_time" disabled>
																						<option disabled selected>Time Gap</option>
																						<option value="1">1 hr</option>
																						<option value="4">4 hrs</option>
																						<option value="8">8 hrs</option>
																						<option value="24">1 day</option>
																					</select>
																				</div>
																			</div>
                                                                        </div>
                                                                        
                                                                        </div>
                                                                        <!-- EMAIL BLOCK END-->


                                                                        <!-- WHATSAPP BLOCK Start-->
                                                                        <div class="row" id="whatsapp_block" style="display: none;">
                                                                            <div class="col-md-12">
                                                                            <h4>Select WhatsApp Templates</h4>
                                                                        </div>
                                                                        
                                                                       
                                                                        <div id="whatsapp_contact" class="col-md-6" style="display: flex; margin-top: 10px;" >
                                                                            <label class="container">Primary Mobile
                                                                              <input name="whatsapp_contact[]" value="P" id="whatsapp_primary" type="checkbox" checked>
                                                                              <span class="checkmark"></span>
                                                                            </label>
                                                                            <label class="container">Alternate Mobile
                                                                              <input name="whatsapp_contact[]" value="" id="whatsapp_alt" type="checkbox" disabled>
                                                                              <span class="checkmark"></span>
                                                                            </label>
                                                                        </div>

                                                                        <div id="whatsapp_communi">
                                                                        <div class="col-md-12" style="margin-top: 10px;">
																				<p>Communication Settings</p>
																			</div>
																			<div class="row" style="padding: 5px 14px;">
																				<div class="col-md-4">
																					<select class="form-control custom-select whatsapp_template" id="selected_whatsapp_template" name="whatsapp_template[]" disabled>
																						<option disabled selected>Select Template</option>
																						<?php $sql = $conn->query("SELECT ID,wa_template_name FROM WhatsApp_Templates");
																						while($result=mysqli_fetch_assoc($sql)){ ?>
																							<option value="<?php echo $result['ID']?>"><?php echo $result['wa_template_name']; ?></option>
																							<?php }?>
																						</select>
																					</div>
																					<div class="col-md-4">
																						<select class="form-control custom-select whatsapp_time" name="whatsapp_time[]" id="whatsapp_time" disabled>
																							<option disabled selected>Time Gap</option>
																							<option value="1">1 hr</option>
																							<option value="4">4 hrs</option>
																							<option value="8">8 hrs</option>
																							<option value="24">1 day</option>
																						</select>
																					</div>
																				</div>
                                                                        </div>
                                                                        
                                                                        
                                                                        </div>

                                                                        <!-- WHATSAPP BLOCK END-->
                                                                    
                                                                </div>
                                                            </div>
                                                            
                                                        
                                                    
                                                </div>
                                                <div class="modal-footer">
											        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											        <button onclick="submitDrip()" type="button" class="btn btn-primary">Save changes</button>
											      </div>
											  </div>
                                                </form>
                                            </div><!-- /.modal-content -->


            						


                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                <!-----End Rule Modal-------->

                            </nav>
                            <h4 class="mb-1 mt-0">Drip Marketing</h4>
                        </div>
                    </div>
                </div> <!-- container-fluid -->

                <div class="modal fade" id="addreccurringrule" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel1" aria-hidden="true">
                	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                		<div class="modal-content">
                			<div class="modal-header">
                				<h5 class="modal-title" id="myCenterModalLabel1">Add Recurring Communication Rule</h5>
                				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                					<span aria-hidden="true">&times;</span>
                				</button>
                			</div>
                			<div class="modal-body">
                				<form id="recurringForm" method="POST" action="ajax_marketing/recurring-sql.php">
                					<p style="font-size: 12px; text-align:left;"><i>A new rule can be added using this dialog, you need to select stage, subdisposition and actions to be performed based on the rule</i></p>

                					<div class="form-group row">
                                        <div class="col-lg-6" style="padding-top: 20px;">
                                            <input class="form-control" name="recurring_rule_name" type="text" id="recurring_rule_name" placeholder="Name of the Rule">
                                        </div>
                                        <div class="col-lg-3" style="padding-top: 20px;">
                                            <select class="form-control custom-select" id="recurring_time_gap" name="recurring_time_gap">
                                              <option disabled selected>Day Gap</option>
                                              <option value="1">1 day</option>
                                              <option value="3">3 days</option>
                                              <option value="5">5 days</option>
                                              <option value="10">10 days</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3" style="padding-top: 20px;">
                                        	<input class="form-control" name="recurring_time" type="time" id="recurring_time" placeholder="Send Time">
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top: 15px;">
                                        <div class="col-xl-3 col-sm-12">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select trigger_type" name="stage_select" id="stage_select">
                                                    <option disabled selected>Select Stage</option>
                                                    <?php
                                                    $stage_query = $conn->query("SELECT * FROM Stages");
                                                    while($row = $stage_query->fetch_assoc()) {
                                                    	?>
                                                    	<option value="<?php echo $row['ID'] ?>"><?php echo $row['Name'] ?></option>
                                                    	<?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-12">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select" id="reason_select" name="reason_select">
                                                    <option disabled selected>Select Reason</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-xl-4 col-sm-12">
                                        	<div class="form-group mt-3 mt-sm-0">
                                        		<select class="form-control custom-select" name="recurring_comm_action" multiple id="recurring_comm_action" onchange="recurringCommMode(this);">
                                        			<option value="SMS">SMS</option>
                                        			<option value="Email">Email</option>
                                        		</select>
                                        	</div>
                                        </div>
                                    </div>

                                    <div class="row" id="recurring_sms_block" style="display: none;">
                                        <div class="col-md-12">
                                            <h4>Select SMS Templates</h4>
                                        </div>
                                        <div id="sms_contact" class="col-md-6" style="display: flex; margin-top: 10px;" >
                                            <label class="container">Primary Mobile
                                              <input name="sms_contact[]" class="sms_contact" type="checkbox" id="recurring_sms_primary" value="P" checked>
                                              <span class="checkmark"></span>
                                            </label>
                                            <label class="container">Alternate Mobile
                                              <input name="sms_contact[]" class="sms_contact" value="" type="checkbox" id="recurring_sms_alt" disabled>
                                              <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div id="recurring_sms_communi">
                                        <div class="col-md-12" style="margin-top: 10px;">
											<p>Communication Settings</p>
										</div>
										<div class="row" style="padding: 5px 14px;"><div class="col-md-4">
											<select class="form-control custom-select sms_template" name="sms_template[]" id="recurring_selected_sms_template" disabled>
												<option disabled selected>Select Template</option><?php $sql = $conn->query("SELECT ID,sms_template_name FROM SMS_Templates");while($result=mysqli_fetch_assoc($sql)){ ?><option value="<?php echo $result['ID']?>"><?php echo $result['sms_template_name']; ?>
													
												</option>
											<?php }?>
											</select>
										</div>
										</div>
                                        </div>
                                        </div>
                                         <!-- SMS BLOCK END-->
                                         
                                       
                                        <!-- EMAIL BLOCK Start-->
                                        <div class="row" id="recurring_email_block" style="display: none;">
                                            <div class="col-md-12">
                                            <h4>Select Email Templates</h4>
                                        </div>
                                        
                                       
                                        <div class="col-md-6" id="recurring_email_contact" style="display: flex; margin-top: 10px;">
                                            <label class="container">Primary Email
                                              <input name="email_contact[]" id="recurring_email_primary" type="checkbox" value="P" checked>
                                              <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div id="recurring_email_communi">
                                        <div class="col-md-12" style="margin-top: 10px;">
											<p>Communication Settings</p>
										</div>
										<div class="row" style="padding: 5px 14px;">
											<div class="col-md-4">
												<select class="form-control custom-select email_template" name="email_template[]" id="recurring_selected_email_template" disabled>
													<option disabled selected>Select Template</option><?php $sql = $conn->query("SELECT ID,template_name FROM Email_Templates");while($result=mysqli_fetch_assoc($sql)){ ?>
														<option value="<?php echo $result['ID']?>"><?php echo $result['template_name']; ?></option>
														<?php }?>
													</select>
												</div>
											</div>
                                        </div>
                                        
                                        </div>
                                        <!-- EMAIL BLOCK END-->

                                        <div class="modal-footer">
					                		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					                		<button onclick="submitReccu()" type="button" class="btn btn-primary">Save changes</button>
					                	</div>
                                </form>
                			</div>
                		</div>
                	</div>
                </div>

                <div class="row" style="margin-left: 10px; margin-right: 10px;">
                  <div class="col-lg-12 card card-body">
                    <div class="table-responsive">
                    <table id="basic-datatable" class="table table-hover mb-0">
                      <thead>
                          <tr>
                          <th scope="col">#</th>
                          <th scope="col">Rule_Name</th>
                          <th scope="col">Trigger Time</th>
                          <th scope="col">If Condition</th>
                          <th scope="col">Communication Modes</th>
                          <th scope="col">Actions</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                         $counter = 0;
                         foreach ($all_recurring_rules as $recurring_rule) {
                          $counter++;
                          ?>
                          <tr>
                          <th scope="row"><?php echo $counter; ?></th>
                          <td><?php echo $recurring_rule['Name']; ?></td>
                          <td><?php echo date("d-M-y g:i a", strtotime($recurring_rule['Start_Timestamp'])); ?></td>
                          <td><?php 

                          	if(strpos($recurring_rule['If_Statements'], "&&") !== false) {
                          		$new_statement = str_replace("&&", "AND", $recurring_rule['If_Statements']);

                          		$conditions = explode("AND", $new_statement);
                          		$final_statement = '';

                          		foreach ($conditions as $condition) {
                          			if(strpos($condition, "university") !== false) {
                          				$uni_id = trim(explode("=", $condition)[1]);

                          				$uni_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$uni_id."'");
                          				$uni_dets = mysqli_fetch_assoc($uni_query);
                          				$uni_name = $uni_dets['Name'];

                          				$condition = "University -> <b>".$uni_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>AND</b><br>";
                          			}

                          			if(strpos($condition, "course") !== false) {
                          				$course_id = trim(explode("=", $condition)[1]);

                          				$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$course_id."'");
                                  if($course_query->num_rows > 0) {
                                    $course_dets = mysqli_fetch_assoc($course_query);
                                    $course_name = $course_dets['Name'];

                                    $condition = "Course -> <b>".$course_name."</b>";

                                    $final_statement = $final_statement.$condition."<br><b>AND</b><br>";
                                  }
                          			}

                          			if(strpos($condition, "stage") !== false) {
                          				$stage_id = trim(explode("=", $condition)[1]);

                          				$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$stage_id."'");
                          				$stage_dets = mysqli_fetch_assoc($stage_query);
                          				$stage_name = $stage_dets['Name'];

                          				$condition = "Stage -> <b>".$stage_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>AND</b><br>";
                          			}

                          			if(strpos($condition, "reason") !== false) {
                          				$reason_id = trim(explode("=", $condition)[1]);

                          				$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$reason_id."'");
                          				$reason_dets = mysqli_fetch_assoc($reason_query);
                          				$reason_name = $reason_dets['Name'];

                          				$condition = "Reason -> <b>".$reason_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>AND</b><br>";
                          			}

                          			if(strpos($condition, "state") !== false) {
                          				$state_id = trim(explode("=", $condition)[1]);

                          				$reason_query = $conn->query("SELECT * FROM States WHERE ID = '".$state_id."'");
                          				$state_dets = mysqli_fetch_assoc($reason_query);
                          				$state_name = $state_dets['Name'];

                          				$condition = "State -> <b>".$state_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>AND</b><br>";
                          			}
                          		}
                          		echo substr($final_statement, 0, strlen($final_statement) - 19);
                          	}
                          	else {
                          		$new_statement = str_replace("||", "OR", $recurring_rule['If_Statements']);

                          		$conditions = explode("OR", $new_statement);
                          		$final_statement = '';

                          		foreach ($conditions as $condition) {
                          			if(strpos($condition, "university") !== false) {
                          				$uni_id = trim(explode("=", $condition)[1]);

                          				$uni_query = $conn->query("SELECT * FROM Institutes WHERE ID = '".$uni_id."'");
                          				$uni_dets = mysqli_fetch_assoc($uni_query);
                          				$uni_name = $uni_dets['Name'];

                          				$condition = "University -> <b>".$uni_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>OR</b><br>";
                          			}

                          			if(strpos($condition, "course") !== false) {
                          				$course_id = trim(explode("=", $condition)[1]);

                          				$course_query = $conn->query("SELECT * FROM Courses WHERE ID = '".$course_id."'");
                          				$course_dets = mysqli_fetch_assoc($course_query);
                          				$course_name = $course_dets['Name'];

                          				$condition = "Course -> <b>".$course_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>OR</b><br>";
                          			}

                          			if(strpos($condition, "stage") !== false) {
                          				$stage_id = trim(explode("=", $condition)[1]);

                          				$stage_query = $conn->query("SELECT * FROM Stages WHERE ID = '".$stage_id."'");
                          				$stage_dets = mysqli_fetch_assoc($stage_query);
                          				$stage_name = $stage_dets['Name'];

                          				$condition = "Stage -> <b>".$stage_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>OR</b><br>";
                          			}

                          			if(strpos($condition, "reason") !== false) {
                          				$reason_id = trim(explode("=", $condition)[1]);

                          				$reason_query = $conn->query("SELECT * FROM Reasons WHERE ID = '".$reason_id."'");
                          				$reason_dets = mysqli_fetch_assoc($reason_query);
                          				$reason_name = $reason_dets['Name'];

                          				$condition = "Reason -> <b>".$reason_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>OR</b><br>";
                          			}

                          			if(strpos($condition, "state") !== false) {
                          				$state_id = trim(explode("=", $condition)[1]);

                          				$reason_query = $conn->query("SELECT * FROM States WHERE ID = '".$state_id."'");
                          				$state_dets = mysqli_fetch_assoc($reason_query);
                          				$state_name = $state_dets['Name'];

                          				$condition = "State -> <b>".$state_name."</b>";

                          				$final_statement = $final_statement.$condition."<br><b>OR</b><br>";
                          			}
                          		}
                          		echo substr($final_statement, 0, strlen($final_statement) - 18);
                          	}

                           ?></td>
                          <td>
                            <?php
                            if(strpos($recurring_rule['Comm_Modes'], 'EMAIL') !== false && strpos($recurring_rule['Comm_Modes'], 'SMS') !== false) {
                              $sms_statement = explode("@", $recurring_rule['Comm_Modes'])[0];
                              $email_statement = explode("@", $recurring_rule['Comm_Modes'])[0];

                              $sms_temp_id = explode("~", $sms_statement)[1];
                              $sms_query = $conn->query("SELECT * FROM SMS_Templates WHERE ID = '".$sms_temp_id."'");
                              $sms_dets = mysqli_fetch_assoc($sms_query);

                              $email_temp_id = explode("~", $email_statement)[1];
                              $email_query = $conn->query("SELECT * FROM Email_Templates WHERE ID = '".$email_temp_id."'");
                              $email_dets = mysqli_fetch_assoc($email_query);

                              echo "SMS -> <b>".$sms_dets['sms_template_name']."</b>";
                              echo "</br>";
                              echo "EMAIL -> <b>".$email_dets['template_name']."</b>";
                            }
                            else if(strpos($recurring_rule['Comm_Modes'], 'EMAIL') !== false) {
                              $email_temp_id = explode("~", $recurring_rule['Comm_Modes'])[1];

                              $email_query = $conn->query("SELECT * FROM Email_Templates WHERE ID = '".$email_temp_id."'");
                              $email_dets = mysqli_fetch_assoc($email_query);

                              echo "EMAIL -> <b>".$email_dets['template_name']."</b>";
                            }
                            else if(strpos($recurring_rule['Comm_Modes'], 'SMS') !== false) {
                              $sms_temp_id = explode("~", $recurring_rule['Comm_Modes'])[1];

                              $sms_query = $conn->query("SELECT * FROM SMS_Templates WHERE ID = '".$sms_temp_id."'");
                              $sms_dets = mysqli_fetch_assoc($sms_query);

                              echo "SMS -> <b>".$sms_dets['sms_template_name']."</b>";
                            }

                            ?>
                          </td>
                          <td>
                          	<i class="fa fa-edit" style="cursor: pointer;" aria-hidden="true" onclick="editRR('<?php echo $recurring_rule['ID'];?>');"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                          	<i class="fa fa-trash" onclick="deleteRR('<?php echo $recurring_rule['ID'];?>');"  style="cursor: pointer;"></i>
                          </td>
                        </tr>
                          <?php
                         }
                        ?>
                      </tbody>
                    </table>
                  </div>
                  </div>
                </div>

        <script>
          function editRR(id){
            $.ajax({
              url:'ajax_marketing/edit_modal.php',
              type:'POST',
              data:{id:id},
              success: function(data){
                $('#edit_rule_body').html(data); 
                $('#edit_rule').modal('show');
              },
            })
            
          }
        </script>

        <script>
          function deleteRR(id){
            $.ajax({
              url:'ajax_marketing/delete_modal.php',
              type:'POST',
              data:{id:id},
              success: function(data){
                $('#delete_rule_body').html(data); 
                $('#delete_rule').modal('show');
              }
            })
            
          }
        </script>


<!---Rule Modal------>
<div class="modal fade" id="edit_rule" tabindex="-1" role="dialog" aria-labelledby="myEditCenterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myEditCenterModalLabel">Edit Rule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="edit_rule_body">
            	
            </div>
        </div><!-- /.modal-dialog -->
    s</div>
</div>


<!-------Delete Lead----->
<div class="modal fade" id="delete_rule" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myCenterModalLabel">Are you sure want to delete?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="delete_rule_body">
				
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--------End Delete Lead---------->

        <script>
          function submitReccu() {
            if($("#recurringForm").valid()) {
              var recurring_rule_name = $('#recurring_rule_name').val();
              var recurring_time_gap = $('#recurring_time_gap').val();
              var recurring_time = $('#recurring_time').val();
              var stage = $('#stage_select').val();
              var reason = $('#reason_select').val();
              var communication_mode = $('#recurring_comm_action').val();

              var sms_ok, email_ok = false;
              var comm_mode_final_text = "";

              if(communication_mode.includes("SMS")) {
                var sms_template = $("#recurring_selected_sms_template").val();
                var sms_contacts = $("#recurring_sms_primary").val()+$("#recurring_sms_alt").val();

                comm_mode_final_text = comm_mode_final_text+"SMS"+"~"+sms_template+"~"+sms_contacts+"@";

                if(sms_template == null) {
                  toastr.error("Please select an SMS Template");
                }
                else {
                  sms_ok = true;
                }
              }
              else {
                sms_ok = true;
              }

              if(communication_mode.includes("Email")) {
                var email_template = $("#recurring_selected_email_template").val();
                var email_contact = $("#recurring_email_primary").val();

                comm_mode_final_text = comm_mode_final_text+"EMAIL"+"~"+email_template+"~"+email_contact+"@";

                if(email_template == null) {
                  toastr.error("Please select an Email Template");
                }
                else {
                  email_ok = true;
                }
              }
              else {
                email_ok = true;
              }

              if(sms_ok && email_ok) {
                if(email_contact === '') {
                  toastr.error("Primary Email is required for Email");
                }
                if(sms_contacts === '') {
                  toastr.error("Please select atleast one number for SMS");
                }
                if(email_contact != '' && sms_contacts != '') {
                  comm_mode_final_text = comm_mode_final_text.substring(0, comm_mode_final_text.length-1);

                  $.ajax({
                    url: 'ajax_cron/addCron.php',
                    type: 'post',
                    data: {"Name": recurring_rule_name, "TimeGap": recurring_time_gap, "StartTime": recurring_time, "Stage": stage, "Reason": reason, "finalCommModes": comm_mode_final_text},
                    success: function(response){ 
                      console.log(response);
                      if(response.match("true")) {
                        toastr.success("Rule added successfully");
                        location.reload();
                      }
                      else {
                        toastr.error("Unable to add rule");
                      }
                    }
                  });
                }


              }
            }
          }
        </script>

        <script type="text/javascript">
          $(document).ready(function() {
            $("#recurringForm").validate({
              rules: {
                recurring_rule_name: "required",
                recurring_time_gap: "required",
                recurring_time: "required",
                stage_select: "required",
                reason_select: "required",
                recurring_comm_action: "required"
              },

              messages: {
                recurring_rule_name: "Please enter rule name",
                recurring_time_gap: "Please select time gap",
                recurring_time: "Please enter send time",
                reason_select: "Please select a reason",
                recurring_comm_action: "Please select atleast one communication mode"
              },

              highlight: function (element) {
                $(element).addClass('error');
                $(element).closest('.form-control').addClass('has-error');
                $(element).closest('.small').addClass('has-error');
              },

              unhighlight: function (element) {
                $(element).removeClass('error');
                $(element).closest('.form-control').removeClass('has-error');
                $(element).closest('.small').removeClass('has-error');
              }
            });
          });
        </script>

        <script>
					$(document).ready(function() {
						$('#stage_select').change(function() {

							$('#reason_select').html("<option disabled selected>Select Reason</option>");

							var selected_stage = $('#stage_select').val();
							$.ajax
							({
								type: "POST",
								url: "onselect/onSelect.php",
								data: { "stage_select": "", "selected_stage": selected_stage },
								success: function(data) {
									if(data != "") {
										$('#reason_select').html("<option value='all'>All</option>"+data);
									}
									else {
										$('#reason_select').html("<option disabled selected>Select Reason</option>");
									}
								}
							});
						});
					});
				</script>

                <script>
                	$(document).ready(function() {
                		$('#recurring_comm_action').select2({
                			placeholder: "Select Communication Modes"
                		});
                	});
                </script>

              <script type="text/javascript">
              	function recurringCommMode(sel) {
                  var opt;
                  var opts = [];
                  var len = sel.options.length;
                  for (var i = 0; i < len; i++) {
                    opt = sel.options[i];

                    if (opt.selected) {
                      opts.push(opt.value);
                  	}
                  }
                  		//console.log(opts);
                      if(opts.includes("Email")) {
                      	$('#recurring_email_block').css('display','block');
                        $('#recurring_email_primary').prop("disabled", false);
                        $('#recurring_email_alt').prop("disabled", false);
                        $('.email_template').prop("disabled", false);
                        $('.email_time').prop("disabled", false);
                      }
                      else {
                      	$('#recurring_email_block').css('display', 'none');
                        $('#recurring_email_primary').prop("disabled", true);
                        $('#recurring_email_alt').prop("disabled", true);
                        $('.email_template').prop("disabled", true);
                        $('.email_time').prop("disabled", true);
                      }
                      
                      if(opts.includes("SMS")) {
                      	$('#recurring_sms_block').css('display','block');
                        $('#recurring_sms_primary').prop("disabled", false);
                        $('#recurring_sms_alt').prop("disabled", false);
                        $('.sms_template').prop("disabled", false);
                        $('.sms_time').prop("disabled", false);

                      }
                      else {
                      	$('#recurring_sms_block').css('display', 'none');
                        $('#recurring_sms_primary').prop("disabled", true);
                        $('#recurring_sms_alt').prop("disabled", true);
                        $('.sms_template').prop("disabled", true);
                        $('.sms_time').prop("disabled", true);
                      }
                    }
              </script>


                <script type="text/javascript">
                
            </script>

            <script>
              $(document).ready(function() {
                $('#recurring_sms_primary').change(function() {
                  if($('#recurring_sms_primary').is(":checked")){
                    $("#recurring_sms_primary").val('P');
                  }
                  else {
                    $("#recurring_sms_primary").val('');
                  }
                });

                $('#recurring_sms_alt').change(function() {
                  if($('#recurring_sms_alt').is(":checked")){
                    $("#recurring_sms_alt").val('A');
                  }
                  else {
                    $("#recurring_sms_alt").val('');
                  }
                });

                $('#recurring_email_primary').change(function() {
                  if($('#recurring_email_primary').is(":checked")){
                    $("#recurring_email_primary").val('P');
                  }
                  else {
                    $("#recurring_email_primary").val('');
                  }
                });
              });
            </script>

            <script>
              $(document).ready(function() {
                $('#sms_primary').change(function() {
                  if($('#sms_primary').is(":checked")){
                    $("#sms_primary").val('P');
                  }
                  else {
                    $("#sms_primary").val('');
                  }
                });

                $('#sms_alt').change(function() {
                  if($('#sms_alt').is(":checked")){
                    $("#sms_alt").val('A');
                  }
                  else {
                    $("#sms_alt").val('');
                  }
                });

                $('#email_primary').change(function() {
                  if($('#email_primary').is(":checked")){
                    $("#email_primary").val('P');
                  }
                  else {
                    $("#email_primary").val('');
                  }
                });
              });
            </script>

            <script>
              $(document).ready(function() {
                $('#edit_sms_primary').change(function() {
                  if($('#edit_sms_primary').is(":checked")){
                    $("#edit_sms_primary").val('P');
                  }
                  else {
                    $("#edit_sms_primary").val('');
                  }
                });

                $('#edit_sms_alt').change(function() {
                  if($('#edit_sms_alt').is(":checked")){
                    $("#edit_sms_alt").val('A');
                  }
                  else {
                    $("#edit_sms_alt").val('');
                  }
                });

                $('#edit_email_primary').change(function() {
                  if($('#edit_email_primary').is(":checked")){
                    $("#edit_email_primary").val('P');
                  }
                  else {
                    $("#edit_email_primary").val('');
                  }
                });
              });
            </script>




            </div> <!-- content -->

            

<?php include 'javascript.php' ?>
<?php include 'filestobeincluded/footer-top.php' ?>
<?php include 'filestobeincluded/footer-bottom.php' ?>
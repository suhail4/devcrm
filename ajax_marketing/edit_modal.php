<?php 
if(session_status() === PHP_SESSION_NONE) session_start();

require '../filestobeincluded/db_config.php';
$rule_id = $_POST['id'];

$_SESSION['rule_id_to_update'] = $rule_id;

$drip_query = $conn->query("SELECT * FROM Drip_Marketing WHERE ID = '".$rule_id."'");
$rule_dets = mysqli_fetch_assoc($drip_query);

?>

<form id="editDripForm" method="POST" action="ajax_marketing/drip-sql.php">
                <p style="font-size: 12px; text-align:left;"><i>A new rule can be added using this dialog, you need to select Rules and actions to be performed based on the Rules</i></p>
                
                    <div class="form-group row">
                        <div class="col-lg-6" style="padding-top: 20px;">
                            <input class="form-control" name="edit_marketing_rule_name" value="<?php echo $rule_dets['Name']; ?>" type="text" id="edit_marketing_rule_name" placeholder="Name of the Rule">
                        </div>
                        <div class="col-lg-3" style="padding-top: 20px;">
                            <input type="text" id="edit_basic-datepicker" name="edit_start_date" value="<?php echo explode(" ",$rule_dets['Added_On'])[0]; ?>" class="form-control" placeholder="Start Date">
                        </div>
                        <div class="col-lg-3" style="padding-top: 20px;">
                            <input type="text" name="edit_start_time" id="edit_basic-timepicker" value="<?php echo explode(" ",$rule_dets['Added_On'])[1]; ?>" class="form-control" placeholder="Start Time">
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a href="#edit_ifcondition" data-toggle="tab" aria-expanded="false"
                                class="nav-link active">
                                <span class="d-block d-sm-none">IF</span>
                                <span class="d-none d-sm-block">IF</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#edit_thencondition" data-toggle="tab" aria-expanded="true"
                                class="nav-link">
                                <span class="d-block d-sm-none">THEN</span>
                                <span class="d-none d-sm-block">THEN</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content p-3 text-muted">
                    	
                        <div class="tab-pane show active" id="edit_ifcondition">
                        	
                            <div class="row" style="padding-top: 20px; padding-bottom:20px;">
                                <div class="col-lg-3">
                                    <ul class="nav nav-pills navtab-bg nav-justified">
                                        <li class="nav-item">
                                            <a href="#edit_and" data-toggle="tab" id="edit_and" aria-expanded="false"
                                                class="nav-link">
                                                
                                                <span class="d-none d-sm-block" >AND</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#edit_or" data-toggle="tab" id="edit_or" aria-expanded="true"
                                                class="nav-link active">
                                                
                                                <span class="d-none d-sm-block" >OR</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <script>
                                $('#edit_and').click(function(){
                                    $('.operator').val('AND');
                                });
                                $('#edit_or').click(function(){
                                    $('.operator').val('OR');
                                });
                            </script>
                            

                            <div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                <div class="element" style="display: block;" id="edit_ele">
                                    <div class="row" style="padding-top: 15px;">
                                        <div class="col-xl-3 col-sm-12">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select trigger_type" name="trigger_select[]" id="edit_trigger_type_1" onclick="edit_trigger(this.id,this.value)">
                                                    <option disabled selected>Select Trigger</option>
                                                    <option value="state">State</option>
                                                    <option value="course">Course</option>
                                                    <option value="university">University</option>
                                                    <option value="stage">Stage</option>
                                                    <option value="reason">Reason</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-12" id="edit_balance_select">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select" id="edit_trigger_operator_1" name="balance_select[]">
                                                    <option disabled selected>Select Operator</option>
                                                    <option value="equalto">EqualsTo</option>
                                                    <option value="notequalto">NotEqualsTo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-sm-12" id="edit_data_select">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select trigger_show" id="edit_trigger_show_1"  name="data_select[]">
                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-sm-12" id="edit_more_buttons">
                                            <button type="button" class="btn btn-link remove" style="float: right;"><i class="fa fa-times" style="margin-top: 0px;"></i></button>
                                            <button type="button" class="btn btn-link clone" style="float: right;" onclick="editFunction();"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button>
                                        </div>
                                        
                                        
                                    </div>
                                     <div class="results" id="edit_result">
                                </div>
                                </div>
                               
                                
                                
                            </div>

                            <div id="edit_append_div" style="margin-top: 10px;"></div>

                            
				
                   
                            

                        </div>



                   
                        <div class="tab-pane" id="edit_thencondition">
                        	 
                            <div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px;">
                                <div class="element" style="display: block;" >
                                    <div class="row" style="padding-top: 15px;">
                                        <div class="col-xl-3 col-sm-12">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select trigger_type" name="lead_select[]" id="edit_lead_1" onchange="editLead(this.id,this.value)">
                                                    <option disabled selected>Select Lead Attribute</option>
                                                    <option value="state">State</option>
                                                    <option value="course">Course</option>
                                                    <option value="university">University</option>
                                                    <option value="stage">Stage</option>
                                                    <option value="reason">Reason</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-12" id="edit_balance_select">
                                            <div class="form-group mt-3 mt-sm-0" style="text-align: center;">
                                                <label>Should be</label>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-sm-12" id="edit_data_select">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select lead_show" id="edit_lead_show_1"  name="lead_data_select[]">
                                                   
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-sm-12" id="edit_more_buttons">
                                            <!-- <button class="btn btn-link remove" style="float: right;"><i class="fa fa-times" style="margin-top: 0px;"></i></button> -->
                                            <button type="button" class="btn btn-link clone" style="float: right;" onclick="editLead();"><i class="fa fa-plus-square-o" style="font-weight: 500;font-size: 25px;"></i></button>
                                        </div> 
                                    </div>
                                    
                                </div>
                            </div>
                            <div id="edit_then_lead_result">
                                
                            </div>
                            <div style="margin-top: 10px;">
                                <input type="text" disabled="" class="btn btn-primary btn-sm operator1" value="AND">
                            </div>
                            
                            <div class="wrapper" style="border: 1px solid #e6e6e6; border-radius:6px; padding: 10px; margin-top: 10px;">
                                <div class="element" style="display: block;" >
                                    <div class="row" style="padding-top: 15px;">
                                        <div class="col-xl-3 col-sm-12">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <select class="form-control custom-select" name="communication_action[]" id="edit_communication_action"  onchange="editcommAction(this.value)">
                                                    <option disabled selected>Select Communication Action</option>
                                                    <option value="Immediate">Immediate</option>
                                                    <!-- <option value="No. Of Occurences">No. Of Occurences</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-sm-12" id="edit_communi_number">
                                            
                                        </div>
                                        <div class="col-xl-4 col-sm-12" id="edit_communi_count">
                                            
                                        </div>

                                </div>


                                <!-- SMS BLOCK Start-->
                                <div class="row" id="edit_sms_block" style="display: none;">
                                    <div class="col-md-12">
                                        <h4>Select SMS Templates</h4>
                                    </div>
                                    <div id="edit_sms_contact" class="col-md-6" style="display: flex; margin-top: 10px;" >
                                        <label class="container">Primary Mobile
                                          <input name="sms_contact[]" class="sms_contact" type="checkbox" id="edit_sms_primary" value="P" checked>
                                          <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Alternate Mobile
                                          <input name="sms_contact[]" class="sms_contact" value="" type="checkbox" id="sms_alt" >
                                          <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div id="edit_sms_communi">
                                    <div class="col-md-12" style="margin-top: 10px;">
										<p>Communication Settings</p>
									</div>
									<div class="row" style="padding: 5px 14px;"><div class="col-md-4">
										<select class="form-control custom-select sms_template" name="sms_template[]" id="edit_selected_sms_template">
											<option disabled selected>Select Template</option><?php $sql = $conn->query("SELECT ID,sms_template_name FROM SMS_Templates");while($result=mysqli_fetch_assoc($sql)){ ?><option value="<?php echo $result['ID']?>"><?php echo $result['sms_template_name']; ?>
												
											</option>
										<?php }?>
										</select>
									</div>
									<div class="col-md-4">
										<select class="form-control custom-select sms_time" name="sms_time[]" id="edit_sms_time">
											<option disabled selected>Time Gap</option>
											<option value="1">1 hr</option>
											<option value="4">4 hrs</option>
											<option value="8">8 hrs</option>
											<option value="24">1 day</option>
										</select>
									</div>
									</div>
                                    </div>
                                    </div>
                                     <!-- SMS BLOCK END-->
                                     
                                   
                                    <!-- EMAIL BLOCK Start-->
                                    <div class="row" id="edit_email_block" style="display: none;">
                                        <div class="col-md-12">
                                        <h4>Select Email Templates</h4>
                                    </div>
                                    
                                   
                                    <div class="col-md-6" id="edit_email_contact" style="display: flex; margin-top: 10px;">
                                        <label class="container">Primary Email
                                          <input name="email_contact[]" id="email_primary" type="checkbox" value="P" checked>
                                          <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div id="edit_email_communi">
                                    <div class="col-md-12" style="margin-top: 10px;">
										<p>Communication Settings</p>
									</div>
									<div class="row" style="padding: 5px 14px;">
										<div class="col-md-4">
											<select class="form-control custom-select email_template" name="email_template[]" id="edit_selected_email_template" disabled>
												<option disabled selected>Select Template</option><?php $sql = $conn->query("SELECT ID,template_name FROM Email_Templates");while($result=mysqli_fetch_assoc($sql)){ ?>
													<option value="<?php echo $result['ID']?>"><?php echo $result['template_name']; ?></option>
													<?php }?>
												</select>
											</div>
											<div class="col-md-4">
												<select class="form-control custom-select email_time" name="email_time[]" id="edit_email_time" disabled>
													<option disabled selected>Time Gap</option>
													<option value="1">1 hr</option>
													<option value="4">4 hrs</option>
													<option value="8">8 hrs</option>
													<option value="24">1 day</option>
												</select>
											</div>
										</div>
                                    </div>
                                    
                                    </div>
                                    <!-- EMAIL BLOCK END-->


                                    <!-- WHATSAPP BLOCK Start-->
                                    <div class="row" id="edit_whatsapp_block" style="display: none;">
                                        <div class="col-md-12">
                                        <h4>Select WhatsApp Templates</h4>
                                    </div>
                                    
                                   
                                    <div id="edit_whatsapp_contact" class="col-md-6" style="display: flex; margin-top: 10px;" >
                                        <label class="container">Primary Mobile
                                          <input name="whatsapp_contact[]" value="P" id="edit_whatsapp_primary" type="checkbox" checked>
                                          <span class="checkmark"></span>
                                        </label>
                                        <label class="container">Alternate Mobile
                                          <input name="whatsapp_contact[]" value="" id="edit_whatsapp_alt" type="checkbox" disabled>
                                          <span class="checkmark"></span>
                                        </label>
                                    </div>

                                    <div id="edit_whatsapp_communi">
                                    <div class="col-md-12" style="margin-top: 10px;">
											<p>Communication Settings</p>
										</div>
										<div class="row" style="padding: 5px 14px;">
											<div class="col-md-4">
												<select class="form-control custom-select whatsapp_template" id="edit_selected_whatsapp_template" name="whatsapp_template[]" disabled>
													<option disabled selected>Select Template</option>
													<?php $sql = $conn->query("SELECT ID,wa_template_name FROM WhatsApp_Templates");
													while($result=mysqli_fetch_assoc($sql)){ ?>
														<option value="<?php echo $result['ID']?>"><?php echo $result['wa_template_name']; ?></option>
														<?php }?>
													</select>
												</div>
												<div class="col-md-4">
													<select class="form-control custom-select whatsapp_time" name="whatsapp_time[]" id="edit_whatsapp_time" disabled>
														<option disabled selected>Time Gap</option>
														<option value="1">1 hr</option>
														<option value="4">4 hrs</option>
														<option value="8">8 hrs</option>
														<option value="24">1 day</option>
													</select>
												</div>
											</div>
                                    </div>
                                    
                                    
                                    </div>

                                    <!-- WHATSAPP BLOCK END-->
                                
                            </div>
                        </div>
                        
                    
                
            </div>
            <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button onclick="editDrip()" type="button" class="btn btn-primary">Save changes</button>
		      </div>
		  </div>
            </form>
        </div><!-- /.modal-content -->
<?php 
    include '../filestobeincluded/db_config.php';
    if(session_status() === PHP_SESSION_NONE) session_start();
    
    if(!empty($_POST['stage'])){
        $stage = $_POST['stage'];
        $get_stage_id = $conn->query("SELECT ID FROM Stages WHERE Name like '$stage'");
        $gsi = mysqli_fetch_assoc($get_stage_id);
        $stage_query = "AND Leads.Stage_ID = '".$gsi['ID']."'";
    }else{
        $stage_query = ' ';
    }

    if(!empty($_POST['counsellor'])){
        $couns = $_POST['counsellor'];
        $exp = explode('(', $couns);
        $couns_id = rtrim($exp[1], ')');
        $couns_query = "AND Leads.Counsellor_ID = '$couns_id'";
    }else{
        $couns_query = ' ';
    }

    if(!empty($_POST['course'])){
        $course = $_POST['course'];
        $exp = explode('-', $course);
        $new_course = $exp[0];
        $new_univ = $exp[1];
        $get_newuniv_id = $conn->query("SELECT ID FROM Institutes WHERE Name like '$new_univ'");
        $nui = mysqli_fetch_assoc($get_newuniv_id);
        $get_course_id = $conn->query("SELECT ID FROM Courses WHERE Name like '$new_course' AND Institute_ID = '".$nui['ID']."'");
        $gci = mysqli_fetch_assoc($get_course_id);
        $course_query = "AND Leads.Course_ID = '".$gci['ID']."'";
    }else{
        $course_query = ' ';
    }

    if(!empty($_POST['university'])){
        $univ = $_POST['university'];
        $get_univ_id = $conn->query("SELECT ID FROM Institutes WHERE Name like '$univ'");
        $gui = mysqli_fetch_assoc($get_univ_id);
        $univ_query = "AND Leads.Institute_ID = '".$gui['ID']."'";
    }else{
        $univ_query = ' ';
    }

    if(!empty($_POST['manager'])){
        $manager = $_POST['manager'];
        $exp = explode('(', $manager);
        $manager_id = rtrim($exp[1], ')');
        $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
    }else{
        $manager_query = "AND (U1.ID = '".$_SESSION['useremployeeid']."' OR Leads.Counsellor_ID = '".$_SESSION['useremployeeid']."')";
    }

    if(!empty($_POST['date'])){
        if(strpos($_POST['date'],'to')>0)
        {
          $date = explode('to', $_POST['date']);
          $from_date = date($date[0]);
          $to_date = date($date[1]);
          $date_query = "AND (Leads.Created_at BETWEEN '$from_date' AND '$to_date')";
        }
        else{
          $from_date = date($_POST['date']);
          $date_query = "AND (Leads.Created_at BETWEEN '".$from_date." 00:00:00' AND '".$from_date." 23:59:59')";
        }
    }else{
        $date_query = ' ';
    }
    
    $get_reason_count = $conn->query("SELECT COUNT(Leads.ID) as lead_count, Sources.Name FROM Leads LEFT JOIN users ON Leads.Counsellor_ID = users.ID LEFT JOIN users as U1 ON users.Reporting_To_User_ID = U1.ID LEFT JOIN Institutes ON Leads.Institute_ID = Institutes.ID LEFT JOIN Courses ON Leads.Course_ID = Courses.ID LEFT JOIN Stages ON Leads.Stage_ID = Stages.ID LEFT JOIN Sources ON Leads.Source_ID = Sources.ID WHERE users.Role != 'Administrator' ".$course_query.$univ_query.$stage_query.$manager_query.$couns_query.$date_query." GROUP BY Leads.Source_ID ORDER BY lead_count DESC");
        while($grc = $get_reason_count->fetch_assoc()){
            if(is_null($grc['Name'])){
                $grc['Name'] = 'Unknown';
            }
            $data3[] = Array($grc['Name'], (int)$grc['lead_count']);
        }
        
        echo json_encode($data3);
        


?>
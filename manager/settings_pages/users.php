<?php require "filestobeincluded/db_config.php" ?>
<?php
    $all_users = array();
    $users_query_res = $conn->query("SELECT *, CAST(AES_DECRYPT(password, '60ZpqkOnqn0UQQ2MYTlJ') AS CHAR(50)) pass FROM users WHERE Reporting_To_User_ID = '".$_SESSION['useremployeeid']."'");
    while ($row = $users_query_res->fetch_assoc()) {
        $all_users[] = $row;
    }
?>

<?php

$all_institutes = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes WHERE ID <> '0'");
while($row = $institutes_query_res->fetch_assoc()) {
    $all_institutes[] = $row;
}
$theins = "";
foreach ($all_institutes as $ins) {
	$theins = $theins."<option selected value='".$ins['ID']."'>".$ins['Name']."</option>";
}

?>

    <br><br>
    <div id="all_users">
    <script src="https://use.fontawesome.com/f4b83e121b.js"></script>
    <table id="datatable-multiple" class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Role</th>
                <th>Reporting to</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $user_counter=0;
                foreach($all_users as $au){
                    $user_counter++;
                    $users_employee_id = $au['ID'];
                    $full_name_users = $au['Name'];
                    $users_email = strtolower($au['Email']);
                    $users_email_pass = $au['Email_Password'];
                    $users_mobile = $au['Mobile'];
                    $users_role = $au['Role'];
                    $users_pass = $au['pass'];
                    $users_designation = $au['Designation'];
                    $users_id = $au['ID'];
                    $users_status = $au['Status'];
                    if(strcasecmp($users_status, 'Y')==0){
                        $status_user = 'checked';
                    }else{
                        $status_user= 'not';
                    }

                    $get_reporting_user_dets = $conn->query("SELECT * FROM users WHERE ID = '".$au['Reporting_To_User_ID']."'");
                    $reporting_user_dets = mysqli_fetch_assoc($get_reporting_user_dets);
                
            ?>
            <tr>
            <td><?php echo $user_counter; ?></td>
            <td><?php echo $full_name_users; ?></td>
            <td><?php echo $users_email; ?></td>
            <td><?php echo $users_mobile; ?></td>
            <td><?php echo $users_role; ?></td>
            <td><?php echo $reporting_user_dets['Name']; ?></td>
            </tr>
                <?php } ?>
        </tbody>
    </table>
    </div>

<script>
	$(document).ready(function () {
		$('#add_user_form').validate({
			rules: {
				firstname: {
					required: true
				},
				employee_id: {
					required: true
				},
				email: {
					required: true,
					email: "Please enter valid email"
				},
				emailpass: {
					required: true
				},
				mobilenumber: {
					required: true,
					minlength: 10,
					maxlength: 10
				},
				designation: {
					required: true
				},
				role: {
					required: true
				},
				
			},
			messages: {
				firstname: {
					required: "Name is required"
				},
				employee_id: {
					required: "Employee ID is required"
				},
				mobilenumber: {
					required: "Mobile number is required",
					minlength: "Invalid mobile number",
					maxlength: "Invalid mobile number"
				},
				emailpass: {
					required: "Email password is required"
				},
				designation: {
					required: "Designation is required"
				},
				role: {
					required: "Role is required"
				},
				
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
				$(element).closest('.small').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        	$(element).closest('.small').removeClass('has-error');
	        }
		});
	});
</script>

<script>
    $(document).ready(function () {
        $('#select_role').change(function () {

            var selected_role = $('#select_role').val();
            var theins = "<?php echo $theins; ?>";
            if(selected_role == 'Administrator') {
                $('#users_select_institute').html("<option selected value='0'>None</option>");

                $('#select_reportingto').html("<option selected value='1'>None</option>");
            }
            else if(selected_role == 'Manager') {
                $('#select_reportingto').html("<option selected value='1'>Administrator</option>");

                $('#users_select_institute').prop('disabled', false);
                $("#users_select_institute").html(theins);
            }
            else {
                $('#users_select_institute').prop('disabled', false);
                $('#select_reportingto').prop('disabled', false);

                $("#users_select_institute").html(theins);
                jQuery('#users_select_institute').trigger('change');
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#users_select_institute').change(function() {

        	var selected_role = $('#select_role').val();
        	if(selected_role == 'Counsellor') {
        		$('#select_reportingto').html("<option disabled selected>Choose</option>");

	            $.ajax
	            ({
	                type: "POST",
	                url: "onselect/onAssignmentSelect.php",
	                data: { "users_select_institute": ""},
	                success: function(data) {
	                    if(data != "") {
	                        $('#select_reportingto').html(data);
	                    }
	                    else {
	                        $('#select_reportingto').html("<option disabled selected>Choose</option>");
	                    }
	                }
	            });
        	}
        });
    });
</script>

<script>
  function addUser() {

  	if($('#add_user_form').valid()) {

  		var user_first_name = $('#firstname').val();
  		var user_employee_id = $('#employee_id').val();
	    var user_email = $('#email').val();
	    var user_email_pass = $('#emailpass').val();
	    var user_mobile = $('#mobilenumber').val();
	    var user_password = $('#userpassword').val();
	    var user_designation = $('#designation').val();
	    var user_institute = '<?php echo $_SESSION['INSTITUTE_ID'] ?>';
	    var user_role = $('#select_role').val();
	    var user_reporting_to = '<?php echo $_SESSION['useremployeeid'] ?>'
	    $.ajax
	    ({
	        type: "POST",
	        url: "settings_pages/ajax_user/add_user.php",
	        data: {"user_first_name": user_first_name, "user_employee_id": user_employee_id, "user_email": user_email, "user_email_pass": user_email_pass, "user_mobile": user_mobile, "user_password":user_password, "user_designation": user_designation, "user_institute": user_institute, "user_role":user_role, "user_reporting_to":user_reporting_to},
	        success: function (data) {
	            
	            $('#addusermodal').modal('hide');

	            if(data.match("true")) {
					$('#add_user_form')[0].reset();
	                $("#all_users").load(location.href + " #all_users");
	                toastr.success('User added successfully');   
	            }
	            else if(data.match("exists")) {
	                toastr.warning('Email already exists');
	            }else if(data.match("duplicate")) {
	                toastr.warning('Employee ID already exists');
	            }else{
	                toastr.error('Unable to add user');
	            }
	        }
	    });
	    return false;
	}
  }
</script>



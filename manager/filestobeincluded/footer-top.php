<!-- Footer Start -->
<footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            2020 &copy; Black Board. All Rights Reserved.
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->
    <!-- Vendor js -->
    <script src="assets/js/vendor.min.js"></script>

    </div>
    <!-- END wrapper -->
	<!-- Plugins Js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.9.0/jquery.validate.min.js" integrity="sha512-FyKT5fVLnePWZFq8zELdcGwSjpMrRZuYmF+7YdKxVREKomnwN0KTUG8/udaVDdYFv7fTMEc+opLqHQRqBGs8+w==" crossorigin="anonymous"></script>
        <script src="assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
        <script src="assets/libs/select2/select2.min.js"></script>
        <script src="assets/libs/multiselect/jquery.multi-select.js"></script>
        <script src="assets/libs/flatpickr/flatpickr.min.js"></script>
        <script src="assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
        <script src="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
        
        <!-- Init js-->
        <script src="assets/js/pages/form-advanced.init.js"></script>


    <!-- datatable js -->
    <script src="assets/libs/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="assets/libs/datatables/dataTables.responsive.min.js"></script>
        <script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>
        
        <script src="assets/libs/datatables/dataTables.buttons.min.js"></script>
        <script src="assets/libs/datatables/buttons.bootstrap4.min.js"></script>
        <script src="assets/libs/datatables/buttons.html5.min.js"></script>
        <script src="assets/libs/datatables/buttons.flash.min.js"></script>
        <script src="assets/libs/datatables/buttons.print.min.js"></script>

        <script src="assets/libs/datatables/dataTables.keyTable.min.js"></script>
        <script src="assets/libs/datatables/dataTables.select.min.js"></script>

        <!-- Datatables init -->
        <script src="assets/js/pages/datatables.init.js"></script>

	<script src="assets/libs/moment/moment.min.js"></script>
        <script src="assets/libs/flatpickr/flatpickr.min.js"></script>
        
        <script src="assets/libs/smartwizard/jquery.smartWizard.min.js"></script>

        <script src="assets/js/pages/form-wizard.init.js"></script>
        <!-- page js -->
        

    <!-- App js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/toastr.min.js"></script>


    <script>
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
    </script>

<?php
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if($actual_link=='https://crm.collegevidya.com/teamlead/myfollowup'){
        $table_id = '#followup_table';
    }else if($actual_link=='https://crm.collegevidya.com/counsellor/myfollowup'){
        $table_id = '#followup_table';
    }else if($actual_link=='https://crm.collegevidya.com/teamlead/myfollowup'){
        $table_id = '#followup_table';
    }else if($actual_link=='https://crm.collegevidya.com/teamlead/settings'){
        $table_id = '#basic-datatable';
    }else{
        $table_id = '#datatable-buttons';
    }  
    
    
?>
            <script>
                $(document).ready(function() {
                    var dataTable = $('<?php echo $table_id; ?>').dataTable(
                        
                    );
                    $("#searchbox").keyup(function() {
                        dataTable.fnFilter(this.value);
                    });
                     
                });
            </script>

<script src="assets/libs/summernote/summernote-bs4.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.summernote').summernote({
                height: 330,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false                 // set focus to editable area after initializing summernote
            });
        });
    </script>


<?php require "db_config.php" ?>

<?php

$all_sources = array();

$sources_query_res = $conn->query("SELECT * FROM Sources");
while($row = $sources_query_res->fetch_assoc()) {
    $all_sources[] = $row;
}

$all_stages = array();

$stages_query_res = $conn->query("SELECT * FROM Stages ORDER BY Name ASC");
while($row = $stages_query_res->fetch_assoc()) {
    $all_stages[] = $row;
}

$all_users = array();

$users_query_res = $conn->query("SELECT * FROM users  WHERE ID in ($tree_ids)");
while($row = $users_query_res->fetch_assoc()) {
    $all_users[] = $row;
}

$all_institutes = array();

$institutes_query_res = $conn->query("SELECT * FROM Institutes  WHERE ID = '".$_SESSION['INSTITUTE_ID']."' ");
while($row = $institutes_query_res->fetch_assoc()) {
	$all_institutes[] = $row;
}

?>

</head>

<body>


    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        <div class="navbar navbar-expand flex-column flex-md-row navbar-custom">
            <div class="container-fluid">
                <!-- LOGO -->
                <a href="/" class="navbar-brand mr-0 mr-md-2 logo">
                    <span class="logo-lg">
                        <center><img src="/assets/images/logo.png" alt="" height="80" /></center>
                        <!--<span class="d-inline h4 ml-1 text-logo">CRM</span>-->
                    </span>
                    <span class="logo-sm">
                        <img src="/assets/images/logo.png" alt="" height="80">
                    </span>
                </a>

                <ul class="navbar-nav bd-navbar-nav flex-row list-unstyled menu-left mb-0">
                    <li class="">
                        <button class="button-menu-mobile open-left disable-btn">
                            <i data-feather="menu" class="menu-icon"></i>
                            <i data-feather="x" class="close-icon"></i>
                        </button>
                    </li>
                </ul>

                <ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu float-right mb-0">
                    <div id="divShowHide1" style="display: none;">
                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Refer selected lead">
                            <a href="javascript:void(0);" class="nav-link selected-refer">
                                <i data-feather="share-2"></i>
                            </a>
                        </li>
                        <script type='text/javascript'>
                            $(document).ready(function(){

                                $('.selected-refer').click(function(){
                                    
                                    var data_id = $('#checkbox-form').find('input[name="id[]"]').serialize();
                                    console.log(data_id);

                                    // AJAX request
                                    $.ajax({
                                        url: 'ajax_leads/refer_selected.php',
                                        type: 'post',
                                        data:{"data_id":data_id},
                                        success: function(response){ 
                                            // Add response in Modal body
                                            $('#modal-body-refer-selected').html(response); 

                                            // Display Modal
                                            $('#selected_refer').modal('show'); 
                                        }
                                    });
                                });
                            });
                        </script>
                    </div>
                    <div id="divShowHide2" style="display: none;">
                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Send SMS to selected lead">
                            <a href="javascript:void(0);" class="nav-link selected-sms">
                                <i data-feather="message-square"></i>
                            </a>
                        </li>
                        <script type='text/javascript'>
                            $(document).ready(function(){

                                $('.selected-sms').click(function(){
                                    
                                    var data_id = $('#checkbox-form').find('input[name="id[]"]').serialize();
                                    console.log(data_id);

                                    // AJAX request
                                    $.ajax({
                                        url: 'ajax_leads/sms_selected.php',
                                        type: 'post',
                                        data:{"data_id":data_id},
                                        success: function(response){ 
                                            // Add response in Modal body
                                            $('#modal-body-sms-selected').html(response); 

                                            // Display Modal
                                            $('#selected_sms').modal('show'); 
                                        }
                                    });
                                });
                            });
                        </script>
                    </div>
                    <div id="divShowHide3" style="display: none;">
                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Send Mail to selected lead">
                            <a href="javascript:void(0);" class="nav-link selected-mail">
                                <i data-feather="mail"></i>
                            </a>
                        </li>
                        <script type='text/javascript'>
                            $(document).ready(function(){

                                $('.selected-mail').click(function(){
                                    
                                    var data_id = $('#checkbox-form').find('input[name="id[]"]').serialize();
                                    console.log(data_id);

                                    // AJAX request
                                    $.ajax({
                                        url: 'ajax_leads/mail_selected.php',
                                        type: 'post',
                                        data:{"data_id":data_id},
                                        success: function(response){ 
                                            // Add response in Modal body
                                            $('#modal-body-mail-selected').html(response); 

                                            // Display Modal
                                            $('#selected_mail').modal('show'); 
                                        }
                                    });
                                });
                            });
                        </script>
                    </div>
                    <!-- <div id="divShowHide4" style="display: none;">
                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Dalete selected lead">
                            <a href="javascript:void(0);" class="nav-link selected-delete">
                                <i data-feather="trash"></i>
                            </a>
                        </li>
                        <script type='text/javascript'>
                            $(document).ready(function(){

                                $('.selected-delete').click(function(){
                                    
                                    var data_id = $('#checkbox-form').find('input[name="id[]"]').serialize();
                                    console.log(data_id);

                                    // AJAX request
                                    $.ajax({
                                        url: 'ajax_leads/delete_selected.php',
                                        type: 'post',
                                        data:{"data_id":data_id},
                                        success: function(response){ 
                                            // Add response in Modal body
                                            $('#modal-body-delete-selected').html(response); 

                                            // Display Modal
                                            $('#selected_delete').modal('show'); 
                                        }
                                    });
                                });
                            });
                        </script>
                    </div> -->

                    <!-- <div id="divShowHide5" style="display: none;">
                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Download selected lead">
                            <a href="javascript:void(0);" class="nav-link selected-download">
                                <i data-feather="download"></i>
                            </a>
                        </li>
                        <script type='text/javascript'>
                            $(document).ready(function(){

                                $('.selected-download').click(function(){
                                    
                                    var data_id = $('#checkbox-form').find('input[name="id[]"]').serialize();
                                    console.log(data_id);

                                    // AJAX request
                                    $.ajax({
                                        url: 'ajax_leads/download_selected.php',
                                        type: 'post',
                                        data:{"data_id":data_id},
                                        success: function(data){ 
                                            var downloadLink = document.createElement("a");
                                                    var fileData = ['\ufeff'+data];

                                                    var blobObject = new Blob(fileData,{
                                                        type: "text/csv;charset=utf-8;"
                                                    });

                                                    

                                                    var url = URL.createObjectURL(blobObject);
                                                    downloadLink.href = url;
                                                    downloadLink.download = "Blackboard_Leads_<?php echo date("d-m-Y h:i a"); ?>.csv";

                                                    /*
                                                    * Actually download CSV
                                                    */
                                                    document.body.appendChild(downloadLink);
                                                    downloadLink.click();
                                                    document.body.removeChild(downloadLink);
                                            // Add response in Modal body
                                            
                                        }
                                    });
                                });
                            });
                        </script>
                    </div> -->


                        <li class="dropdown notification-list" data-toggle="tooltip" data-placement="left" title="Add Quick Lead">
                            <a href="javascript:void(0);" class="nav-link" data-toggle="modal" data-target="#addquicklead">
                                <i data-feather="plus-circle"></i>
                            </a>
                        </li>

                        


                        <li class="d-none d-sm-block">
                        <div class="app-search">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="searchbox" class="form-control" onkeypress="handle(event);" placeholder="Search..." />
                            
                            </div>
                            <button type="button" onclick="getList()" class="btn btn-primary" style="height:36px;">
                                <i class="fas fa-search"></i>
                            </button>
                            </div>
                        </div>
                    </li>

                    

                </ul>
            </div>
            <script>
                function handle(e){
                    if(e.keyCode===13){
                        e.preventDefault();
                        getList();
                    }
                }
            </script>

        </div>
        <!-- end Topbar -->


        <!------------Quick Lead Modal----------->
        <div id="addquicklead" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="addquicklead">Add Quick Leads</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST" id="quick_lead_form">
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Full Name</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="quick_lead_name" name="quick_lead_name" placeholder="Full Name">
                                                    </div>
                                                    <label class="col-lg-2 col-form-label">Email ID</label>
                                                    <div class="col-lg-4">
                                                        <input type="email" class="form-control" id="quick_lead_email" name="quick_lead_email" placeholder="leadmail@email.com">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Mobile Number</label>
                                                    <div class="col-lg-4">
                                                        <input type="number" maxlength="10" minlength="10" name="quick_lead_mobile" class="form-control" id="quick_lead_mobile" placeholder="9450XXXXXX">
                                                    </div>
                                                    <label class="col-lg-2 col-form-label">Alternate Number</label>
                                                    <div class="col-lg-4">
                                                        <input type="number" maxlength="10" minlength="10" name="quick_lead_alt_mobile" class="form-control" id="quick_lead_alt_mobile" placeholder="9450XXXXXX">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="dob1">DOB</label>
                                                    <div class="col-lg-4">
                                                        <input type="date" id="dob1" name="dob1" class="form-control" placeholder="Date of birth">
                                                    </div>        
                                                    <label class="col-lg-2 col-form-label" for="quick_lead_remarks">Remarks</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="quick_lead_remarks" placeholder="Remarks">
                                                    </div>

                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Source</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" name="quick_lead_source" id="quick_lead_source">
                                                            <option selected disabled>Choose</option>
                                                            <?php
                                                            foreach ($all_sources as $source) {
                                                                ?>
                                                                <option value="<?php echo $source['ID']; ?>"><?php echo $source['Name']; ?></option>
                                                                <?
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label">Sub-Source</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" name="quick_lead_sub_source" id="quick_lead_sub_source">
                                                            <option selected disabled>Choose</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Stage</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" name="quick_lead_stage" id="quick_lead_stage">
                                                            <option selected disabled>Choose</option>
                                                            <?php
                                                            foreach ($all_stages as $stage) {
                                                                ?>
                                                                <option value="<?php echo $stage['ID']; ?>"><?php echo $stage['Name']; ?></option>
                                                                <?
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label">Reason</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" name="quick_lead_reason" id="quick_lead_reason">
                                                            <option selected disabled>Choose</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Counsellor</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" name="quick_lead_lead_owner" id="quick_lead_lead_owner">
                                                            <option selected disabled>Choose</option>
                                                            <?php
                                                            foreach ($all_users as $user) {
                                                                ?>
                                                                <option value="<?php echo $user['ID']; ?>"><?php echo $user['Name']; ?></option>
                                                                <?
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label">University</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" name="quick_lead_university" id="quick_lead_university">
                                                            <option selected disabled>Choose</option>
                                                            <?php
                                                            foreach ($all_institutes as $institute) {
                                                                ?>
                                                                <option value="<?php echo $institute['ID']; ?>" <?php if($institute['ID']==$_SESSION['INSTITUTE_ID']){ echo 'selected'; } ?>><?php echo $institute['Name']; ?></option>
                                                                <?
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label">Course</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" name="quick_lead_course" id="quick_lead_course">
                                                            <option selected disabled>Choose</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label">Specialization</label>
                                                    <div class="col-lg-4">
                                                        <select data-plugin="customselect" class="small" id="quick_lead_specialization">
                                                            <option selected disabled>Choose</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-lg-2 col-form-label">State</label>
                                                    <div class="form-group col-lg-4">
                                                        
                                                        <select data-plugin="customselect" class="form-control" id="new_state_selectf" name="state_select">
                                                            <option selected value="<?php echo $row['State_ID'] ?>"><?
                                                            $get_state_name = $conn->query("SELECT * FROM States WHERE ID = '".$row['State_ID']."'");
                                                            $state_name = mysqli_fetch_assoc($get_state_name);
                                                            if($get_state_name->num_rows>0){
                                                                echo $state_name['Name'];
                                                            }else{
                                                                $state_name['Name'] = 'Select State';
                                                                echo $state_name['Name'];
                                                            }
                                                            ?></option>
                                                            <?php
                                                                $result_state = $conn->query("SELECT * FROM States WHERE Country_ID = 101");
                                                                while($states = $result_state->fetch_assoc()) {
                                                            ?>
                                                                <option value="<?php echo $states['ID']; ?>"><?php echo $states['Name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label class="col-lg-2 col-form-label" for="new_city_selectf">City</label>
                                                    <div class="form-group col-lg-4">
                                                        
                                                        <select data-plugin="customselect" class="form-control" id="new_city_selectf">
                                                            <option selected value="<?php echo $row['City_ID'] ?>"><?
                                                            $get_city_name = $conn->query("SELECT * FROM Cities WHERE ID = '".$row['City_ID']."'");
                                                            $city_name = mysqli_fetch_assoc($get_city_name);
                                                            if($get_city_name->num_rows > 0){
                                                                echo $city_name['Name'];
                                                            }else{
                                                                $city_name['Name']= 'Choose City';
                                                                echo $city_name['Name'];
                                                            }
                                                            
                                                            ?></option>
                                                            <?php
                                                                $result_city = $conn->query("SELECT * FROM Cities WHERE State_ID = '".$row['State_ID']."'");
                                                                while($cities = $result_city->fetch_assoc()) {
                                                            ?>
                                                                <option value="<?php echo $cities['ID']; ?>"><?php echo $cities['Name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                            
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="addQuickLead();">Save</button>
                                            </div>
                                        </form>

                                    
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <!-----------End Modal------------------->

<script>
	function addQuickLead() {
		if($('#quick_lead_form').valid()) {

			var fullName = $('#quick_lead_name').val();
			var emailID = $('#quick_lead_email').val();
			var mobileNumber = $('#quick_lead_mobile').val();
			var altMobileNumber = $('#quick_lead_alt_mobile').val();
			var sourceID = $('#quick_lead_source').val();
			var subsourceID = $('#quick_lead_sub_source').val();
			var stageID = $('#quick_lead_stage').val();
			var reasonID = $('#quick_lead_reason').val();
			var leadOwner = $('#quick_lead_lead_owner').val();
			var instituteID = $('#quick_lead_university').val();
			var courseID = $('#quick_lead_course').val();
			var specializationID = $('#quick_lead_specialization').val();
			var remarks = $('#quick_lead_remarks').val();
            var state_ID = $('#new_state_selectf').val();
            var city_ID = $('#new_city_selectf').val();
            var dob = $('#dob1').val();

			$.ajax
	        ({
	          type: "POST",
	          url: "filestobeincluded/add_quick_lead.php",
	          data: { "fullName": fullName, "emailID":  emailID, "mobileNumber": mobileNumber, "altMobileNumber": altMobileNumber, "sourceID": sourceID, "subsourceID": subsourceID, "stageID": stageID, "reasonID": reasonID, "leadOwner": leadOwner, "instituteID": instituteID, "courseID": courseID, "specializationID": specializationID, "remarks": remarks, "dob": dob,"state_ID":state_ID,"city_ID":city_ID },
	          success: function (data) {
	          	console.log(data);
	            $('#addquicklead').modal('hide');
	            if(data.match("true")) {
	                toastr.success('Lead added successfully');
	                window.location.reload();
	            }
                else if(data.match("renquired")) {
                    toastr.warning('Lead already exists');
                    window.location.reload();
                }
	            else {
	                toastr.error('Unable to add lead');
	            }
	          }
	        });
	        return false;
		}
	}
</script>

<script>
    $(document).ready(function() {
        $('#quick_lead_source').change(function() {

            $('#quick_lead_sub_source').html("<option disabled selected>Select Sub-Source</option>");

            var selected_source = $('#quick_lead_source').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "source_select": "", "selected_source": selected_source },
                success: function(data) {
                    if(data != "") {
                        $('#quick_lead_sub_source').html(data);
                    }
                    else {
                        $('#quick_lead_sub_source').html("<option disabled selected>Select Sub-Source</option>");
                    }
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#quick_lead_stage').change(function() {

            $('#quick_lead_reason').html("<option disabled selected>Select Reason</option>");

            var selected_stage = $('#quick_lead_stage').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "stage_select": "", "selected_stage": selected_stage },
                success: function(data) {
                    if(data != "") {
                        $('#quick_lead_reason').html(data);
                    }
                    else {
                        $('#quick_lead_reason').html("<option disabled selected>Select Reason</option>");
                    }
                }
            });
        });
    });
</script>

<script>
    function get_course() {

            $('#quick_lead_course').html("<option disabled selected>Select Course</option>");
            $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");

            var selected_institute = $('#quick_lead_university').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "select_institute": "", "selected_institute": selected_institute },
                success: function(data) {
                    if(data != "") {
                        $('#quick_lead_course').html(data);
                        jQuery('#quick_lead_course').trigger('change');
                    }
                    else {
                        $('#quick_lead_course').html("<option disabled selected>Select Course</option>");
                        $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");
                    }
                }
            });
    }
    get_course();
</script>

<script>
    $(document).ready(function() {
        $('#quick_lead_course').change(function() {

            $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");

            var selected_course = $('#quick_lead_course').val();
            var selected_institute = $('#quick_lead_university').val();

            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "select_course": "", "selected_course": selected_course, "selected_institute": selected_institute },
                success: function(data) {
                    if(data != "") {
                        $('#quick_lead_specialization').html(data);
                    }
                    else {
                        $('#quick_lead_specialization').html("<option disabled selected>Select Specialization</option>");
                    }
                }
            });
        });
    });
</script>


<script>
	$(document).ready(function(){

		$('#quick_lead_form').validate({
			rules: {
				quick_lead_name: {
					required: true
				},
				quick_lead_email: {
					email: "Please enter a valid email address."
				},
				quick_lead_mobile: {
					required: true,
					minlength: 10,
					maxlength: 15
				},
				quick_lead_alt_mobile: {
					minlength: 10,
					maxlength: 15
				},
				quick_lead_source: {
					required: true
				},
				quick_lead_sub_source: {
					required: true
				},
				quick_lead_stage: {
					required: true
				},
				quick_lead_reason: {
					required: true
				},
				quick_lead_lead_owner: {
					required: true
				},
				quick_lead_university: {
					required: true
				},
				quick_lead_course: {
					required: true
				}
			},
			messages: {
				quick_lead_name: {
					required: 'Full Name is required'
				},
				quick_lead_mobile: {
					required: 'Mobile number is required',
					minlength: 'Invalid mobile number',
					maxlength: 'Invalid mobile number'
				},
				quick_lead_alt_mobile: {
					minlength: 'Invalid mobile number',
					maxlength: 'Invalid mobile number'
				},
				quick_lead_source: {
					required: 'Please select a source'
				},
				quick_lead_sub_source: {
					required: 'Please select a sub-source'
				},
				quick_lead_stage: {
					required: 'Please select a stage'
				},
				quick_lead_reason: {
					required: 'Please select a reason'
				},
				quick_lead_lead_owner: {
					required: 'Please select a lead owner'
				},
				quick_lead_university: {
					required: 'Please select a university'
				},
				quick_lead_course: {
					required: 'Please select a course'
				}
			},
			highlight: function (element) {
				$(element).addClass('error');
				$(element).closest('.form-control').addClass('has-error');
				$(element).closest('.small').addClass('has-error');
	        },
	        unhighlight: function (element) {
	        	$(element).removeClass('error');
	        	$(element).closest('.form-control').removeClass('has-error');
	        	$(element).closest('.small').removeClass('has-error');
	        }
		});

	});
</script>
<script>
    $(document).ready(function() {
        $('#new_state_selectf').change(function() {

            $('#new_city_selectf').html("<option disabled selected>Select City</option>");

            var new_selected_state = $('#new_state_selectf').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "state_select": "", "selected_state": new_selected_state },
                success: function(data) {
                    if(data != "") {
                        $('#new_city_selectf').html(data);
                    }
                    else {
                        $('#new_city_selectf').html("<option disabled selected>Select City</option>");
                    }
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#new_state_selectf').change(function() {

            $('#new_city_selectf').html("<option disabled selected>Select City</option>");

            var new_selected_state = $('#new_state_selectf').val();
            $.ajax
            ({
                type: "POST",
                url: "../onselect/onSelect.php",
                data: { "state_select": "", "selected_state": new_selected_state },
                success: function(data) {
                    if(data != "") {
                        $('#new_city_selectf').html(data);
                    }
                    else {
                        $('#new_city_selectf').html("<option disabled selected>Select City</option>");
                    }
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        jQuery('#quick_lead_university').trigger('change');
    });
    $(document).ready(function() {
        jQuery('#quick_lead_course').trigger('change');
    });
</script>
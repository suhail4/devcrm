<?php
if(session_status() === PHP_SESSION_NONE) session_start();
require '../filestobeincluded/db_config.php';

$sms_temp_body = $_POST['sms_temp_body'];
$mobile = $_POST['mobile'];
$all_fine = 0;

$leads_mobile = explode(",", $mobile);
array_pop($leads_mobile);

require_once '../DialerAPI/vendor/autoload.php';

$client = new \GuzzleHttp\Client();

foreach ($leads_mobile as $individual_lead_mobile) {
    $get_current_lead_details = $conn->query("SELECT * FROM Leads WHERE Mobile = '".$individual_lead_mobile."'");
	$gcld = mysqli_fetch_assoc($get_current_lead_details);
	$lead_name = $gcld['Name'];
	$lead_id = $gcld['ID'];

    $phoneNumber = $individual_lead_mobile;
    $leadName = $lead_name;

    $counsellor_query = $conn->query("SELECT * FROM users WHERE ID = '".$_SESSION['useremployeeid']."'");
    $counsellor_details = mysqli_fetch_assoc($counsellor_query);

    $counsellor_name = $counsellor_details['Name'];
    $counsellor_mobile = $counsellor_details['Mobile'];
    $counsellor_email= $counsellor_details['Email'];

    $sms_temp_body = str_replace('$lead_name', $lead_name, $sms_temp_body);
    $sms_temp_body = str_replace('$counsellor_name', $counsellor_name, $sms_temp_body);
    $sms_temp_body = str_replace('$counsellor_contact_no', $counsellor_mobile, $sms_temp_body);
    $sms_temp_body = str_replace('$counsellor_email', $counsellor_email, $sms_temp_body);

    $sender_id_query = $conn->query("SELECT Sender_ID FROM Institutes LEFT JOIN users ON Institutes.ID = users.Institute_ID WHERE users.ID = '".$_SESSION['useremployeeid']."'");

    $sender_id_dets = mysqli_fetch_assoc($sender_id_query);
	$sender_id = $sender_id_dets['Sender_ID'];

    if(strcasecmp($sender_id, "")!=0) {
		$key = "010TH37S00jcoqsW5hBKpgRe0mJEA";
		$profile_id = "624897";

		$response = $client->request('GET', 'http://sms.venetsmedia.com/shn/api/pushsms.php?usr='.$profile_id.'&key='.$key.'&sndr='.$sender_id.'&ph='.$phoneNumber.'&text='.urlencode($sms_temp_body).'&rpt=1');
		$resp = $response->getBody();

		if (strpos($resp, 'Message Sent.') !== false) {
			$all_fine++;
			$create_log = $conn->query("INSERT INTO SMS_Logs(Lead_ID, Employee_ID, SMS_temp) VALUES ('".$lead_id."', '".$_SESSION['useremployeeid']."', '".$sms_temp_body."')");
			if(!$create_log) {
				echo mysqli_error($conn);
			}
		}
    }
    else {
    	echo "false";
    	return;
    }
}

if($all_fine > 0) {
	echo "true";
}
else {
	echo "false";
}

?>
<?php require '/opt/lampp/htdocs/filestobeincluded/db_config.php'; ?>

<?php

date_default_timezone_set("Asia/Kolkata");

$get_all_universities = $conn->query("SELECT * FROM Institutes WHERE ID <> '0'");
while($uni = $get_all_universities->fetch_assoc()) {

	$get_all_managers = $conn->query("SELECT * FROM users WHERE Role = 'Manager' AND Institute_ID = '".$uni['ID']."'");
	while($manager = $get_all_managers->fetch_assoc()) {

		$all_uni_counsellors = array();

		$all_new_leads = array();

		$get_new_leads = $conn->query("SELECT Leads.ID, Leads.TimeStamp FROM Leads LEFT JOIN users ON Leads.Counsellor_ID=users.ID WHERE users.Reporting_To_User_ID='".$manager['ID']."' AND (Leads.Stage_ID = '1' OR Leads.Reason_ID = '25') AND Leads.Institute_ID = '".$uni['ID']."'");

		while($new_lead = $get_new_leads->fetch_assoc()) {
			$new_lead_timestamp = $new_lead['TimeStamp'];

			$number_of_minutes = round((time() - strtotime($new_lead_timestamp))/(60));

			if((int)$number_of_minutes >= 75) {
				$all_new_leads[] = $new_lead;
			}
		}

		$get_uni_counsellors = $conn->query("SELECT * FROM users WHERE Role = 'Counsellor' AND Institute_ID = '".$uni['ID']."' AND Reporting_To_User_ID = '".$manager['ID']."'");
		while($uni_counsellor = $get_uni_counsellors->fetch_assoc()) {
			$all_uni_counsellors[] = $uni_counsellor;
		}

		if(count($all_new_leads) > 0 && count($all_uni_counsellors) > 0) {
			$uni_new_lead_chunks = slotify($all_new_leads, count($all_uni_counsellors));
		}
		else {
			$uni_new_lead_chunks = array();
		}

		shuffle($all_uni_counsellors);

		foreach ($uni_new_lead_chunks as $index => $lead_chunks) {
			$counsellor = $all_uni_counsellors[$index];
			foreach ($lead_chunks as $lead) {
				$update_lead = $conn->query("UPDATE Leads SET Stage_ID = '1', Reason_ID = '25', Counsellor_ID = '".$counsellor['ID']."' WHERE ID = '".$lead['ID']."'");
				if($update_lead) {
					//echo $counsellor['ID'];
				}
				else {
					echo mysqli_error($conn);
				}
			}
		}
	}
}

function slotify($array, $length)
{
    $count = count($array);
    $chunk_size = ceil($count/$length);
    $boundary = $count%$length;

    if($boundary === 0)
        $boundary = $length;

    for($i=0; $i<$length; $i++) {
        if($i === $boundary)
            $chunk_size--;
        $result[$i] = array_splice($array, 0, $chunk_size);
    }
    
    $result = array_map('array_filter', $result);
    $result = array_filter($result);
    return $result;
}

?>
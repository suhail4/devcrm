<?php

if(session_status() === PHP_SESSION_NONE) session_start();


require '../filestobeincluded/db_config.php';

/**
 * This example shows settings to use when sending via Google's Gmail servers.
 * This uses traditional id & password authentication - look at the gmail_xoauth.phps
 * example to see how to use XOAUTH2.
 * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
 */

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require 'vendor/autoload.php';

$mail_temp_id = $_POST['mail_id'];



$all_users=array();
    $user_query_res = $conn->query("SELECT * FROM users WHERE ID = '".$_SESSION['useremployeeid']."'");
    while($urow = $user_query_res->fetch_assoc()) {
        $all_users[] = $urow;
    }
    foreach($all_users as $au){
        $counsellor_name = $au['Name'];

        $counsellor_mobile = $au['Mobile'];

        $counsellor_email= $au['Email'];

        $emailpass = $au['Email_Password'];
    }

    $all_leads=array();
    $leads_query_res = $conn->query("SELECT * FROM Leads");
    while($row = $leads_query_res->fetch_assoc()) {
        $all_leads[] = $row;
    }
    foreach($all_leads as $al){
    $leadid = $al['ID'];
    $lead_name = $al['Name'];
    $emailid = $al['Email'];

    //Create a new PHPMailer instance
        $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // SMTP::DEBUG_OFF = off (for production use)
    // SMTP::DEBUG_CLIENT = client messages
    // SMTP::DEBUG_SERVER = client and server messages
    $mail->SMTPDebug = 1;

    //Set the hostname of the mail server
    $mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 587;

    //Set the encryption mechanism to use - STARTTLS or SMTPS
    $mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    $all_temp=array();
    $temp_query_res = $conn->query("SELECT * FROM Email_Templates Where id = $mail_temp_id");
    while($mrow = $temp_query_res->fetch_assoc()) {
        $all_temp[] = $mrow;
    }
    
    foreach($all_temp as $at){
        $body = trim($at['email_template']);
        $subject = $at['subject'];
        $body = str_replace('$lead_name', $lead_name, $body);
        $body = str_replace('$counsellor_name', $counsellor_name, $body);
        $body = str_replace('$counsellor_contact_no', $counsellor_mobile, $body);
        $body = str_replace('$counsellor_email', $counsellor_email, $body);
    }

    


    /**
     * This example shows settings to use when sending via Google's Gmail servers.
     * This uses traditional id & password authentication - look at the gmail_xoauth.phps
     * example to see how to use XOAUTH2.
     * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
     */

    
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = $counsellor_email;

    //Password to use for SMTP authentication
    $mail->Password = $emailpass;
    

    //Set who the message is to be sent from
    $mail->setFrom($counsellor_email, $counsellor_name);

    //Set an alternative reply-to address
    $mail->addReplyTo($counsellor_email, $counsellor_name);

    //Set who the message is to be sent to
    $mail->addAddress($emailid, $lead_name);

    //Set the subject line
    $mail->Subject = $subject;

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

    //Replace the plain text body with one created manually
    $mail->Body = $body;
    $mail->IsHTML(true);
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');
    //send the message, check for errors
    $body = mysqli_real_escape_string($conn, $body);
    $create_log = $conn->query("INSERT INTO `Email_Logs`(`Lead_ID`,`Employee_ID`, `Email_temp`, `Email_Send_By`, `Sender_Mail`) VALUES ('$leadid','".$_SESSION['useremployeeid']."','$body','$counsellor_name','$counsellor_email')");

    if (!$mail->send()) {
        //echo 'Mailer Error: '. $mail->ErrorInfo;
    } else {
        echo 'true';
        //Section 2: IMAP
        //Uncomment these to save your message in the 'Sent Mail' folder.
        #if (save_mail($mail)) {
        #    echo "Message saved!";
        #}
    }
}


